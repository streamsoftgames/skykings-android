﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public static GameManager _Instance { get; private set; }

    #region Encapsulamento
    public string SceneActualName
    {
        get
        {
            return sceneActualName;
        }

        set
        {
            sceneActualName = value;
        }
    }

    public float TravelledDistance
    {
        get
        {
            return travelledDistance;
        }

        set
        {
            travelledDistance = value;
        }
    }

    public int GameLevel
    {
        get
        {
            return gameLevel;
        }

        set
        {
            gameLevel = value;
        }
    }

    public int PointsOnGame
    {
        get
        {
            return pointsOnGame;
        }

        set
        {
            pointsOnGame = value;
        }
    }

    public bool LevelChanged
    {
        get
        {
            return levelChanged;
        }

        set
        {
            levelChanged = value;
        }
    }

    public bool IsPaused
    {
        get
        {
            return isPaused;
        }

        set
        {
            isPaused = value;
        }
    }

    public bool SwapScene
    {
        get
        {
            return swapScene;
        }

        set
        {
            swapScene = value;
        }
    }

    public bool InGame
    {
        get
        {
            return inGame;
        }

        set
        {
            inGame = value;
        }
    }



    public Vector2 LastPosition
    {
        get
        {
            return lastPosition;
        }

        set
        {
            lastPosition = value;
        }
    }

    public int MoneyGame
    {
        get
        {
            return moneyGame;
        }

        set
        {
            moneyGame = value;
        }
    }

    public float DistanceGameLevelUP
    {
        get
        {
            return distanceGameLevelUP;
        }

        set
        {
            distanceGameLevelUP = value;
        }
    }

    public float AuxDistanceTravelled
    {
        get
        {
            return auxDistanceTravelled;
        }

        set
        {
            auxDistanceTravelled = value;
        }
    }

    public bool IsInfiniteFuel
    {
        get
        {
            return isInfiniteFuel;
        }

        set
        {
            isInfiniteFuel = value;
        }
    }

    public bool IsInvencible
    {
        get
        {
            return isInvencible;
        }

        set
        {
            isInvencible = value;
        }
    }

    public bool IsDoubleMoney
    {
        get
        {
            return isDoubleMoney;
        }

        set
        {
            isDoubleMoney = value;
        }
    }

    public int TotalMoneyGame
    {
        get
        {
            return totalMoneyGame;
        }

        set
        {
            totalMoneyGame = value;
        }
    }

    public float BestDistance
    {
        get
        {
            return bestDistance;
        }

        set
        {
            bestDistance = value;
        }
    }

    public int MedalMoney
    {
        get
        {
            return medalMoney;
        }

        set
        {
            medalMoney = value;
        }
    }

    public int QuantityInvencible
    {
        get
        {
            return quantityInvencible;
        }

        set
        {
            quantityInvencible = value;
        }
    }

    public int QuantityInfiniteFuel
    {
        get
        {
            return quantityInfiniteFuel;
        }

        set
        {
            quantityInfiniteFuel = value;
        }
    }

    public int QuantityEspecialShoot
    {
        get
        {
            return quantityEspecialShoot;
        }

        set
        {
            quantityEspecialShoot = value;
        }
    }

    public bool IsSpecialShoot
    {
        get
        {
            return isSpecialShoot;
        }

        set
        {
            isSpecialShoot = value;
        }
    }

    public int ValueBuyUpgradeInvisibility
    {
        get
        {
            return valueBuyUpgradeInvisibility;
        }

        set
        {
            valueBuyUpgradeInvisibility = value;
        }
    }

    public int ValueBuyUpgradeFuelInfinite
    {
        get
        {
            return valueBuyUpgradeFuelInfinite;
        }

        set
        {
            valueBuyUpgradeFuelInfinite = value;
        }
    }

    public int ValueBuyUpgradeSpecialShoot
    {
        get
        {
            return valueBuyUpgradeSpecialShoot;
        }

        set
        {
            valueBuyUpgradeSpecialShoot = value;
        }
    }

    public int LevelInvencibility
    {
        get
        {
            return levelInvencibility;
        }

        set
        {
            levelInvencibility = value;
        }
    }

    public int LevelFuelInifinite
    {
        get
        {
            return levelFuelInifinite;
        }

        set
        {
            levelFuelInifinite = value;
        }
    }

    public int LevelSpecialShoot
    {
        get
        {
            return levelSpecialShoot;
        }

        set
        {
            levelSpecialShoot = value;
        }
    }

    public float TimeInvencibility
    {
        get
        {
            return timeInvencibility;
        }

        set
        {
            timeInvencibility = value;
        }
    }

    public float TimeFuelInfinite
    {
        get
        {
            return timeFuelInfinite;
        }

        set
        {
            timeFuelInfinite = value;
        }
    }

    public float TimeSpecialShoot
    {
        get
        {
            return timeSpecialShoot;
        }

        set
        {
            timeSpecialShoot = value;
        }
    }

    public int BestGameLevel
    {
        get
        {
            return bestGameLevel;
        }

        set
        {
            bestGameLevel = value;
        }
    }

    public int SelectedShip_ID
    {
        get
        {
            return selectedShip_ID;
        }

        set
        {
            selectedShip_ID = value;
        }
    }

    public int TotalPointsInGame
    {
        get
        {
            return totalPointsInGame;
        }

        set
        {
            totalPointsInGame = value;
        }
    }

    #endregion                                                      //Inicio dos Encapsulamentos
    [SerializeField]    private bool PRIMEIRA_VEZ = true;

    public AudioManager _audio;
    public PlayerManager _player;                                       //Instancia do player.

    /* Para todos */
    [SerializeField]    private string sceneActualName;                                     //Nome da cena atual do jogo.


    /* In game */
    [SerializeField]    private int gameLevel = 1;                      //level atual que o jogador está jogando.
    [SerializeField]    private int pointsOnGame = 0;                   //pontos dentro do jogo.
    [SerializeField]    private int totalPointsInGame = 0;              //guarda o total de pontos no jogo;
    [SerializeField]    private int moneyGame = 0;                      //dinheiro ganho na partida.
    [SerializeField]    private int totalMoneyGame = 0;                 //dinheiro total do jogador.
    [SerializeField]    private int medalMoney;

    private int bestGameLevel;

    private bool swapScene = false;                 //trocou de cena.
    [SerializeField]    private bool inGame = false;                    //no jogo, player jogando.
    [SerializeField]    private bool isPaused = false;                  //game pausado

    private bool levelChanged = false;

    [SerializeField]    private float auxDistanceTravelled;             
    [SerializeField]    private float distanceGameLevelUP = 30;         //Distancia inicial para proximo nivel do jogo.

    private Vector2 lastPosition;                                       //medir distância
    [SerializeField]    private float travelledDistance = 0;            //guarda o valor da distancia viajada
    [SerializeField]    private float bestDistance = 0;                 //guarda o valor da distancia viajada
    private float distanciaY;
    private int selectedShip_ID = 0;                                     //qual é o ID da nave selecionada



    /* Boosters e Outros */
    [SerializeField]    private bool isInfiniteFuel = false;
    [SerializeField]    private bool isInvencible = false;
    [SerializeField]    private bool isDoubleMoney = false;
    [SerializeField]    private bool isSpecialShoot = false;
    private int quantityInvencible = 0;
    private int quantityInfiniteFuel = 0;
    private int quantityEspecialShoot = 0;

    private int levelInvencibility = 0;
    private int levelFuelInifinite = 0;
    private int levelSpecialShoot = 0;

    [SerializeField]    private float timeInvencibility = 3;
    [SerializeField]    private float timeFuelInfinite = 3;
    [SerializeField]    private float timeSpecialShoot = 3;

    private int valueBuyUpgradeInvisibility = 15;
    private int valueBuyUpgradeFuelInfinite = 15;
    private int valueBuyUpgradeSpecialShoot = 15;

    /* Upgrades */
    [SerializeField]    public int[] 
        upgradeVelocidade = new int[16],
        upgradeMovimento = new int[16],
        upgradeTanque = new int[16],
        upgradeMagnetude = new int[16],
        upgradePoder = new int[16];

    public int[] valorVelocidade = new int[16],
        valorMovimento = new int[16],
        valorTanque = new int[16],
        valorMagnetude = new int[16],
        valorPoder = new int[16];

    public int[] naveComprada = new int[16];


    public float LastSave { get; set; }
    public float PlayTimeSinceSave { get; set; }
    public float TotalPlayTime { get { return (Time.time - LastSave) + PlayTimeSinceSave; } }


    void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
            SwapScene = true;
            Debug.Log("Singleton criado");
        }
        else
        {
            Destroy(gameObject);
            _Instance.SwapScene = true;
            Debug.Log("Singleton Destruido");

        }

        DontDestroyOnLoad(gameObject);

        upgradeVelocidade = new int[16];
        upgradeMovimento = new int[16];
        upgradeTanque = new int[16];
        upgradeMagnetude = new int[16];
        upgradePoder = new int[16];
        valorMovimento = new int[16];
        valorTanque = new int[16];
        valorMagnetude = new int[16];
        valorPoder = new int[16];
        naveComprada = new int[16];

    }

    void Update()
    {

        if (SwapScene) //executa quando muda de uma cena para outra.
        {
            SceneActualName = SceneManager.GetActiveScene().name;
            
        }

        if (SceneActualName.Equals("Menu"))
        {
            if (SwapScene)
            {

                //LoadInfoGame();
                //SceneManager.LoadSceneAsync("Radio", LoadSceneMode.Additive);
                SwapScene = false;
            }
        }

        if (SceneActualName.Equals("PlayerSelect"))
        {
            if (SwapScene)
            { 
                //SceneManager.LoadSceneAsync("Radio", LoadSceneMode.Additive);
                SwapScene = false;
            }
        }

        if (SceneActualName.Equals("Game"))
        {
            if (SwapScene)      //no momento em que a cena é mudada, faz esses comandos. 
            {

                //SceneManager.LoadSceneAsync("Radio", LoadSceneMode.Additive);
                _audio = _Instance.GetComponent<AudioManager>();
                _player = GameObject.Find("Player").GetComponent<PlayerManager>();  //instanciando o player.
                LastPosition = _player.transform.position;

                /* Variaveis iniciais - Valores que devem ter ao iniciar o layout */
                gameLevel = 1;
                moneyGame = 0;
                travelledDistance = 0;
                PointsOnGame = 0;
                IsInfiniteFuel = false;
                IsInvencible = false;
                IsSpecialShoot = false;

                StartCoroutine(BeginGame()); //courotine para iniciar o jogo.

                SwapScene = false;
            }

            if (InGame) //jogo começou, jogador está jogando. 
            {
                UpdateDistanceTravelled(); //atualiza informação da distancia percorrida.
                GameLevelUP(); //controla o level do jogo.

            }

        }

    }
    void UpdateDistanceTravelled() //Atualiza distancias do jogo.
    {
        if (_player.transform.position.y > 0)
            distanciaY = _player.transform.position.y;

        TravelledDistance += Vector2.Distance(_player.transform.position, LastPosition);
        AuxDistanceTravelled += Vector2.Distance(_player.transform.position, LastPosition);

        if (!IsInfiniteFuel)
            _player.FuelActual -= Vector2.Distance(_player.transform.position, LastPosition);

        LastPosition = _player.transform.position;

    }

    void GameLevelUP() //Atualiza level do jogo. 
    {
        //Debug.Log((int)distanceTravelled_AUX % 5);

        if (AuxDistanceTravelled >= DistanceGameLevelUP)
        {
            gameLevel++;
            LevelChanged = true;

            AuxDistanceTravelled = AuxDistanceTravelled - DistanceGameLevelUP;

            DistanceGameLevelUP += ((DistanceGameLevelUP * 2) / 100);
            Debug.Log(DistanceGameLevelUP);
        }
    }

    IEnumerator BeginGame() //IEnumerator que conta o tempo para começar o jogo. 
    {
        yield return new WaitForSeconds(2);
        InGame = true;


    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void EndGame()
    {
        TotalMoneyGame += MoneyGame;
        InGame = false;

        if (TravelledDistance >= BestDistance)
            BestDistance = travelledDistance;

        SaveGame(SelectedShip_ID);

        return;

    }

    /*public void SaveInfosGame()
    {
        PlayerPrefs.SetInt("TotalMoney", TotalMoneyGame);       //Dinheiro Total
        PlayerPrefs.SetInt("TotalMedals", MedalMoney);       //Dinheiro Total
        PlayerPrefs.SetFloat("BestDistance", BestDistance);       //Melhor distancia percorrida
        PlayerPrefs.SetInt("TotalPoints", TotalPointsInGame);
        PlayerPrefs.SetInt("BestLevel", BestGameLevel);


        PlayerPrefs.SetInt("QuantidadeInvencibilidade", QuantityInvencible);
        PlayerPrefs.SetInt("QuantidadeFuelInfinito", QuantityInfiniteFuel);
        PlayerPrefs.SetInt("QuantidadeSpecialShoot", QuantityEspecialShoot);

        PlayerPrefs.SetFloat("TimeInvencibilidade", TimeInvencibility);
        PlayerPrefs.SetFloat("TimeFuelInfinito", TimeFuelInfinite);
        PlayerPrefs.SetFloat("TimeEspecialShoot", TimeSpecialShoot);

        PlayerPrefs.SetInt("LevelInvencibilidade", LevelInvencibility);
        PlayerPrefs.SetInt("LevelFuel", LevelFuelInifinite);
        PlayerPrefs.SetInt("LevelShoot", LevelSpecialShoot);

        PlayerPrefs.SetInt("ValorInvencibilidade", ValueBuyUpgradeInvisibility);
        PlayerPrefs.SetInt("ValorFuel", ValueBuyUpgradeFuelInfinite);
        PlayerPrefs.SetInt("ValorShoot", ValueBuyUpgradeSpecialShoot);

        PlayerPrefs.Save();

        Debug.Log("Salva as informações principais do jogo");

    }*/

    public string SaveGame(int id)
    {

        string saveData = "";

        
        saveData += TotalPlayTime.ToString() + '|';
        LastSave = Time.time;

        //Naves Status
        for(int i = 0; i < 16; i++)
        {
            saveData +=
                naveComprada[i].ToString() + '%' +

                upgradeMagnetude[i].ToString() + '%' +
                upgradeMovimento[i].ToString() + '%' +
                upgradeTanque[i].ToString() + '%' +
                upgradeMagnetude[i].ToString() + '%' +
                upgradePoder[i].ToString() + '%' +

                valorVelocidade[i].ToString() + '%' +
                valorMovimento[i].ToString() + '%' +
                valorTanque[i].ToString() + '%' +
                valorMagnetude[i].ToString() + '%' +
                valorPoder[i].ToString() + '#';
        }

        saveData += '|';
   

        /* Info Game */

        saveData +=
            TotalMoneyGame.ToString() + '%' +
            MedalMoney.ToString() + '%' +
            BestDistance.ToString() + '%' +
            TotalPointsInGame.ToString() + '%' +
            BestGameLevel.ToString() + '%' +

            QuantityInvencible.ToString() + '%' +
            QuantityInfiniteFuel.ToString() + '%' +
            QuantityEspecialShoot.ToString() + '%' +

      
            TimeInvencibility.ToString() + '%' +
            TimeFuelInfinite.ToString() + '%' +
            TimeSpecialShoot.ToString() + '%' +

            LevelInvencibility.ToString() + '%' +
            LevelFuelInifinite.ToString() + '%' +
            LevelSpecialShoot.ToString() + '%' +
       
            ValueBuyUpgradeInvisibility.ToString() + '%' +
            ValueBuyUpgradeFuelInfinite.ToString() + '%' +
            ValueBuyUpgradeSpecialShoot.ToString() + '|';


        //Debug.Log("Informações Salvas da Nave " + PlayerPrefs.GetInt("Ship" + id + "_ID"));

        return saveData;

    }

   /* public void LoadInfoGame()
    {






        if (PlayerPrefs.HasKey("TotalMoney"))
        {
            TotalMoneyGame = PlayerPrefs.GetInt("TotalMoney");
            MedalMoney = PlayerPrefs.GetInt("TotalMedals");

            TotalPointsInGame = PlayerPrefs.GetInt("TotalPoints");
            BestDistance = PlayerPrefs.GetFloat("BestDistance");

            QuantityInvencible = PlayerPrefs.GetInt("QuantidadeInvencibilidade");
            QuantityInfiniteFuel = PlayerPrefs.GetInt("QuantidadeFuelInfinito");
            QuantityEspecialShoot = PlayerPrefs.GetInt("QuantidadeSpecialShoot");

            Debug.Log("Carregada as informações principais do jogo");
        }
        else
        {
            TotalMoneyGame = 0;
            MedalMoney = 0;

            TotalPointsInGame = 0;
            BestDistance = 0;

            QuantityInvencible = 3;
            QuantityInfiniteFuel = 3;
            QuantityEspecialShoot = 3;

            SaveInfosGame();
        }

    }*/

    public void LoadGame(string saveData)
    {

        if (saveData == "")
            return;

        string[] data = saveData.Split('|');

        //Time
        string[] misc = data[0].Split('%');
        PlayTimeSinceSave = float.Parse(misc[0]);

        //Ships
        string[] ship = data[1].Split('#');

        for(int i = 0; i < 16; i++)
        {
            string[] shipStatus = ship[i].Split('%');

            upgradeVelocidade[i] = int.Parse(shipStatus[0]);
            upgradeMovimento[i] = int.Parse(shipStatus[1]);
            upgradeTanque[i] = int.Parse(shipStatus[2]);
            upgradeMagnetude[i] = int.Parse(shipStatus[3]);
            upgradePoder[i] = int.Parse(shipStatus[4]);

            valorVelocidade[i] = int.Parse(shipStatus[5]);
            valorMovimento[i] = int.Parse(shipStatus[6]);
            valorTanque[i] = int.Parse(shipStatus[7]);
            valorMagnetude[i] = int.Parse(shipStatus[8]);
            valorPoder[i] = int.Parse(shipStatus[9]);

        }

        //Status Game
        string[] status = data[2].Split('|');

        TotalMoneyGame = int.Parse(status[0]);
        MedalMoney = int.Parse(status[1]);
        BestDistance = float.Parse(status[2]);
        TotalPointsInGame = int.Parse(status[3]);
        BestGameLevel = int.Parse(status[4]);

        QuantityInvencible = int.Parse(status[5]);
        QuantityInfiniteFuel = int.Parse(status[6]);
        QuantityEspecialShoot = int.Parse(status[7]);


        TimeInvencibility = float.Parse(status[8]);
        TimeFuelInfinite = float.Parse(status[9]);
        TimeSpecialShoot = float.Parse(status[10]);

        LevelInvencibility = int.Parse(status[11]);
        LevelFuelInifinite = int.Parse(status[12]);
        LevelSpecialShoot = int.Parse(status[13]);

        ValueBuyUpgradeInvisibility = int.Parse(status[14]);
        ValueBuyUpgradeFuelInfinite = int.Parse(status[15]);
        ValueBuyUpgradeSpecialShoot = int.Parse(status[16]);

/*
        //PlayTimeSinceSave = PlayerPrefs.GetFloat("TotalPlayTime");

        if (PlayerPrefs.HasKey("Ship" + id + "_ID"))
        {
            //naveComprada = PlayerPrefs.GetInt("Ship" + id + "_isBuy");

            UpgradeVelocidade = PlayerPrefs.GetInt("Ship" + id + "_upgradeVelocidade");
            UpgradeMovimento = PlayerPrefs.GetInt("Ship" + id + "_upgradeMovimento");
            UpgradeTanque = PlayerPrefs.GetInt("Ship" + id + "_upgradeTanque");
            UpgradeMagnetude = PlayerPrefs.GetInt("Ship" + id + "_upgradeMagnetude");
            UpgradePoder = PlayerPrefs.GetInt("Ship" + id + "_upgradePoder");

            ValorVelocidade = PlayerPrefs.GetInt("Ship" + id + "_valorVelocidade");
            ValorMovimento = PlayerPrefs.GetInt("Ship" + id + "_valorMovimento");
            ValorTanque = PlayerPrefs.GetInt("Ship" + id + "_valorTanque");
            ValorMagnetude = PlayerPrefs.GetInt("Ship" + id + "_valorMagnetude");
            ValorPoder = PlayerPrefs.GetInt("Ship" + id + "_valorPoder");

            Debug.Log("Informações Carregadas da Nave " + PlayerPrefs.GetInt("Ship" + id + "_ID"));
        }
        else
        {
            //Naves n = _n.GetItem(id);

            UpgradeVelocidade = 0;
            UpgradeMovimento = 0;
            UpgradeTanque = 0;
            UpgradeMagnetude = 0;
            UpgradePoder = 0;

            ValorVelocidade = 50;
            ValorMovimento = 50;
            ValorTanque = 50;
            ValorMagnetude = 50;
            ValorPoder = 50;

            SaveGame(id);

            Debug.Log("Não continha informações dessa nave, elas foram criadas: " + PlayerPrefs.GetInt("Ship" + id + "_ID"));
        }

        if (PlayerPrefs.HasKey("TotalMoney"))
        {
            TotalMoneyGame = PlayerPrefs.GetInt("TotalMoney");
            MedalMoney = PlayerPrefs.GetInt("TotalMedals");

            TotalPointsInGame = PlayerPrefs.GetInt("TotalPoints");
            BestDistance = PlayerPrefs.GetFloat("BestDistance");

            QuantityInvencible = PlayerPrefs.GetInt("QuantidadeInvencibilidade");
            QuantityInfiniteFuel = PlayerPrefs.GetInt("QuantidadeFuelInfinito");
            QuantityEspecialShoot = PlayerPrefs.GetInt("QuantidadeSpecialShoot");

            Debug.Log("Carregada as informações principais do jogo");
        }
        else
        {
            TotalMoneyGame = 0;
            MedalMoney = 0;

            TotalPointsInGame = 0;
            BestDistance = 0;

            QuantityInvencible = 3;
            QuantityInfiniteFuel = 3;
            QuantityEspecialShoot = 3;

            SaveInfosGame();
        }*/

    }

    public void SaveLocal()
    {
        PlayerPrefs.SetString("SkyKingsGame", SaveGame(SelectedShip_ID));
    }
    public void LoadLocal()
    {
        LoadGame(PlayerPrefs.GetString("SkyKingsGame"));
    }
}
