﻿using UnityEngine;
using System;
using System.Collections.Generic;
//gpg
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
//for encoding
using System.Text;
//for extra save ui
using UnityEngine.SocialPlatforms;
//for text, remove
using UnityEngine.UI;

public class GooglePlayServices : MonoBehaviour
{

    // public Image button;
    // public Text DEBUG;
    public static GooglePlayServices Instance { get; set; }


    private bool m_saving;
    //save name. This name will work, change it if you like.
    private static string m_saveName = "Skykings_GameSave";
    //This is the saved file. Put this in seperate class with other variables for more advanced setup. Remember to change merging, toBytes and fromBytes for more advanced setup.
    private string saveString = "";



    // Use this for initialization
    void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
            print("CLOUD INSTANCIADO");
        }
       

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
       // enables saving game progress.
       .EnableSavedGames()
       .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = false;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
    }


    public bool IsAuthenticated
    {
        get
        {
            return Social.localUser.authenticated;
        }
    }


    public void Login(string state)
    {
        if (state.Equals("Login"))
        {
            if (!IsAuthenticated)
            {
                Social.localUser.Authenticate((bool success) =>
                {
                    if (success)
                    {
                        Social.ReportProgress(SkykingsGP.achievement_welcome, 100.0f, (bool successConquista) =>
                        {
                            // handle success or failure
                        });

                        AnalitycsManager.Instance.NomeUsuario = GameManager._Instance.nomeGoogle = ((PlayGamesLocalUser)Social.localUser).userName;
                    }
                });
            }
        }
        else if (state.Equals("Logout"))
            if (IsAuthenticated)
                PlayGamesPlatform.Instance.SignOut();
    }


    public void ShowConquistas()
    {
        if (IsAuthenticated)
        {
            Social.ShowAchievementsUI();
        }
    }

    private void ProcessCloudData(byte[] cloudData)
    {
        if (cloudData == null)
        {
            Debug.Log("No data saved to the cloud yet...");
            GameManager._Instance.LoadLocal();

            return;
        }

        Debug.Log("Decoding cloud data from bytes.");

        string progress = FromBytes(cloudData);

        Debug.Log("Merging with existing game progress.");

        GameManager._Instance.LoadGame(progress); //Carregando no JOGO


        //MergeWith(progress);
    }

    //load save from cloud
    public void LoadFromCloud()
    {

        if (IsAuthenticated)
        {
            Debug.Log("Loading game progress from the cloud. CONECTADO");
            m_saving = false;

            //DEBUG.text = "LOAD SAVE CLOUD";

            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(
                m_saveName, //name of file.
                DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime,
                SavedGameOpened);

        }
        else
        {
            Debug.Log("Carregando progresso LOCALMENTE >  Nao esta Conectado");
            GameManager._Instance.LoadLocal();
        }
    }

    //overwrites old file or saves a new one
    public void SaveToCloud()
    {
        //saveString = GameManager._Instance.SaveGame();

        if (IsAuthenticated)
        {
            saveString = GameManager._Instance.SaveGame();

            Debug.Log("Saving progress to the cloud... filename: " + m_saveName);
            //PopupManager.Instance.ShowPopUP("Save on Cloud", "Game "+ m_saveName + " is saved!");
            m_saving = true;

            //GameManager._Instance.SaveLocal();

            //save to named file
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(
                m_saveName, //name of file. If save doesn't exist it will be created with this name
                DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime,
                SavedGameOpened);

            //DEBUG.text = "NUVEM";
        }
        else
        {
            Debug.Log("Not authenticated! NAO SALVOU NUVEM, Só Local");
            //PopupManager.Instance.ShowPopUP("Save on Cloud", "Game " + m_saveName + " not saved! Saved  in Local");
            //DEBUG.text = "LOCAL";
            //GameManager._Instance.SaveLocal();
        }
    }

    //save is opened, either save or load it.
    private void SavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        //check success


        if (status == SavedGameRequestStatus.Success)
        {
            //saving
            if (m_saving)
            {
                //read bytes from save
                byte[] data = ToBytes();
                TimeSpan playedTime = TimeSpan.FromSeconds(GameManager._Instance.TotalPlayTime);
                //create builder. here you can add play time, time created etc for UI.
                SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder()
                    .WithUpdatedPlayedTime(playedTime)
                    .WithUpdatedDescription("Saved game at: " + DateTime.Now);

                SavedGameMetadataUpdate updatedMetadata = builder.Build();
                //saving to cloud
                ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(game, updatedMetadata, data, SavedGameWritten);

            }
            else//loading
            {
                ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(game, SavedGameLoaded);

            }
            //error
        }
        else
        {
            Debug.LogWarning("Error opening game: " + status);
            //PopupManager.Instance.ShowPopUP("Open on Cloud", "Game not Opene: " + status);
        }
    }

    //callback from SavedGameOpened. Check if loading result was successful or not.
    private void SavedGameLoaded(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            Debug.Log("SaveGameLoaded, success=" + status);
            ProcessCloudData(data);

            // PopupManager.Instance.ShowPopUP("Load on Cloud", "Status: " + status);
        }
        else
        {
            Debug.LogWarning("Error reading game: " + status);
            // PopupManager.Instance.ShowPopUP("Load on Cloud", "Status: " + status);
        }
    }

    //callback from SavedGameOpened. Check if saving result was successful or not.
    private void SavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            Debug.Log("Game " + game.Description + " written");
            // PopupManager.Instance.ShowPopUP("SavedGameWritten", "Game " + game.Description + " written");
        }
        else
        {
            // PopupManager.Instance.ShowPopUP("SavedGameWritten", "Error saving game: " + status);
            Debug.LogWarning("Error saving game: " + status);
        }
    }

    //merge local save with cloud save. Here is where you change the merging betweeen cloud and local save for your setup.
    private void MergeWith(string other)
    {
        if (other != "")
        {
            saveString = other;
        }
        else
        {
            Debug.Log("Loaded save string doesn't have any content");
        }
    }

    //return saveString as bytes
    private byte[] ToBytes()
    {
        byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(saveString);
        //byte[] bytes = Encoding.UTF8.GetBytes(saveString);
        return bytes;
    }

    //take bytes as arg and return string
    private string FromBytes(byte[] bytes)
    {
        string decodedString = System.Text.ASCIIEncoding.ASCII.GetString(bytes);
        //string decodedString = Encoding.UTF8.GetString(bytes);
        return decodedString;
    }

    // -------------------- ### Extra UI for testing ### -------------------- 

    //call this with Unity button to view all saves on GPG. Bug: Can't close GPG window for some reason without restarting.
    public void showUI()
    {
        // user is ILocalUser from Social.LocalUser - will work when authenticated
        ShowSaveSystemUI(Social.localUser, (SelectUIStatus status, ISavedGameMetadata game) =>
        {
            // do whatever you need with save bundle
        });
    }
    //displays savefiles in the cloud. This will only include one savefile if the m_saveName hasn't been changed
    private void ShowSaveSystemUI(ILocalUser user, Action<SelectUIStatus, ISavedGameMetadata> callback)
    {
        uint maxNumToDisplay = 3;
        bool allowCreateNew = true;
        bool allowDelete = true;

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        if (savedGameClient != null)
        {
            savedGameClient.ShowSelectSavedGameUI(user.userName + "\u0027s saves",
                maxNumToDisplay,
                allowCreateNew,
                allowDelete,
                (SelectUIStatus status, ISavedGameMetadata saveGame) =>
                {
                    // some error occured, just show window again
                    if (status != SelectUIStatus.SavedGameSelected)
                    {
                        ShowSaveSystemUI(user, callback);
                        return;
                    }

                    if (callback != null)
                        callback.Invoke(status, saveGame);
                });
        }
        else
        {
            // this is usually due to incorrect APP ID
            Debug.LogError("Save Game client is null...");
        }
    }

    public void PostScore(int points, string placar)
    {
        Social.ReportScore(points, placar, (bool success) =>
        {
            // handle success or failure
        });
    }

    public void UnlockAchievements(string acv)
    {
        Social.ReportProgress(acv, 100.0f, (bool success) =>
        {
            // handle success or failure
        });
    }

    public void ShowAllScores() { Social.ShowLeaderboardUI(); }
    public void ShowNormalScore() { PlayGamesPlatform.Instance.ShowLeaderboardUI(SkykingsGP.leaderboard_world_championship); }
    public void ShowVIPScore() { PlayGamesPlatform.Instance.ShowLeaderboardUI(SkykingsGP.leaderboard_vip_championship); }
}

