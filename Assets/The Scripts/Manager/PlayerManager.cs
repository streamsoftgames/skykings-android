﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{

    #region Encapsulamento

   

    public float FuelMax
    {
        get
        {
            return fuelMax;
        }

        set
        {
            fuelMax = value;
        }
    }

    public float FuelActual
    {
        get
        {
            return fuelActual;
        }

        set
        {
            fuelActual = value;
        }
    }

    public int Poder
    {
        get
        {
            return poder;
        }

        set
        {
            poder = value;
        }
    }

    public float MagnetudePadrao
    {
        get
        {
            return magnetudePadrao;
        }

        set
        {
            magnetudePadrao = value;
        }
    }

    public int PlayerLife
    {
        get
        {
            return playerLife;
        }

        set
        {
            playerLife = value;
        }
    }

	public int GoSpeed
	{
		get
		{
			return goSpeed;
		}

		set
		{
			goSpeed = value;
		}
	}


    #endregion

    /* Objetos que serão instanciados */
    public VirtualJoystick joystick;
    public GameObject InitialPos;
    public GameObject shoot;
    public GameObject specialShoot;
    //public GameObject boosterBar;
    public GameObject forceField;

    //public Image barBooster;

    private UIManager uiManager;

    private bool animationChanged = false;

    private NavesManager naveData;
    public PlayerAudio _audio;

    //Status Padrão
    [SerializeField]    private float horizontalVelocity = 0f;
    [SerializeField]    private float verticalVelocity = 0f;
    [SerializeField]    private float horizontalVelocityTotal = 0f;
    [SerializeField]    private float verticalVelocityTotal = 0f;
    [SerializeField]    private float magnetudePadrao = 0f;

    [SerializeField]    private float fuelMax = 0f;
    [SerializeField]    private float fuelActual = 0f;

    [SerializeField]    private int poder = 1;

    [SerializeField]    private float divisionBrake = 1f;
    [SerializeField]    private float multiplicationAceleration = 1;

    [SerializeField]    private int playerLife = 1;
    //fim

    //Aprimoramentos
    public float aprimoramentoVelocidade = 0;
    public float aprimoramentoMovimento = 0;
    public float aprimoramentoTanqueMaximo = 0;
    public float aprimoramentoMagnetude = 0;
    public int aprimoramentoPoder = 0;
    //fim


    private Rigidbody2D _rigibody2d;
    private Animator _animator;
    //private string direction = "null";

    public float timeBoosterActual;

    public GameObject painelGameOver;

    [SerializeField] private int goSpeed = 1;

	public TutorialCenarioManager tutorialManager;


    void Awake()
    {

        //Instanciando elementos
        _rigibody2d = gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
        naveData = NavesManager.Instance;
        _audio = GetComponent<PlayerAudio>();
        uiManager = Camera.main.GetComponent<UIManager>();
        //fim

        //transform.position = InitialPos.transform.position; //deixa o player na posição inicial desejada.

        DefineShipInitial();
    }

    void Start()
    {

        //Carregar infos do JSON
        Naves n = naveData.GetItem(GameManager._Instance.SelectedShip_ID);

        aprimoramentoVelocidade = GameManager._Instance.upgradeVelocidade[n.ID];
        aprimoramentoMovimento = GameManager._Instance.upgradeMovimento[n.ID];
        aprimoramentoTanqueMaximo = GameManager._Instance.upgradeTanque[n.ID];
        aprimoramentoMagnetude = GameManager._Instance.upgradeMagnetude[n.ID];
        aprimoramentoPoder = GameManager._Instance.upgradePoder[n.ID];

        MagnetudePadrao = n.MagnetudeInicial;

        horizontalVelocity = n.MovimentoInicial + aprimoramentoMovimento;
        verticalVelocity = n.VelocidadeInicial + aprimoramentoVelocidade;
        FuelMax = n.TanqueTotalInicial + aprimoramentoTanqueMaximo;
        Poder = n.PoderInicial + aprimoramentoPoder;

        GetComponent<SpriteRenderer>().sprite = n.SpriteIcon;
        
        //fim

        FuelActual = FuelMax;

        goSpeed = 1;

       
    }


    void Update()
    {
        
                
        if(transform.position.y >= InitialPos.transform.position.y && PlayerLife > 0)
        {
            GameManager._Instance.IsIntro = false;
            GameManager._Instance.InGame = true;
        }

		if (GameManager._Instance.IsIntro)
		{

			if (GameManager._Instance.IsTutorial) 
			{
				tutorialManager = GameObject.Find ("Tutorial Manager").GetComponent<TutorialCenarioManager> ();
				print ("TUTORIAL INSTANCIADO");
			} 

			verticalVelocityTotal = (Time.deltaTime * verticalVelocity) * multiplicationAceleration;
			_rigibody2d.velocity = new Vector2 (0, goSpeed * verticalVelocityTotal);
		}
        
        if (GameManager._Instance.InGame)
        {
            
            horizontalVelocityTotal = (Time.deltaTime * horizontalVelocity);
            verticalVelocityTotal = (Time.deltaTime * verticalVelocity) * multiplicationAceleration;

            _rigibody2d.velocity = new Vector2(
                (joystick.Horizontal() * horizontalVelocityTotal),
                (goSpeed * verticalVelocityTotal) / divisionBrake);

        }
        


        /*Campo de Força*/
        if (GameManager._Instance.IsInvencible) { forceField.SetActive(true); }
        else { forceField.SetActive(false); }


        

        if (joystick.Horizontal() < 0)
            _animator.SetTrigger("Left");
        else if (joystick.Horizontal() > 0)
            _animator.SetTrigger("Right");
        else if (joystick.Horizontal() == 0)
            _animator.SetTrigger("Idle");

        //print(_rigibody2d.velocity.x + "  " + _rigibody2d.velocity.y);

        //_animator.SetFloat("Movimento", joystick.Horizontal());

        //Breque da nave.
        if (joystick.VerticalBrake())
            divisionBrake = 2;
        else
            divisionBrake = 1;

        //Acelerador da nave
        if (joystick.VerticalAceleration())
            multiplicationAceleration = 1.5f;
        else
            multiplicationAceleration = 1f;


        #region Controle Teclado

        if (Input.GetKeyDown(KeyCode.Space)) //Atirar
        {
            Shoot();
        }

        #endregion

        

        //Morte
        if (PlayerLife == 0 || (FuelActual >= -1 && FuelActual <= 0))
        {

            _rigibody2d.velocity = new Vector2(0, 0);
            GetComponent<EdgeCollider2D>().enabled = false;

            goSpeed = 0;
            // GameManager._Instance.GameOver();

            _animator.SetBool("Morte", true);
            _audio.PlaySFX("Explosion");

            GameManager._Instance.EndGame();
            
           // GameManager._Instance.ShowAd();

            PlayerLife = -1;
            FuelActual = -2;
        }
        
    }

   

    public void Shoot()
    {
        if (GameManager._Instance.InGame)
        {
            if (!GameManager._Instance.IsSpecialShoot)
            {
                this.transform.GetChild(0).rotation = Quaternion.Euler(0, 0, 0);
                Instantiate(shoot, transform.Find("Shoot").position, Quaternion.identity);
                _audio.PlaySFX("Shoot");

                
            }
            else
            {
                _audio.PlaySFX("Special");

                for (int i = 0; i < 3; i++)
                {
                    GameObject shootAux;
                    shootAux = Instantiate(specialShoot, transform.Find("Shoot").position, Quaternion.identity) as GameObject;
                    if(i == 0)
                    {
                        shootAux.GetComponent<Rigidbody2D>().velocity = new Vector2(0.28f, 1) * (370f * Time.deltaTime);
                    }
                    else if(i == 1)
                    {
                        shootAux.GetComponent<Rigidbody2D>().velocity = new Vector2(shootAux.GetComponent<Rigidbody2D>().velocity.x, 1 * (Time.deltaTime * 370f));
                    }
                    else if(i == 2)
                    {
                        shootAux.GetComponent<Rigidbody2D>().velocity = new Vector2(-0.28f, 1) * (370f * Time.deltaTime);

                    }

                }
            }
        }
    }

    void DefineShipInitial()
    {
        for (int i = 0; i < _animator.layerCount; i++)
        {
            if (i == GameManager._Instance.SelectedShip_ID)
            {
                _animator.SetLayerWeight(i, 1);
                animationChanged = true;
            }

            else
                _animator.SetLayerWeight(i, 0);
        }

    }

    #region Colisores
    void OnCollisionEnter2D(Collision2D col) //Tratar colisão com o player.
    {
        //Tirar Vida do Personagem
        if (col.gameObject.CompareTag("Cenario") || col.gameObject.CompareTag("ShootEnemy") || col.gameObject.CompareTag("Enemys"))
        {
            if (!GameManager._Instance.IsInvencible)
                PlayerLife = 0;

            if (col.gameObject.CompareTag("ShootEnemy"))
            {
                Destroy(col.gameObject);
            }
                
        }

        if (col.gameObject.CompareTag("Coin"))
        {
            _audio.PlaySFX("Coin");
        }
        if (col.gameObject.CompareTag("Fuel"))
        {
            _audio.PlaySFX("Item");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Coin") || col.gameObject.CompareTag("Medal"))
        {
            _audio.PlaySFX("Coin");
        }
        if (col.gameObject.CompareTag("Fuel"))
        {
            _audio.PlaySFX("Item");
        }

		/* GATILHOS TUTORIAL */
		if (col.gameObject.name.Equals("Gatilho Movimento") && tutorialManager.estagioTutorial == 0)
		{
			goSpeed = 0;
			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 1;
		}

		if (col.gameObject.name.Equals("Gatilho Movimento Esquerda") && tutorialManager.estagioTutorial == 1.0f)
		{
			goSpeed = 0;
			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 1.1f;
		}
		if (col.gameObject.name.Equals("Gatilho Movimento Direita") && tutorialManager.estagioTutorial == 1.1f)
		{
			goSpeed = 0;
			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 1.2f;
		}
		if (col.gameObject.name.Equals("Gatilho Movimento Centro") && tutorialManager.estagioTutorial == 1.2f)
		{
			goSpeed = 1;

			//tutorialManager.estagioTutorial = 4;
		}

		if (col.gameObject.name.Equals("Gatilho Movimento Cima") && tutorialManager.estagioTutorial == 1.2f)
		{
			
			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 1.3f;
		}

		/* TIRO */
		if (col.gameObject.name.Equals("Gatilho Tiro") && tutorialManager.estagioTutorial == 1.3f)
		{
			print ("TIRO GATILHO");
			goSpeed = 0;
			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 2;
		}

		if (col.gameObject.name.Equals("Gatilho HUD") && tutorialManager.estagioTutorial == 2)
		{
			print ("HUD GATILHO");

			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 3;
		}
		if (col.gameObject.name.Equals("Gatilho Gasolina") )
		{
			print ("ITEM GATILHO");

			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 4;
		}

		if (col.gameObject.name.Equals("Gatilho Booster"))
		{
			print ("ITEM GATILHO");

			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 5;
		}
		if (col.gameObject.name.Equals("Gatilho Fim"))
		{
			print ("FIM GATILHO");

			tutorialManager.isOK = true;
			tutorialManager.estagioTutorial = 6;
		}
    }

    #endregion

    public void PlayerSelect()
    {
        SceneManager.LoadScene("2 - Menu");
    }
    public void RestartLevel()
    {
        painelGameOver.SetActive(true);
    }
    public void GameOverTeste()
    {
        SceneManager.LoadScene("5 - Game");
    }

    //Boosters
    public void Invencible()
    {
        if (!GameManager._Instance.IsInfiniteFuel && !GameManager._Instance.IsInvencible && !GameManager._Instance.IsSpecialShoot)
        {
            if (GameManager._Instance.QuantityInvencible > 0)
            {
                StartCoroutine(IeInvencible());
                GameManager._Instance.QuantityInvencible--;
                AnalitycsManager.Instance.TotalBoosterInvencibilidadeUsados++;

                GameManager._Instance.PontosGanhosnoJogo += 500;
            }
            //else
                //uiManager.InvencibilityButton.GetComponent<Image>().color = Color.red;
        }
    }
    public void FuelInfinite()
    {
        if (!GameManager._Instance.IsInfiniteFuel && !GameManager._Instance.IsInvencible && !GameManager._Instance.IsSpecialShoot)
        {
            if (GameManager._Instance.QuantityInfiniteFuel > 0)
            {
                StartCoroutine(IeFuelCharge());
                GameManager._Instance.QuantityInfiniteFuel--;
                AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaUsados++;

                GameManager._Instance.PontosGanhosnoJogo += 500;
            }
            //else
                //uiManager.FuelInfiniteButton.GetComponent<Image>().color = Color.red;
        }
    }
    public void SpecialShoot()
    {
        if (!GameManager._Instance.IsInfiniteFuel && !GameManager._Instance.IsInvencible && !GameManager._Instance.IsSpecialShoot)
        {

            if (GameManager._Instance.QuantityEspecialShoot > 0)
            {
                StartCoroutine(IeSpecialShoot());
                GameManager._Instance.QuantityEspecialShoot--;
                AnalitycsManager.Instance.TotalBoosterTiroEspecialUsados++;

                GameManager._Instance.PontosGanhosnoJogo += 500;
            }
            //else
                //uiManager.SpecialShootButton.GetComponent<Image>().color = Color.red;
        }
    }

    IEnumerator IeInvencible()
    {

        GameManager._Instance.IsInvencible = true;
        Debug.Log(GameManager._Instance.IsInvencible);
        uiManager.InvencibilityButton.GetComponent<Image>().color = Color.green;

        yield return new WaitForSeconds(GameManager._Instance.TimeInvencibility);

        uiManager.InvencibilityButton.GetComponent<Image>().color = Color.white;
        GameManager._Instance.IsInvencible = false;
        Debug.Log(GameManager._Instance.IsInvencible);

    }
    IEnumerator IeFuelCharge()
    {

        GameManager._Instance.IsInfiniteFuel = true;
        Debug.Log(GameManager._Instance.IsInfiniteFuel);
        uiManager.FuelInfiniteButton.GetComponent<Image>().color = Color.green;

        yield return new WaitForSeconds(GameManager._Instance.TimeFuelInfinite);

        uiManager.FuelInfiniteButton.GetComponent<Image>().color = Color.white;
        GameManager._Instance.IsInfiniteFuel = false;
        Debug.Log(GameManager._Instance.IsInfiniteFuel);

    }
    IEnumerator IeSpecialShoot()
    {

        GameManager._Instance.IsSpecialShoot = true;
        Debug.Log(GameManager._Instance.IsSpecialShoot);
        uiManager.SpecialShootButton.GetComponent<Image>().color = Color.green;

        yield return new WaitForSeconds(GameManager._Instance.TimeSpecialShoot);

        uiManager.SpecialShootButton.GetComponent<Image>().color = Color.white;
        GameManager._Instance.IsSpecialShoot = false;
        Debug.Log(GameManager._Instance.IsSpecialShoot);

    }


    public void GANHECOMADS()
    {
        AdsManager.Instance.ShowAdsRecompensa();

    }
}
