﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class InfosPlayerManager : MonoBehaviour
{

    public static InfosPlayerManager instance;

    

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        
    }
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }
    /*
        public void Save(int ID)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/Perrengue" + ID + ".topzera");

            ShipInfo s = new ShipInfo();

            s.ID = gm.SelectedShip_ID;
            s.ISBUY = gm.NaveComprada;

            s.UP_VELOCIDADE = gm.UpgradeVelocidade;
            s.UP_MOVIMENTO = gm.UpgradeMovimento;
            s.UP_TANQUE = gm.UpgradeTanque;
            s.UP_MAGNETUDE = gm.UpgradeMagnetude;
            s.UP_PODER = gm.UpgradePoder;

            s.VALOR_VELOCIDADE = gm.ValorVelocidade;
            s.VALOR_MOVIMENTO = gm.ValorMovimento;
            s.VALOR_TANQUE = gm.ValorTanque;
            s.VALOR_MAGNETUDE = gm.ValorMagnetude;
            s.VALOR_PODER = gm.ValorPoder;


            bf.Serialize(file, s);
            file.Close();

        }

        public void Load(int ID)
        {
            if (File.Exists(Application.persistentDataPath + "/Perrengue" + ID + ".topzera"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/Perrengue" + ID + ".topzera", FileMode.Open);
                ShipInfo s = (ShipInfo)bf.Deserialize(file);

                file.Close();

                gm.UpgradeVelocidade = s.UP_VELOCIDADE;
                gm.UpgradeMovimento = s.UP_MOVIMENTO;
                gm.UpgradeTanque = s.UP_TANQUE;
                gm.UpgradeMagnetude = s.UP_MAGNETUDE;
                gm.UpgradePoder = s.UP_PODER;

                gm.ValorVelocidade = s.VALOR_VELOCIDADE;
                gm.ValorMovimento = s.VALOR_MOVIMENTO;
                gm.ValorTanque = s.VALOR_TANQUE;
                gm.ValorMagnetude = s.VALOR_MAGNETUDE;
                gm.ValorPoder = s.VALOR_PODER;

            }
            else
            {

                gm.UpgradeVelocidade = 0;
                gm.UpgradeMovimento = 0;
                gm.UpgradeTanque = 0;
                gm.UpgradeMagnetude = 0;
                gm.UpgradePoder = 0;

                gm.ValorVelocidade = 50;
                gm.ValorMovimento = 50;
                gm.ValorTanque = 50;
                gm.ValorMagnetude = 50;
                gm.ValorPoder = 50;


                Save(ID);

                Debug.Log("Não continha informações dessa nave, elas foram criadas: Perrengue" + ID + ".topzera");
            }
        }
    }*/
}

[Serializable]
public class ShipInfo
{

    public int ID;
    public int ISBUY;

    public int UP_VELOCIDADE;
    public int UP_MOVIMENTO;
    public int UP_TANQUE;
    public int UP_MAGNETUDE;
    public int UP_PODER;

    public int VALOR_VELOCIDADE;
    public int VALOR_MOVIMENTO;
    public int VALOR_TANQUE;
    public int VALOR_MAGNETUDE;
    public int VALOR_PODER;


}
