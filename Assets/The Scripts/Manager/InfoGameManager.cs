﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class InfoGameManager : MonoBehaviour
{

    public static InfoGameManager instance;

    private GameManager gm;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        gm = GameManager._Instance;
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Moderninha.topzera");

        GameInfos g = new GameInfos();

        g.TOTAL_MONEY = gm.MoneyGame;
        g.TOTAL_MEDALS = gm.MedalMoney;
        g.TOTAL_POINTS = gm.TotalPontosGeral;
        g.BEST_DISTANCE = gm.BestDistance;
        g.BEST_GAMELEVEL = gm.BestGameLevel;

        g.QUANTIDADE_BOOSTER_FUEL = gm.QuantityInfiniteFuel;
        g.QUANTIDADE_BOOSTER_INVENCIVEL = gm.QuantityInvencible;
        g.QUANTIDADE_BOOSTER_SPECIAL = gm.QuantityEspecialShoot;

        g.BOOSTER_TIME_FUEL = gm.TimeFuelInfinite;
        g.BOOSTER_TIME_INVENCIVEL = gm.TimeInvencibility;
        g.BOOSTER_TIME_SPECIAL = gm.TimeSpecialShoot;

        g.BOOSTER_LEVEL_FUEL = gm.LevelFuelInifinite;
        g.BOOSTER_LEVEL_INVENCIVEL = gm.LevelInvencibility;
        g.BOOSTER_LEVEL_SPECIAL = gm.LevelSpecialShoot;

        g.BOOSTER_VALOR_FUEL = gm.ValueBuyUpgradeFuelInfinite;
        g.BOOSTER_VALOR_INVENCIVEL = gm.ValueBuyUpgradeInvisibility;
        g.BOOSTER_VALOR_SPECIAL = gm.ValueBuyUpgradeSpecialShoot;

        bf.Serialize(file, g);
        file.Close();
    }
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/Moderninha.topzera"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Moderninha.topzera", FileMode.Open);
            GameInfos g = (GameInfos)bf.Deserialize(file);

            file.Close();

            gm.TotalMoneyGame = g.TOTAL_MONEY;
            gm.MedalMoney = g.TOTAL_MEDALS;
            gm.TotalPontosGeral = g.TOTAL_POINTS;
            gm.BestDistance = g.BEST_DISTANCE;
            gm.BestGameLevel = g.BEST_GAMELEVEL;

            gm.QuantityInvencible = g.QUANTIDADE_BOOSTER_INVENCIVEL;
            gm.QuantityInfiniteFuel = g.QUANTIDADE_BOOSTER_FUEL;
            gm.QuantityEspecialShoot = g.QUANTIDADE_BOOSTER_SPECIAL;

            gm.TimeFuelInfinite = g.BOOSTER_TIME_FUEL;
            gm.TimeInvencibility = g.BOOSTER_TIME_INVENCIVEL;
            gm.TimeSpecialShoot = g.BOOSTER_TIME_SPECIAL;

            gm.LevelFuelInifinite = g.BOOSTER_LEVEL_FUEL;
            gm.LevelInvencibility = g.BOOSTER_LEVEL_INVENCIVEL;
            gm.LevelSpecialShoot = g.BOOSTER_LEVEL_SPECIAL;

            gm.ValueBuyUpgradeFuelInfinite = g.BOOSTER_VALOR_FUEL;
            gm.ValueBuyUpgradeInvisibility = g.BOOSTER_VALOR_INVENCIVEL;
            gm.ValueBuyUpgradeSpecialShoot = g.BOOSTER_VALOR_SPECIAL;

        }
        else
        {

            gm.TotalMoneyGame = 0;
            gm.MedalMoney = 0;

            gm.TotalPontosGeral = 0;
            gm.BestDistance = 0;

            gm.QuantityInvencible = 3;
            gm.QuantityInfiniteFuel = 3;
            gm.QuantityEspecialShoot = 3;

            gm.TimeFuelInfinite = 3;
            gm.TimeInvencibility = 3;
            gm.TimeSpecialShoot = 3;

            gm.LevelFuelInifinite = 0;
            gm.LevelInvencibility = 0;
            gm.LevelSpecialShoot = 0;

            gm.ValueBuyUpgradeFuelInfinite = 15;
            gm.ValueBuyUpgradeInvisibility = 15;
            gm.ValueBuyUpgradeSpecialShoot = 15;


            Save();

            Debug.Log("Não continha save do jogo geral");
        }
    }
}

[Serializable]
public class GameInfos
{
    public int TOTAL_MONEY;
    public int TOTAL_MEDALS;
    public int TOTAL_POINTS;
    public int BEST_GAMELEVEL;
    public float BEST_DISTANCE;

    public int QUANTIDADE_BOOSTER_INVENCIVEL;
    public int QUANTIDADE_BOOSTER_FUEL;
    public int QUANTIDADE_BOOSTER_SPECIAL;

    public float BOOSTER_TIME_INVENCIVEL;
    public float BOOSTER_TIME_FUEL;
    public float BOOSTER_TIME_SPECIAL;

    public int BOOSTER_LEVEL_INVENCIVEL;
    public int BOOSTER_LEVEL_FUEL;
    public int BOOSTER_LEVEL_SPECIAL;

    public int BOOSTER_VALOR_INVENCIVEL;
    public int BOOSTER_VALOR_FUEL;
    public int BOOSTER_VALOR_SPECIAL;
}
