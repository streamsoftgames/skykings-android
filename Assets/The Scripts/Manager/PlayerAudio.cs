﻿using UnityEngine;
using System.Collections;

public class PlayerAudio : MonoBehaviour {

    AudioSource _audio;
    public AudioClip shoot;
    public AudioClip shootSpecial;
    public AudioClip explosion;
    public AudioClip getItem;
    public AudioClip getCoin;
    

    void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }
	

    public void PlaySFX(string situation)
    {
        if (situation.Equals("Shoot"))
            _audio.clip = shoot;
        else if (situation.Equals("Explosion"))
            _audio.clip = explosion;
        else if (situation.Equals("Special"))
            _audio.clip = shootSpecial;
        else if (situation.Equals("Item"))
            _audio.clip = getItem;
        else if (situation.Equals("Coin"))
            _audio.clip = getCoin;

        if (_audio.isPlaying)
        {
            _audio.Stop();
            _audio.Play();
        }
        else
            _audio.Play();
    }
}
