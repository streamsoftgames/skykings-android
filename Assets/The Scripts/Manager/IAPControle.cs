﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System.Collections;

public class IAPControle : MonoBehaviour {

    public Text preco_dobroMoeda;
    public Text preco_dobroMedalha;
    public Text preco_coin1;
    public Text preco_coin2;
    public Text preco_coin3;
    public Text preco_medalha1;
    public Text preco_medalha2;
    public Text preco_master;
    public Text preco_ps4;


	
	// Update is called once per frame
	void Update ()
    {
        preco_dobroMoeda.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_DOUBLECOIN);
        preco_dobroMedalha.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_DOUBLEMEDAL);

        preco_coin1.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTECOIN_1);
        preco_coin2.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTECOIN_2);
        preco_coin3.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTECOIN_3);

        preco_medalha1.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTEMEDAL_1);
        preco_medalha2.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTEMEDAL_2);

        preco_master.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PACOTEMASTER);

        preco_ps4.text = IAPManager.Instance.GetPrice(IAPManager.PRODUTO_PS4);
    }
}
