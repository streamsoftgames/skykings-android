﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelManager : MonoBehaviour
{

   [SerializeField] private Sprite[] 
        Deserto,
        Neve,
        Estrada,
        atualCenario;

    public GameObject[] transitions;

    private GameObject[] 
        spawnPointEnemy_Air,
        spawnPointEnemy_Water,
        spawnPointEnemy_Tank,
        itemSpawnPoint;

    public GameObject[] 
        enemies_air,
        enemies_tank,
        enemies_water,
        itens;

    public GameObject medal;

    public bool[]
        pointSpawned_Air,
        pointSpawned_Water,
        pointSpawned_Tank,
        ItemIsSpawned;

    public GameObject[] scenarios;

    public Transform scenaryGeneratePoint;
    private GameObject nextScenaryAux;

    private int
        maxAirEnemies = 0,
        maxTanksEnemies = 0,
        maxWaterEnemies = 0,
        maxItens = 3;
        

    private float scenarySize;

    [SerializeField] private int
        levelAux = 0,
        transitionLevel = 3;

    public string nomeAnteriorCenario;
    public string nomeLevelAtual;
    
    public string[] nomesLeveis = new string[3] { "Neve", "Estrada", "Deserto" };
    public GameObject actualTransitionScenary;

	public GameObject primeiroNivel;
    


    void Start()
    {

        //Define qual é o primeiro cenario
        nextScenaryAux = GameObject.Find("Cenario_base_0");
        scenarySize = GetHeight(nextScenaryAux);
        //SpawnPoints(nextScenaryAux, 0, 0, 0, 0);
        transform.position = new Vector2(transform.position.x, transform.position.y + scenarySize);

        //inicializa os vetores que guardarão o cenarios.
        //Deserto = new Sprite[16];
        //Estrada = new Sprite[16];
        //Neve = new Sprite[16];
        //atualCenario = new Sprite[16]; //Guarda a imagem do atual cenario do jogo. 

        transitionLevel = 3;

        //Carrega nos arrays todos os cenarios.
        /* for (int i = 0; i < 16; i++)
         {
             Deserto[i] = Resources.Load<Sprite>("Cenario/Deserto/Base " + i);
             Estrada[i] = Resources.Load<Sprite>("Cenario/Estrada/Base " + i);
             Neve[i] = Resources.Load<Sprite>("Cenario/Neve/Bases " + i);
         }*/


        string aux = nomesLeveis [Random.Range (0, nomesLeveis.Length)];

		switch (aux) 
		{
		case "Neve":
            atualCenario = Neve;
            primeiroNivel.GetComponent<SpriteRenderer>().sprite = Neve [0];
			nomeAnteriorCenario = nomeLevelAtual = "Neve";  
			break;

		case "Deserto":
            atualCenario = Deserto; //Deserto;
			primeiroNivel.GetComponent<SpriteRenderer>().sprite = Deserto [0]; //Deseto [0];
			nomeAnteriorCenario = nomeLevelAtual = "Deserto";  
			break;

		case "Estrada":
            atualCenario = Estrada;// Estrada;
            primeiroNivel.GetComponent<SpriteRenderer>().sprite = Estrada [0];
			nomeAnteriorCenario = nomeLevelAtual = "Estrada";  
			break;
		}

    }

    void Update()
    {

        /* Se cenario mudar, upa o level do jogo. */
        if (GameManager._Instance.LevelChanged)
        {
            levelAux++;
            GameManager._Instance.LevelChanged = false;
        }

        if (transform.position.y < scenaryGeneratePoint.position.y)
        {

            if (levelAux < transitionLevel)
            {
				int aux = Random.Range(0, scenarios.Length);

                //Criar Terreno
                nextScenaryAux = Instantiate(scenarios[aux], transform.position, transform.rotation) as GameObject;
                nextScenaryAux.GetComponent<SpriteRenderer>().sprite = atualCenario[aux];

                scenarySize = GetHeight(nextScenaryAux);
                transform.position = new Vector2(transform.position.x, transform.position.y + scenarySize);


                Balanceamente();

               // SpawnPoints(nextScenaryAux, maxAirEnemies, maxWaterEnemies, maxTanksEnemies, maxItens);

            }
            else if (levelAux == transitionLevel)
            {
                Debug.Log("Transição");

                nomeAnteriorCenario = nomeLevelAtual;

                nomeLevelAtual = nomesLeveis[Random.Range(0, nomesLeveis.Length)];

                
                for (int i = 0; i < transitions.Length; i++) //
                {
                    if (transitions[i].name.Equals(nomeAnteriorCenario + "2" + nomeLevelAtual))
                    {
                        actualTransitionScenary = transitions[i];
                    }
                }


                //Se o proximo ambiente não for repetido ao anterior gera o cenario de transição de ambiente
                if (!nomeLevelAtual.Equals(nomeAnteriorCenario))
                {
                    nextScenaryAux = Instantiate(actualTransitionScenary, transform.position, transform.rotation) as GameObject;
                    Debug.Log("Transição - NORMAL");

                    GameObject medalCreated = Instantiate(medal, nextScenaryAux.transform.position, Quaternion.identity) as GameObject;
                    medalCreated.transform.parent = nextScenaryAux.transform;
                }

                else //caso seja repetido não faz nada. 
                {
                    int aux = Random.Range(0, scenarios.Length);

                    //Criar Terreno
                    nextScenaryAux = Instantiate(scenarios[aux], transform.position, transform.rotation) as GameObject;
                    nextScenaryAux.GetComponent<SpriteRenderer>().sprite = atualCenario[aux];
                    Debug.Log("Transição - REPETIDO");
                }

                scenarySize = GetHeight(nextScenaryAux);
                transform.position = new Vector2(transform.position.x, transform.position.y + scenarySize);


                if (nomeLevelAtual.Equals("Neve"))
                    atualCenario = Neve; //Neve;

                else if (nomeLevelAtual.Equals("Estrada"))
                    atualCenario = Estrada; //Estrada;

                else if (nomeLevelAtual.Equals("Deserto"))
                    atualCenario = Deserto; //Deserto;

                levelAux = 0;
            }

        }
    }

    void Balanceamente()
    {
        if (GameManager._Instance.GameLevel <= 10)
        {
            if (GameManager._Instance.GameLevel == 1)
            {
                maxAirEnemies = 1;
                maxTanksEnemies = 0;
                maxWaterEnemies = 1;
                maxItens = 1;
            }
            else if (GameManager._Instance.GameLevel == 2)
            {
                maxAirEnemies = Random.Range(1, 3);
                maxTanksEnemies = 0;
                maxWaterEnemies = 2;
                maxItens = 2;
            }
            else if (GameManager._Instance.GameLevel == 3)
            {
                maxAirEnemies = Random.Range(1, 3);
                maxTanksEnemies = 0;
                maxWaterEnemies = Random.Range(1, 3);
                maxItens = 2;
            }
            else if (GameManager._Instance.GameLevel >= 4 && GameManager._Instance.GameLevel < 7)
            {
                maxAirEnemies = Random.Range(1, 3);
                maxTanksEnemies = 1;
                maxWaterEnemies = Random.Range(1, 3);
                maxItens = 2;
            }
            else if (GameManager._Instance.GameLevel >= 7 && GameManager._Instance.GameLevel <= 10)
            {
                maxAirEnemies = Random.Range(1, 3);
                maxTanksEnemies = Random.Range(0, 3);
                maxWaterEnemies = Random.Range(1, 3);
                maxItens = 2;
            }
        }
        else if (GameManager._Instance.GameLevel > 10 && GameManager._Instance.GameLevel <= 20)
        {
            maxAirEnemies = Random.Range(0, 4);
            maxTanksEnemies = Random.Range(0, 4);
            maxWaterEnemies = 2; Random.Range(0, 4);
            maxItens = Random.Range(0, 3);

        }
        else if (GameManager._Instance.GameLevel > 20 && GameManager._Instance.GameLevel <= 50)
        {
            maxAirEnemies = Random.Range(0, 5);
            maxTanksEnemies = Random.Range(0, 5);
            maxWaterEnemies = 2; Random.Range(0, 5);
            maxItens = Random.Range(0, 3);

        }
        else if (GameManager._Instance.GameLevel > 50)
        {
            maxAirEnemies = Random.Range(1, 6);
            maxTanksEnemies = Random.Range(1, 6);
            maxWaterEnemies = 2; Random.Range(1, 7);
            maxItens = Random.Range(0, 3);

        }

		SpawnPoints(nextScenaryAux, maxAirEnemies, maxWaterEnemies, maxTanksEnemies, maxItens);
    }

    float GetHeight(GameObject obj)
    {
        float aux;

        SpriteRenderer sreder = obj.GetComponent<SpriteRenderer>();
        aux = sreder.bounds.size.y;

        return aux;
    }

    void SpawnPoints(GameObject obj, int maxAir, int maxWater, int maxTank, int maxAuxItem)
    {
        spawnPointEnemy_Air = new GameObject[obj.transform.Find("Voador").childCount];
        pointSpawned_Air = new bool[spawnPointEnemy_Air.Length];

        spawnPointEnemy_Water = new GameObject[obj.transform.Find("Parado").childCount];
        pointSpawned_Water = new bool[spawnPointEnemy_Water.Length];

        spawnPointEnemy_Tank = new GameObject[obj.transform.Find("Terreo").childCount];
        pointSpawned_Tank = new bool[spawnPointEnemy_Tank.Length];

        itemSpawnPoint = new GameObject[obj.transform.Find("Item").childCount];
        ItemIsSpawned = new bool[itemSpawnPoint.Length];


        for (int i = 0; i < spawnPointEnemy_Air.Length; i++)
            spawnPointEnemy_Air[i] = obj.transform.Find("Voador").GetChild(i).gameObject;

        for (int i = 0; i < spawnPointEnemy_Water.Length; i++)
            spawnPointEnemy_Water[i] = obj.transform.Find("Parado").GetChild(i).gameObject;

        for (int i = 0; i < spawnPointEnemy_Tank.Length; i++)
            spawnPointEnemy_Tank[i] = obj.transform.Find("Terreo").GetChild(i).gameObject;

        for (int i = 0; i < itemSpawnPoint.Length; i++)
            itemSpawnPoint[i] = obj.transform.Find("Item").GetChild(i).gameObject;


        GerateAirEnemies(0, maxAir);
        GerateTankEnemies(0, maxTank);
        GerateWaterEnemies(0, maxWater);
        GerateItens(0, maxAuxItem);
    }

    void GerateAirEnemies(int cont, int max)
    {
        int aux = cont;
        int auxPos;
        int AuxLenght;

        if (max > spawnPointEnemy_Air.Length)
            AuxLenght = spawnPointEnemy_Air.Length;
        else
            AuxLenght = max;

        while (aux < AuxLenght)
        {
            auxPos = Random.Range(0, spawnPointEnemy_Air.Length);

            if (!pointSpawned_Air[auxPos])
            {
                GameObject _enemyFly = Instantiate(enemies_air[Random.Range(0, enemies_air.Length)]) as GameObject;
                _enemyFly.transform.parent = nextScenaryAux.transform;
                pointSpawned_Air[auxPos] = true;
                _enemyFly.transform.position = spawnPointEnemy_Air[auxPos].transform.position;

                if (_enemyFly.transform.position.x < 0)
                    _enemyFly.GetComponent<AirEnemyManager>().faceActual = "Right";
            }
            else
            {
                GerateAirEnemies(aux, AuxLenght);
                break;
            }

            aux++;
        }
    }

    void GerateTankEnemies(int cont, int max)
    {
        int aux = cont;
        int auxPos;
        int AuxLenght;
        //maxTanksEnemies = spawnPointEnemy_Tank.Length;

        if (max > spawnPointEnemy_Tank.Length)
            AuxLenght = spawnPointEnemy_Tank.Length;
        else
            AuxLenght = max;

        while (aux < AuxLenght)
        {
            auxPos = Random.Range(0, spawnPointEnemy_Tank.Length);

            if (!pointSpawned_Tank[auxPos])
            {

                GameObject _enemyTank = Instantiate(enemies_tank[Random.Range(0, enemies_tank.Length)], transform.position, Quaternion.identity) as GameObject;
                _enemyTank.transform.parent = nextScenaryAux.transform;
                pointSpawned_Tank[auxPos] = true;
                _enemyTank.transform.position = spawnPointEnemy_Tank[auxPos].transform.position;

            }
            else
            {
                GerateTankEnemies(aux, AuxLenght);
                break;
            }
            aux++;
        }
    }

    void GerateWaterEnemies(int cont, int max)
    {
        int aux = cont;
        int auxPos;
        int AuxLenght;
        //maxWaterEnemies = spawnPointEnemy_Water.Length;

        if (max > spawnPointEnemy_Water.Length)
            AuxLenght = spawnPointEnemy_Water.Length;
        else
            AuxLenght = max;

        while (aux < AuxLenght)
        {
            auxPos = Random.Range(0, spawnPointEnemy_Water.Length);

            if (!pointSpawned_Water[auxPos])
            {
                GameObject _enemyIdle = Instantiate(enemies_water[Random.Range(0, enemies_water.Length)]) as GameObject;
                _enemyIdle.transform.parent = nextScenaryAux.transform;
                pointSpawned_Water[auxPos] = true;
                _enemyIdle.transform.position = spawnPointEnemy_Water[auxPos].transform.position;
            }

            else
            {
                GerateWaterEnemies(aux, AuxLenght);
                break;
            }

            aux++;

        }
    }

    void GerateItens(int cont, int max)
    {
        int aux = cont;
        int auxPos;
        int AuxLenght;

        if (max > itemSpawnPoint.Length)
            AuxLenght = itemSpawnPoint.Length;
        else
            AuxLenght = max;

        while (aux < AuxLenght)
        {

            auxPos = Random.Range(0, itemSpawnPoint.Length);

            if (!ItemIsSpawned[auxPos])
            {
                GameObject _item = Instantiate(itens[Random.Range(0, itens.Length)]) as GameObject;
                _item.transform.parent = nextScenaryAux.transform;
                ItemIsSpawned[auxPos] = true;
                _item.transform.position = itemSpawnPoint[auxPos].transform.position;
            }
            else
            {
                GerateItens(aux, AuxLenght);
                break;
            }

            aux++;
        }

    }


}
