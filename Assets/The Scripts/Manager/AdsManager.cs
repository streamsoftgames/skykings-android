﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class AdsManager : MonoBehaviour
{

    public static AdsManager Instance { get; set; }
    // Define gameId as a public field
    //  so it can be set from the Inspector.
    // Serialize private fields
    //  instead of making them public.
    /* [SerializeField]    string iosGameId =  "1153441";
     [SerializeField]    string androidGameId = "1153438";*/
    [SerializeField]
    bool enableTestMode;


    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {

        enableTestMode = Advertisement.testMode;
        /* string gameId = null;

         #if UNITY_IOS // If build platform is set to iOS...
         gameId = iosGameId;
         #elif UNITY_ANDROID // Else if build platform is set to Android...
         gameId = androidGameId;
         #endif

         if (string.IsNullOrEmpty(gameId))
         { // Make sure the Game ID is set.
             Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
         }
         else if (!Advertisement.isSupported)
         {
             Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
         }
         else if (Advertisement.isInitialized)
         {
             Debug.Log("Unity Ads is already initialized.");
         }
         else
         {
             Debug.Log(string.Format("Initialize Unity Ads using Game ID: {0} - with Test Mode: {1}.",
                 gameId, enableTestMode ? "enabled" : "disabled"));
             Advertisement.Initialize(gameId, enableTestMode);
         }*/
    }


    public void ShowAdNormal()
    {
        if (Advertisement.IsReady())
            Advertisement.Show();
    }

    public void ShowAdsRecompensa()
    {
        string id = "rewardedVideo";

        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        if (Advertisement.IsReady(id))
            Advertisement.Show(id, options);

    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:

                GameManager._Instance.TotalMoneyGame += 200;
                Debug.Log("Video completed. User rewarded  credits. GANHOU 30 moedas TOTAL:" + GameManager._Instance.MoneyGame);
                GooglePlayServices.Instance.SaveToCloud();
                GameManager._Instance.SaveLocal();

                GameManager._Instance.usedADS = true;

                //WebService.instance.SendADS();

                break;
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped.");
                break;
            case ShowResult.Failed:
                Debug.LogError("Video failed to show. :(");
                break;
        }
    }


}
