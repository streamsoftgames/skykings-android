﻿using UnityEngine;
using System.Collections;

public class ConquistasManager : MonoBehaviour
{

    public static ConquistasManager Instancia { get; set; }


    #region Encapsulamento
    public int LevelInimigos
    {
        get
        {
            return levelInimigos;
        }

        set
        {
            levelInimigos = value;
        }
    }

    public int LevelAereos
    {
        get
        {
            return levelAereos;
        }

        set
        {
            levelAereos = value;
        }
    }

    public int LevelAquaticos
    {
        get
        {
            return levelAquaticos;
        }

        set
        {
            levelAquaticos = value;
        }
    }

    public int LevelTanques
    {
        get
        {
            return levelTanques;
        }

        set
        {
            levelTanques = value;
        }
    }

    public int LevelCombustivelsDest
    {
        get
        {
            return levelCombustivelsDest;
        }

        set
        {
            levelCombustivelsDest = value;
        }
    }

    public int LevelCombustivelsColet
    {
        get
        {
            return levelCombustivelsColet;
        }

        set
        {
            levelCombustivelsColet = value;
        }
    }

    public int LevelMedalhasColet
    {
        get
        {
            return levelMedalhasColet;
        }

        set
        {
            levelMedalhasColet = value;
        }
    }

    public int LevelDistancia
    {
        get
        {
            return levelDistancia;
        }

        set
        {
            levelDistancia = value;
        }
    }

    public int LevelMoedasColet
    {
        get
        {
            return levelMoedasColet;
        }

        set
        {
            levelMoedasColet = value;
        }
    }

    public int LevelMortes
    {
        get
        {
            return levelMortes;
        }

        set
        {
            levelMortes = value;
        }
    }

    public int LevelDinheiroGasto
    {
        get
        {
            return levelDinheiroGasto;
        }

        set
        {
            levelDinheiroGasto = value;
        }
    }


    #endregion

    [SerializeField]
    private int levelInimigos = 0;
    [SerializeField]
    private int levelAereos = 0;
    [SerializeField]
    private int levelAquaticos = 0;
    [SerializeField]
    private int levelTanques = 0;

    [SerializeField]
    private int levelCombustivelsDest = 0;
    [SerializeField]
    private int levelCombustivelsColet = 0;
    [SerializeField]
    private int levelMedalhasColet = 0;
    [SerializeField]
    private int levelMoedasColet = 0;
    [SerializeField]
    private int levelDistancia = 0;

    [SerializeField]
    private int levelMortes = 0;
    [SerializeField]
    private int levelDinheiroGasto = 0;


    public int[] inimigosDestruidos = { 300, 800, 3500 }; //**
    public int[] aereosDestruidos = { 100, 500, 1600 }; //**
    public int[] aquaticosDestruidos = { 120, 550, 1700 }; //**
    public int[] tanquesDestruidos = { 80, 450, 1450 }; //**

    public int[] combustivelDestruidos = { 50, 350, 1300 }; //**
    public int[] combustiveColetados = { 80, 400, 1000 }; //**
    public int[] medalhasColetadas = { 80, 400, 1000 }; //**
    public int[] moedasColetadas = { 80, 400, 1000 }; //**
    public int[] distanciaPercorrida = { 5000, 45000, 130000 };

    public int[] quantidadeMortes = { 10, 150, 350 };
    public int[] dinheiroGasto = { 100, 10000, 100000 };


    public int[] recompenca_inimigos = { 10, 35, 80 };
    public int[] recompenca_aereos = { 10, 35, 80 };
    public int[] recompenca_aquaticos = { 10, 35, 80 };
    public int[] recompenca_tanques = { 10, 35, 80 };

    public int[] recompenca_combustiveisDestruidos = { 10, 35, 80 };
    public int[] recompenca_combustiveisColetados = { 10, 35, 80 };
    public int[] recompenca_moedasColetadas = { 10, 35, 80 };
    public int[] recompenca_medalhasColetadas = { 10, 35, 80 };
    public int[] recompenca_distancia = { 10, 35, 80 };

    public int[] recompenca_quantidadeMorte = { 10, 35, 80 };
    public int[] recompenca_dinheiroGasto = { 10, 35, 80 };


    void Awake()
    {
        if (Instancia == null)
            Instancia = this;
    }



    void Update()
    {
        AtualizarConquistas();
    }

    public void AtualizarConquistas()
    {
        LevelInimigos = GameManager._Instance.LevelInimigos;
        LevelAereos = GameManager._Instance.LevelAereos;
        LevelAquaticos = GameManager._Instance.LevelAquaticos;
        LevelTanques = GameManager._Instance.LevelTanques;

        LevelCombustivelsDest = GameManager._Instance.LevelCombustivelsDest;
        LevelCombustivelsColet = GameManager._Instance.LevelCombustivelsColet;
        LevelMedalhasColet = GameManager._Instance.LevelMedalhasColet;
        LevelMoedasColet = GameManager._Instance.LevelMoedasColet;
        LevelDistancia = GameManager._Instance.LevelDistancia;

        LevelMortes = GameManager._Instance.LevelMortes;
        LevelDinheiroGasto = GameManager._Instance.LevelDinheiroGasto;

        #region Inimigos Destruidos
        /* INIMIGOS DESTRUIDOS */
        if (AnalitycsManager.Instance.TotalInimigosDestruidos >= inimigosDestruidos[0] && LevelInimigos == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_inimigos[LevelInimigos];
            GameManager._Instance.EstagioLevelInimigos += recompenca_inimigos[LevelInimigos];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_enemies_destroyed_i);
            PopupManager.Instance.ShowPopUP(recompenca_inimigos[LevelInimigos].ToString());

            LevelInimigos = 1;
            GameManager._Instance.LevelInimigos = LevelInimigos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();

        }
        else if (AnalitycsManager.Instance.TotalInimigosDestruidos >= inimigosDestruidos[1] && LevelInimigos == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_inimigos[LevelInimigos];
            GameManager._Instance.EstagioLevelInimigos += recompenca_inimigos[LevelInimigos];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_enemies_destroyed_ii);
            PopupManager.Instance.ShowPopUP(recompenca_inimigos[LevelInimigos].ToString());

            LevelInimigos = 2;
            GameManager._Instance.LevelInimigos = LevelInimigos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalInimigosDestruidos >= inimigosDestruidos[2] && LevelInimigos == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_inimigos[LevelInimigos];
            GameManager._Instance.EstagioLevelInimigos += recompenca_inimigos[LevelInimigos];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_enemies_destroyed_iii);
            PopupManager.Instance.ShowPopUP(recompenca_inimigos[LevelInimigos].ToString());

            LevelInimigos = 3;
            GameManager._Instance.LevelInimigos = LevelInimigos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Aereos Destruidos
        /* AEREOS DESTRUIDOS */
        if (AnalitycsManager.Instance.TotalAereosDestruidos >= aereosDestruidos[0] && LevelAereos == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_aereos[LevelAereos];
            GameManager._Instance.EstagioLevelAereos += recompenca_aereos[LevelAereos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_air_enemies_destroyed_i);

            PopupManager.Instance.ShowPopUP(recompenca_aereos[LevelAereos].ToString());

            LevelAereos = 1;
            GameManager._Instance.LevelAereos = LevelAereos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalAereosDestruidos >= aereosDestruidos[1] && LevelAereos == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_aereos[LevelAereos];
            GameManager._Instance.EstagioLevelAereos += recompenca_aereos[LevelAereos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_air_enemies_destroyed_ii);

            PopupManager.Instance.ShowPopUP(recompenca_aereos[LevelAereos].ToString());

            LevelAereos = 2;
            GameManager._Instance.LevelAereos = LevelAereos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalAereosDestruidos >= aereosDestruidos[2] && LevelAereos == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_aereos[LevelAereos];
            GameManager._Instance.EstagioLevelAereos += recompenca_aereos[LevelAereos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_air_enemies_destroyed_iii);

            PopupManager.Instance.ShowPopUP(recompenca_aereos[LevelAereos].ToString());

            LevelAereos = 3;
            GameManager._Instance.LevelAereos = LevelAereos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }

        #endregion

        #region Aquaticos Destruidos
        /* AQUATICOS DESTRUIDOS */
        if (AnalitycsManager.Instance.TotalAquaticosDestruidos >= aquaticosDestruidos[0] && LevelAquaticos == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_aquaticos[LevelAquaticos];
            GameManager._Instance.EstagioLevelAquaticos += recompenca_aquaticos[LevelAquaticos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_aquatic_enemies_destroyed_i);

            PopupManager.Instance.ShowPopUP(recompenca_aquaticos[LevelAquaticos].ToString());

            LevelAquaticos = 1;
            GameManager._Instance.LevelAquaticos = LevelAquaticos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if ((AnalitycsManager.Instance.TotalAquaticosDestruidos >= aquaticosDestruidos[1] && AnalitycsManager.Instance.TotalAquaticosDestruidos < aquaticosDestruidos[2]) && LevelAquaticos == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_aquaticos[LevelAquaticos];
            GameManager._Instance.EstagioLevelAquaticos += recompenca_aquaticos[LevelAquaticos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_aquatic_enemies_destroyed_ii);

            PopupManager.Instance.ShowPopUP(recompenca_aquaticos[LevelAquaticos].ToString());

            LevelAquaticos = 2;
            GameManager._Instance.LevelAquaticos = LevelAquaticos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalAquaticosDestruidos >= aquaticosDestruidos[2] && LevelAquaticos == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_aquaticos[LevelAquaticos];
            GameManager._Instance.EstagioLevelAquaticos += recompenca_aquaticos[LevelAquaticos];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_aquatic_enemies_destroyed_iii);

            PopupManager.Instance.ShowPopUP(recompenca_aquaticos[LevelAquaticos].ToString());

            LevelAquaticos = 3;
            GameManager._Instance.LevelAquaticos = LevelAquaticos;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Tanques Destruidos

        /* TANQUES DESTRUIDOS */
        if (AnalitycsManager.Instance.TotalTanquesDestruidos >= tanquesDestruidos[0] && LevelTanques == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_tanques[LevelTanques];
            GameManager._Instance.EstagioLevelTanques += recompenca_tanques[LevelTanques];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_tank_enemies_destroyed_i);

            PopupManager.Instance.ShowPopUP(recompenca_tanques[LevelTanques].ToString());

            LevelTanques = 1;
            GameManager._Instance.LevelTanques = LevelTanques;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalTanquesDestruidos >= tanquesDestruidos[1] && LevelTanques == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_tanques[LevelTanques];
            GameManager._Instance.EstagioLevelTanques += recompenca_tanques[LevelTanques];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_tank_enemies_destroyed_ii);

            PopupManager.Instance.ShowPopUP(recompenca_tanques[LevelTanques].ToString());

            LevelTanques = 2;
            GameManager._Instance.LevelTanques = LevelTanques;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalTanquesDestruidos >= tanquesDestruidos[2] && LevelTanques == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_tanques[LevelTanques];
            GameManager._Instance.EstagioLevelTanques += recompenca_tanques[LevelTanques];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_tank_enemies_destroyed_iii);

            PopupManager.Instance.ShowPopUP(recompenca_tanques[LevelTanques].ToString());

            LevelTanques = 3;
            GameManager._Instance.LevelTanques = LevelTanques;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Combustiveis Coletados
        /* COMBUSTIVEIS COLETADOS */
        if (AnalitycsManager.Instance.TotalGasolinasColetadas >= combustiveColetados[0] && LevelCombustivelsColet == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_combustiveisColetados[LevelCombustivelsColet];
            GameManager._Instance.EstagioLevelCombustivelsColet += recompenca_combustiveisColetados[LevelCombustivelsColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_i);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisColetados[LevelCombustivelsColet].ToString());

            LevelCombustivelsColet = 1;
            GameManager._Instance.LevelCombustivelsColet = LevelCombustivelsColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalGasolinasColetadas >= combustiveColetados[1] && LevelCombustivelsColet == 1)
        {

            GameManager._Instance.MedalMoney += recompenca_combustiveisColetados[LevelCombustivelsColet];
            GameManager._Instance.EstagioLevelCombustivelsColet += recompenca_combustiveisColetados[LevelCombustivelsColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_ii);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisColetados[LevelCombustivelsColet].ToString());

            LevelCombustivelsColet = 2;
            GameManager._Instance.LevelCombustivelsColet = LevelCombustivelsColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalGasolinasColetadas >= combustiveColetados[2] && LevelCombustivelsColet == 2)
        {

            GameManager._Instance.MedalMoney += recompenca_combustiveisColetados[LevelCombustivelsColet];
            GameManager._Instance.EstagioLevelCombustivelsColet += recompenca_combustiveisColetados[LevelCombustivelsColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_iii);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisColetados[LevelCombustivelsColet].ToString());

            LevelCombustivelsColet = 3;
            GameManager._Instance.LevelCombustivelsColet = LevelCombustivelsColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Combustiveis Destruidos
        /* COMBUSTIVEIS DESTRUÍDOS */
        if (AnalitycsManager.Instance.TotalGasolinasDestruidas >= combustivelDestruidos[0] && LevelCombustivelsDest == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_combustiveisDestruidos[LevelCombustivelsDest];
            GameManager._Instance.EstagioLevelCombustivelsDest += recompenca_combustiveisDestruidos[LevelCombustivelsDest];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_destroyed_fuels_i);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisDestruidos[LevelCombustivelsDest].ToString());

            LevelCombustivelsDest = 1;
            GameManager._Instance.LevelCombustivelsDest = LevelCombustivelsDest;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalGasolinasDestruidas >= combustivelDestruidos[1] && LevelCombustivelsDest == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_combustiveisDestruidos[LevelCombustivelsDest];
            GameManager._Instance.EstagioLevelCombustivelsDest += recompenca_combustiveisDestruidos[LevelCombustivelsDest];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_destroyed_fuels_ii);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisDestruidos[LevelCombustivelsDest].ToString());

            LevelCombustivelsDest = 2;
            GameManager._Instance.LevelCombustivelsDest = LevelCombustivelsDest;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalGasolinasDestruidas >= combustivelDestruidos[2] && LevelCombustivelsDest == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_combustiveisDestruidos[LevelCombustivelsDest];
            GameManager._Instance.EstagioLevelCombustivelsDest += recompenca_combustiveisDestruidos[LevelCombustivelsDest];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_destroyed_fuels_iii);

            PopupManager.Instance.ShowPopUP(recompenca_combustiveisDestruidos[LevelCombustivelsDest].ToString());

            LevelCombustivelsDest = 3;
            GameManager._Instance.LevelCombustivelsDest = LevelCombustivelsDest;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Moedas Coletadas
        /* MOEDAS COLETADAS */
        if (AnalitycsManager.Instance.TotalDinheiroGanho >= moedasColetadas[0] && LevelMoedasColet == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_moedasColetadas[LevelMoedasColet];
            GameManager._Instance.EstagioLevelMoedasColet += recompenca_moedasColetadas[LevelMoedasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_i);

            PopupManager.Instance.ShowPopUP(recompenca_moedasColetadas[LevelMoedasColet].ToString());

            LevelMoedasColet = 1;
            GameManager._Instance.LevelMoedasColet = LevelMoedasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDinheiroGanho >= moedasColetadas[1] && LevelMoedasColet == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_moedasColetadas[LevelMoedasColet];
            GameManager._Instance.EstagioLevelMoedasColet += recompenca_moedasColetadas[LevelMoedasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_ii);

            PopupManager.Instance.ShowPopUP(recompenca_moedasColetadas[LevelMoedasColet].ToString());

            LevelMoedasColet = 2;
            GameManager._Instance.LevelMoedasColet = LevelMoedasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDinheiroGanho >= moedasColetadas[2] && LevelMoedasColet == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_moedasColetadas[LevelMoedasColet];
            GameManager._Instance.EstagioLevelMoedasColet += recompenca_moedasColetadas[LevelMoedasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_collected_fuels_iii);

            PopupManager.Instance.ShowPopUP(recompenca_moedasColetadas[LevelMoedasColet].ToString());

            LevelMoedasColet = 3;
            GameManager._Instance.LevelMoedasColet = LevelMoedasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Medalhas Coletadas
        /* MEDALHAS COLETADAS */
        if (AnalitycsManager.Instance.TotalMedalhaGanho >= medalhasColetadas[0] && LevelMedalhasColet == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_medalhasColetadas[LevelMedalhasColet];
            GameManager._Instance.EstagioLevelMedalhasColet += recompenca_medalhasColetadas[LevelMedalhasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_searching_of_medal_i);

            PopupManager.Instance.ShowPopUP(recompenca_medalhasColetadas[LevelMedalhasColet].ToString());

            LevelMedalhasColet = 1;
            GameManager._Instance.LevelMedalhasColet = LevelMedalhasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalMedalhaGanho >= medalhasColetadas[1] && LevelMedalhasColet == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_medalhasColetadas[LevelMedalhasColet];
            GameManager._Instance.EstagioLevelMedalhasColet += recompenca_medalhasColetadas[LevelMedalhasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_searching_of_medal_ii);

            PopupManager.Instance.ShowPopUP(recompenca_medalhasColetadas[LevelMedalhasColet].ToString());

            LevelMedalhasColet = 2;
            GameManager._Instance.LevelMedalhasColet = LevelMedalhasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalMedalhaGanho >= medalhasColetadas[2] && LevelMedalhasColet == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_medalhasColetadas[LevelMedalhasColet];
            GameManager._Instance.EstagioLevelMedalhasColet += recompenca_medalhasColetadas[LevelMedalhasColet];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_searching_of_medal_iii);

            PopupManager.Instance.ShowPopUP(recompenca_medalhasColetadas[LevelMedalhasColet].ToString());

            LevelMedalhasColet = 3;
            GameManager._Instance.LevelMedalhasColet = LevelMedalhasColet;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Distancia Total
        /* TOTAL DISTANCIA */
        if (AnalitycsManager.Instance.TotalDistanciaViajada >= distanciaPercorrida[0] && LevelDistancia == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_distancia[LevelDistancia];
            GameManager._Instance.EstagioLevelDistancia += recompenca_distancia[LevelDistancia];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_closest_to_infinity_i);

            PopupManager.Instance.ShowPopUP(recompenca_distancia[LevelDistancia].ToString());

            LevelDistancia = 1;
            GameManager._Instance.LevelDistancia = LevelDistancia;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDistanciaViajada >= distanciaPercorrida[1] && LevelDistancia == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_distancia[LevelDistancia];
            GameManager._Instance.EstagioLevelDistancia += recompenca_distancia[LevelDistancia];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_closest_to_infinity_ii);

            PopupManager.Instance.ShowPopUP(recompenca_distancia[LevelDistancia].ToString());

            LevelDistancia = 2;
            GameManager._Instance.LevelDistancia = LevelDistancia;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDistanciaViajada >= distanciaPercorrida[2] && LevelDistancia == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_distancia[LevelDistancia];
            GameManager._Instance.EstagioLevelDistancia += recompenca_distancia[LevelDistancia];

            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_closest_to_infinity_iii);

            PopupManager.Instance.ShowPopUP(recompenca_distancia[LevelDistancia].ToString());

            LevelDistancia = 3;
            GameManager._Instance.LevelDistancia = LevelDistancia;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Mortes Totais
        /* TOTAL MORTES */
        if (AnalitycsManager.Instance.TotalMortes >= quantidadeMortes[0] && LevelMortes == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_quantidadeMorte[LevelMortes];
            GameManager._Instance.EstagioLevelMortes += recompenca_quantidadeMorte[LevelMortes];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_walking_with_death_i);
            PopupManager.Instance.ShowPopUP(recompenca_quantidadeMorte[LevelMortes].ToString());
            LevelMortes = 1;
            GameManager._Instance.LevelMortes = LevelMortes;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalMortes >= quantidadeMortes[1] && LevelMortes == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_quantidadeMorte[LevelMortes];
            GameManager._Instance.EstagioLevelMortes += recompenca_quantidadeMorte[LevelMortes];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_walking_with_death_i);
            PopupManager.Instance.ShowPopUP(recompenca_quantidadeMorte[LevelMortes].ToString());
            LevelMortes = 2;
            GameManager._Instance.LevelMortes = LevelMortes;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalMortes >= quantidadeMortes[2] && LevelMortes == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_quantidadeMorte[LevelMortes];
            GameManager._Instance.EstagioLevelMortes += recompenca_quantidadeMorte[LevelMortes];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_walking_with_death_i);
            PopupManager.Instance.ShowPopUP(recompenca_quantidadeMorte[LevelMortes].ToString());
            LevelMortes = 3;
            GameManager._Instance.LevelMortes = LevelMortes;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion

        #region Dinheiro Gasto
        /* Dinheiro Gasto */
        if (AnalitycsManager.Instance.TotalDinheiroGasto >= dinheiroGasto[0] && LevelDinheiroGasto == 0)
        {
            GameManager._Instance.MedalMoney += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GameManager._Instance.EstagioLevelDinheiroGasto += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_flying_money_i);
            PopupManager.Instance.ShowPopUP(recompenca_dinheiroGasto[LevelDinheiroGasto].ToString());
            LevelDinheiroGasto = 1;
            GameManager._Instance.LevelDinheiroGasto = LevelDinheiroGasto;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDinheiroGasto >= dinheiroGasto[1] && LevelDinheiroGasto == 1)
        {
            GameManager._Instance.MedalMoney += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GameManager._Instance.EstagioLevelDinheiroGasto += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_flying_money_ii);
            PopupManager.Instance.ShowPopUP(recompenca_dinheiroGasto[LevelDinheiroGasto].ToString());
            LevelDinheiroGasto = 2;
            GameManager._Instance.LevelDinheiroGasto = LevelDinheiroGasto;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        else if (AnalitycsManager.Instance.TotalDinheiroGasto == dinheiroGasto[2] && LevelDinheiroGasto == 2)
        {
            GameManager._Instance.MedalMoney += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GameManager._Instance.EstagioLevelDinheiroGasto += recompenca_dinheiroGasto[LevelDinheiroGasto];
            GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_flying_money_iii);
            PopupManager.Instance.ShowPopUP(recompenca_dinheiroGasto[LevelDinheiroGasto].ToString());
            LevelDinheiroGasto = 3;
            GameManager._Instance.LevelDinheiroGasto = LevelDinheiroGasto;
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
        }
        #endregion




    }
}
