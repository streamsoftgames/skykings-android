﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopupManager : MonoBehaviour {

	public static PopupManager Instance { set; get; }

    public CanvasGroup canvasGroup;
    public Text Message;

    public Transform uiRoot;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;
    }

    public void ShowPopUP(string message)
    {
        Debug.LogWarning("CHAMOU POPUP");
        if (uiRoot == null)
            uiRoot = GameObject.FindGameObjectWithTag("UIRoot").transform;

        transform.SetParent(uiRoot);
        transform.position = new Vector3(0, 0, 0);
        transform.localScale = new Vector3(1, 1, 1);

        canvasGroup.alpha = 1;
       
        
        Message.text = message;

        StartCoroutine(OnClick());
        
    }

    public IEnumerator OnClick()
    {
        Debug.LogWarning("ANTES POPUP");
        yield return new WaitForSeconds(1.5f);

        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;

        transform.SetParent(uiRoot.parent);

        StopCoroutine(OnClick());
        Debug.LogWarning("DEPOIS POPUP");
    }

}
