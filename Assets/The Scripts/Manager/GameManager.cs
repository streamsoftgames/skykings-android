﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using GoogleMobileAds.Api;

public class GameManager : MonoBehaviour
{

    public static GameManager _Instance { get; private set; }

    #region Encapsulamento

    public bool IsIntro
    {
        get
        {
            return isIntro;
        }

        set
        {
            isIntro = value;
        }
    }
    public bool IsDoubleMedal
    {
        get
        {
            return isDoubleMedal;
        }

        set
        {
            isDoubleMedal = value;
        }
    }
    public string SceneActualName
    {
        get
        {
            return sceneActualName;
        }

        set
        {
            sceneActualName = value;
        }
    }
    public float TravelledDistance
    {
        get
        {
            return travelledDistance;
        }

        set
        {
            travelledDistance = value;
        }
    }
    public int GameLevel
    {
        get
        {
            return gameLevel;
        }

        set
        {
            gameLevel = value;
        }
    }
    public int PontosGanhosnoJogo
    {
        get
        {
            return pointsOnGame;
        }

        set
        {
            pointsOnGame = value;
        }
    }
    public bool LevelChanged
    {
        get
        {
            return levelChanged;
        }

        set
        {
            levelChanged = value;
        }
    }
    public bool IsPaused
    {
        get
        {
            return isPaused;
        }

        set
        {
            isPaused = value;
        }
    }
    public bool SwapScene
    {
        get
        {
            return swapScene;
        }

        set
        {
            swapScene = value;
        }
    }
    public bool InGame
    {
        get
        {
            return inGame;
        }

        set
        {
            inGame = value;
        }
    }
    public Vector2 LastPosition
    {
        get
        {
            return lastPosition;
        }

        set
        {
            lastPosition = value;
        }
    }
    public int MoneyGame
    {
        get
        {
            return moneyGame;
        }

        set
        {
            moneyGame = value;
        }
    }
    public float DistanceGameLevelUP
    {
        get
        {
            return distanceGameLevelUP;
        }

        set
        {
            distanceGameLevelUP = value;
        }
    }
    public float AuxDistanceTravelled
    {
        get
        {
            return auxDistanceTravelled;
        }

        set
        {
            auxDistanceTravelled = value;
        }
    }
    public bool IsInfiniteFuel
    {
        get
        {
            return isInfiniteFuel;
        }

        set
        {
            isInfiniteFuel = value;
        }
    }
    public bool IsInvencible
    {
        get
        {
            return isInvencible;
        }

        set
        {
            isInvencible = value;
        }
    }
    public bool IsDoubleMoney
    {
        get
        {
            return isDoubleMoney;
        }

        set
        {
            isDoubleMoney = value;
        }
    }
    public int TotalMoneyGame
    {
        get
        {
            return totalMoneyGame;
        }

        set
        {
            totalMoneyGame = value;
        }
    }
    public float BestDistance
    {
        get
        {
            return bestDistance;
        }

        set
        {
            bestDistance = value;
        }
    }
    public int MedalMoney
    {
        get
        {
            return medalMoney;
        }

        set
        {
            medalMoney = value;
        }
    }
    public int QuantityInvencible
    {
        get
        {
            return quantityInvencible;
        }

        set
        {
            quantityInvencible = value;
        }
    }
    public int QuantityInfiniteFuel
    {
        get
        {
            return quantityInfiniteFuel;
        }

        set
        {
            quantityInfiniteFuel = value;
        }
    }
    public int QuantityEspecialShoot
    {
        get
        {
            return quantityEspecialShoot;
        }

        set
        {
            quantityEspecialShoot = value;
        }
    }
    public bool IsSpecialShoot
    {
        get
        {
            return isSpecialShoot;
        }

        set
        {
            isSpecialShoot = value;
        }
    }
    public int ValueBuyUpgradeInvisibility
    {
        get
        {
            return valueBuyUpgradeInvisibility;
        }

        set
        {
            valueBuyUpgradeInvisibility = value;
        }
    }
    public int ValueBuyUpgradeFuelInfinite
    {
        get
        {
            return valueBuyUpgradeFuelInfinite;
        }

        set
        {
            valueBuyUpgradeFuelInfinite = value;
        }
    }
    public int ValueBuyUpgradeSpecialShoot
    {
        get
        {
            return valueBuyUpgradeSpecialShoot;
        }

        set
        {
            valueBuyUpgradeSpecialShoot = value;
        }
    }
    public int LevelInvencibility
    {
        get
        {
            return levelInvencibility;
        }

        set
        {
            levelInvencibility = value;
        }
    }
    public int LevelFuelInifinite
    {
        get
        {
            return levelFuelInifinite;
        }

        set
        {
            levelFuelInifinite = value;
        }
    }
    public int LevelSpecialShoot
    {
        get
        {
            return levelSpecialShoot;
        }

        set
        {
            levelSpecialShoot = value;
        }
    }
    public float TimeInvencibility
    {
        get
        {
            return timeInvencibility;
        }

        set
        {
            timeInvencibility = value;
        }
    }
    public float TimeFuelInfinite
    {
        get
        {
            return timeFuelInfinite;
        }

        set
        {
            timeFuelInfinite = value;
        }
    }
    public float TimeSpecialShoot
    {
        get
        {
            return timeSpecialShoot;
        }

        set
        {
            timeSpecialShoot = value;
        }
    }
    public int BestGameLevel
    {
        get
        {
            return bestGameLevel;
        }

        set
        {
            bestGameLevel = value;
        }
    }
    public int SelectedShip_ID
    {
        get
        {
            return selectedShip_ID;
        }

        set
        {
            selectedShip_ID = value;
        }
    }
    public int TotalPontosGeral
    {
        get
        {
            return totalPointsInGame;
        }

        set
        {
            totalPointsInGame = value;
        }
    }

    public string Idioma
    {
        get
        {
            return idioma;
        }

        set
        {
            idioma = value;
        }
    }

    public bool IsTutorial
    {
        get { return isTutorial; }
        set { isTutorial = value; }
    }

    public bool LoadInicial_ok
    {
        get { return loadInicial_ok; }
        set { loadInicial_ok = value; }
    }

    public int LevelInimigos
    {
        get { return levelInimigos; }
        set { levelInimigos = value; }
    }

    public int LevelAereos
    {
        get { return levelAereos; }
        set { levelAereos = value; }
    }

    public int LevelAquaticos
    {
        get { return levelAquaticos; }
        set { levelAquaticos = value; }
    }

    public int LevelTanques
    {
        get { return levelTanques; }
        set { levelTanques = value; }
    }

    public int LevelCombustivelsDest
    {
        get { return levelCombustivelsDest; }
        set { levelCombustivelsDest = value; }
    }
    public int LevelCombustivelsColet
    {
        get { return levelCombustivelsColet; }
        set { levelCombustivelsColet = value; }
    }
    public int LevelMedalhasColet
    {
        get { return levelMedalhasColet; }
        set { levelMedalhasColet = value; }
    }
    public int LevelMoedasColet
    {
        get { return levelMoedasColet; }
        set { levelMoedasColet = value; }
    }
    public int LevelDistancia
    {
        get { return levelDistancia; }
        set { levelDistancia = value; }
    }

    public int LevelMortes
    {
        get { return levelMortes; }
        set { levelMortes = value; }
    }
    public int LevelDinheiroGasto
    {
        get { return levelDinheiroGasto; }
        set { levelDinheiroGasto = value; }
    }

    public int EstagioLevelInimigos
    {
        get { return inimigos_TotalRecebido; }
        set { inimigos_TotalRecebido = value; }
    }

    public int EstagioLevelAereos
    {
        get { return aereos_TotalRecebidos; }
        set { aereos_TotalRecebidos = value; }
    }

    public int EstagioLevelAquaticos
    {
        get { return aquaticos_TotalRecebido; }
        set { aquaticos_TotalRecebido = value; }
    }

    public int EstagioLevelTanques
    {
        get { return tanques_TotalRecebido; }
        set { tanques_TotalRecebido = value; }
    }

    public int EstagioLevelCombustivelsDest
    {
        get { return combustivelDest_TotalRecebido; }
        set { combustivelDest_TotalRecebido = value; }
    }
    public int EstagioLevelCombustivelsColet
    {
        get { return combustivelColet_TotalRecebido; }
        set { combustivelColet_TotalRecebido = value; }
    }
    public int EstagioLevelMedalhasColet
    {
        get { return medalhas_TotalRecebido; }
        set { medalhas_TotalRecebido = value; }
    }
    public int EstagioLevelMoedasColet
    {
        get { return moedas_TotalRecebido; }
        set { moedas_TotalRecebido = value; }
    }
    public int EstagioLevelDistancia
    {
        get { return distancia_TotalRecebido; }
        set { distancia_TotalRecebido = value; }
    }

    public int EstagioLevelMortes
    {
        get { return morter_TotalRecebido; }
        set { morter_TotalRecebido = value; }
    }
    public int EstagioLevelDinheiroGasto
    {
        get { return dinheiroGasto_TotalRecebido; }
        set { dinheiroGasto_TotalRecebido = value; }
    }

    public bool Primeira_vez
    {
        get { return primeira_vez; }
        set { primeira_vez = value; }
    }
    public int BestPontuation
    {
        get { return bestPontuation; }
        set { bestPontuation = value; }
    }

    public int TotalRecebido_inimigos
    {
        get { return totalRecebido_inimigos; }
        set { totalRecebido_inimigos = value; }
    }



    #endregion                                                      //Inicio dos Encapsulamentos
    string appIdAdmob = "ca-app-pub-9533152074799631~8030947244";
    string bannerIdAdmob = "ca-app-pub-9533152074799631/9716900443";
    public BannerView bannerAdmob;
    AdRequest request;

    tempService webservice;
    public string nomeGoogle;
    public string idGoogle;
    public string emailGoole;

    private bool carregouNovo = false;

    [SerializeField]
    private string idioma; // idioma do jogo

    [SerializeField]
    private bool primeira_vez = true; //controle se é a primeira vez que a pessoa está jogando.
    [SerializeField]
    private bool loadInicial_ok = false; //

    [SerializeField]
    private bool isTutorial = false;
    public bool tutorialOK = false;

    //public AudioManager _audio;
    public PlayerManager _player;                                       //Instancia do player.
                                                                        //    public GooglePlaySaveInCloud _save;

    /* Para todos */
    [SerializeField]
    private string sceneActualName;                                     //Nome da cena atual do jogo.

    [SerializeField]
    private int axuADSShow = 0;
    public bool usedADS = false;

    /* In game */
    [SerializeField]
    private int gameLevel;                      	//level atual que o jogador está jogando.
    [SerializeField]
    private int pointsOnGame = 0;                   //pontos dentro do jogo.
    [SerializeField]
    private int totalPointsInGame = 0;              //guarda o total de pontos no jogo;
    [SerializeField]
    private int moneyGame = 0;                      //dinheiro ganho na partida.
    [SerializeField]
    private int totalMoneyGame = 0;                 //dinheiro total do jogador.
    [SerializeField]
    private int medalMoney;                         //total de medalhas do jogador.

    private int bestGameLevel;						//melhor dificuldade alcançada.

    [SerializeField]
    private bool swapScene = false;                 //trocou de cena.
    [SerializeField]
    private bool inGame = false;                    //no jogo, player jogando.
    [SerializeField]
    private bool isPaused = false;                  //game pausado
    [SerializeField]
    private bool isIntro;							//controla saida da nave do barco.

    [SerializeField]
    private bool levelChanged = false;				//se o level foi mudado.

    [SerializeField]
    private float auxDistanceTravelled;
    [SerializeField]
    private float distanceGameLevelUP = 30;         //Distancia inicial para proximo nivel do jogo.

    private Vector2 lastPosition;                   //medir distância
    [SerializeField]
    private float travelledDistance = 0;            //guarda o valor da distancia viajada
    [SerializeField]
    private float bestDistance = 0;                 //guarda o valor da distancia viajada
    [SerializeField]
    private int bestPontuation = 0;                 //guarda o valor da distancia viajada

    public int selectedShip_ID = 0;                 //qual é o ID da nave selecionada



    /* Boosters e Outros */
    [SerializeField]
    private bool isInfiniteFuel = false;			//booster: gasolina infinita.
    [SerializeField]
    private bool isInvencible = false;				//booster: invencibilidade.
    [SerializeField]
    private bool isDoubleMoney = false;				//iap: dobro de dinheiro.
    [SerializeField]
    private bool isDoubleMedal = false;				//iap:  dobro de medalha.
    [SerializeField]
    private bool isSpecialShoot = false;			//booster: tiro infinito.

    [SerializeField]
    private int quantityInvencible = 0;
    [SerializeField]
    private int quantityInfiniteFuel = 0;
    [SerializeField]
    private int quantityEspecialShoot = 0;

    private int levelInvencibility = 0;
    private int levelFuelInifinite = 0;
    private int levelSpecialShoot = 0;

    [SerializeField]
    private float timeInvencibility = 3.5f;
    [SerializeField]
    private float timeFuelInfinite = 3.5f;
    [SerializeField]
    private float timeSpecialShoot = 3.5f;

    private int valueBuyUpgradeInvisibility = 15;
    private int valueBuyUpgradeFuelInfinite = 15;
    private int valueBuyUpgradeSpecialShoot = 15;

    /* Upgrades */
    [SerializeField]
    public int[]
        upgradeVelocidade,
        upgradeMovimento,
        upgradeTanque,
        upgradeMagnetude,
        upgradePoder;

    public int[]
        valorVelocidade,
        valorMovimento,
        valorTanque,
        valorMagnetude,
        valorPoder;

    public int[] naveComprada;


    public float LastSave { get; set; }
    public float PlayTimeSinceSave { get; set; }
    public float TotalPlayTime { get { return (Time.time - LastSave) + PlayTimeSinceSave; } }

    #region Encapsulamento

    public string Fb_nome
    {
        get
        {
            return fb_nome;
        }

        set
        {
            fb_nome = value;
        }
    }

    public string Fb_email
    {
        get
        {
            return fb_email;
        }

        set
        {
            fb_email = value;
        }
    }

    public string Fb_id
    {
        get
        {
            return fb_id;
        }

        set
        {
            fb_id = value;
        }
    }

    public int WebserviceID
    {
        get
        {
            return webserviceID;
        }

        set
        {
            webserviceID = value;
        }
    }

    public bool Ps4Participante
    {
        get
        {
            return ps4Participante;
        }

        set
        {
            ps4Participante = value;
        }
    }

    #endregion

    /* CONQUISTA VARIAVEIS*/

    [SerializeField]
    private int levelInimigos = 0;
    [SerializeField]
    private int levelAereos = 0;
    [SerializeField]
    private int levelAquaticos = 0;
    [SerializeField]
    private int levelTanques = 0;

    [SerializeField]
    private int levelCombustivelsDest = 0;
    [SerializeField]
    private int levelCombustivelsColet = 0;
    [SerializeField]
    private int levelMedalhasColet = 0;
    [SerializeField]
    private int levelMoedasColet = 0;
    [SerializeField]
    private int levelDistancia = 0;

    [SerializeField]
    private int levelMortes = 0;
    [SerializeField]
    private int levelDinheiroGasto = 0;

    [SerializeField]
    private int inimigos_TotalRecebido = 0;
    [SerializeField]
    private int aereos_TotalRecebidos = 0;
    [SerializeField]
    private int aquaticos_TotalRecebido = 0;
    [SerializeField]
    private int tanques_TotalRecebido = 0;

    [SerializeField]
    private int combustivelDest_TotalRecebido = 0;
    [SerializeField]
    private int combustivelColet_TotalRecebido = 0;
    [SerializeField]
    private int medalhas_TotalRecebido = 0;
    [SerializeField]
    private int moedas_TotalRecebido = 0;
    [SerializeField]
    private int distancia_TotalRecebido = 0;

    [SerializeField]
    private int morter_TotalRecebido = 0;
    [SerializeField]
    private int dinheiroGasto_TotalRecebido = 0;

    [SerializeField]
    private int totalRecebido_inimigos = 0;

    [SerializeField]
    private string fb_nome, fb_email;
    [SerializeField]
    private string fb_id;

    [SerializeField]
    private int webserviceID;

    [SerializeField]
    private bool ps4Participante = false;

    public bool temConexao;

    void Awake()
    {


        if (_Instance == null)
        {

            _Instance = this;
            SwapScene = true;
            Debug.Log("Singleton criado");
            AnalitycsManager.Instance.TotalAppIniciado++;

           
            

            naveComprada = new int[16];

            upgradeVelocidade = new int[16];
            upgradeMovimento = new int[16];
            upgradeTanque = new int[16];
            upgradeMagnetude = new int[16];
            upgradePoder = new int[16];

            valorVelocidade = new int[16];
            valorMovimento = new int[16];
            valorTanque = new int[16];
            valorMagnetude = new int[16];
            valorPoder = new int[16];

            LoadInicial_ok = false;
        }
        else
        {
            Destroy(gameObject);
            _Instance.SwapScene = true;
            Debug.Log("Singleton Destruido");

        }

        print("É primeira vez ?  " + Primeira_vez);

        DontDestroyOnLoad(gameObject);



        //idioma = "en-us";
        //print(TotalPlayTime);


        //_save = GooglePlaySaveInCloud.Instancia;

    }
    void Start()
    {

        //GooglePlayServices.Instance.Login("Login");
        ZPlayerPrefs.Initialize("capeta666", "skykingsthebest");

        MobileAds.Initialize(appIdAdmob);

        bannerAdmob = new BannerView(bannerIdAdmob, AdSize.Banner, AdPosition.Bottom);
        request = new AdRequest.Builder().Build();
        bannerAdmob.LoadAd(request);
        bannerAdmob.Hide();

    }
    void Update()
    {

        temConexao = _Instance.GetComponent<CheckInternet>().ChecarConexao();

        if (SwapScene) //executa quando muda de uma cena para outra.
        {
            SceneActualName = SceneManager.GetActiveScene().name;
        }

        if (SceneActualName.Equals("0 - Inicializar")) //Inicializar
        {
            if (SwapScene)
            {
                //Inicilizar();

                SwapScene = false;
                bannerAdmob.Hide();
            }
        }

        if (SceneActualName.Equals("1 - Idioma")) //IDIOMA
        {
            if (SwapScene)
            {
                //Inicilizar();

                SwapScene = false;
                bannerAdmob.Hide();
            }
        }
        if (SceneActualName.Equals("2 - Menu")) //MENU
        {
            if (SwapScene)
            {

                SwapScene = false;
                bannerAdmob.Hide();
            }
        }

        if (SceneActualName.Equals("3 - PlayerSelect")) //P.SELECT
        {
            if (SwapScene)
            {

                for (int i = 0; i < 16; i++)
                {
                    if (valorVelocidade[i] < 20)
                        valorVelocidade[i] = 20;

                    if (valorMovimento[i] < 20)
                        valorMovimento[i] = 20;

                    if (valorTanque[i] < 20)
                        valorTanque[i] = 20;

                    if (valorMagnetude[i] < 20)
                        valorMagnetude[i] = 20;

                    if (valorPoder[i] < 20)
                        valorPoder[i] = 20;
                }

                Analytics.CustomEvent("ValoresFinais.PlayerSelect", new Dictionary<string, object>
                    {
                        { "AppIniciado", AnalitycsManager.Instance.TotalAppIniciado },
                        { "NomeJogador", AnalitycsManager.Instance.NomeUsuario },
                    });

                //SceneManager.LoadSceneAsync("Radio", LoadSceneMode.Additive);
                SwapScene = false;
                bannerAdmob.Hide();
            }
        }

        if (SceneActualName.Equals("5 - Game")) //JOGO
        {
            if (SwapScene)      //no momento em que a cena é mudada, faz esses comandos. 
            {
                Debug.LogWarning("Estou entrando no GAME!");

                bannerAdmob.Hide();

                //SceneManager.LoadSceneAsync("Radio", LoadSceneMode.Additive);
                //_audio = _Instance.GetComponent<AudioManager>();

                _player = GameObject.Find("Player").GetComponent<PlayerManager>();  //instanciando o player.
                LastPosition = _player.transform.position - _player.InitialPos.transform.position;

                /* Variaveis iniciais - Valores que devem ter ao iniciar o layout */
                isTutorial = false;
                gameLevel = 1;
                moneyGame = 0;
                travelledDistance = 0;
                AuxDistanceTravelled = 0;
                PontosGanhosnoJogo = 0;

                usedADS = false;
                IsInfiniteFuel = false;
                IsInvencible = false;
                IsSpecialShoot = false;
                AnalitycsManager.Instance.TotalJogosIniciado++;

                StartCoroutine(BeginGame()); //courotine para iniciar o jogo.



                Analytics.CustomEvent("GastosJogo.PlayerSelect", new Dictionary<string, object>
                    {
                        {"GastosMedalha", AnalitycsManager.Instance.TotalMedalhaGasto },
                        {"GastoMoedas", AnalitycsManager.Instance.TotalDinheiroGasto },

                        {"QuantidadeBooster.Invencibilidade", AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados },
                        {"QuantidadeBooster.GasolinaInfinita", AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaComprados },
                        {"QuantidadeBooster.TiroEspecial", AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados },

                        { "TotalJogosIniciados", AnalitycsManager.Instance.TotalJogosIniciado },

                    });

                SwapScene = false;
            }



            if (InGame) //jogo começou, jogador está jogando. 
            {
                UpdateDistanceTravelled(); //atualiza informação da distancia percorrida.
                GameLevelUP(); //controla o level do jogo.

            }

        }

        if (SceneActualName.Equals("7 - Tutorial")) //P.SELECT
        {
            if (SwapScene)
            {

                _player = null;
                _player = GameObject.Find("Player").GetComponent<PlayerManager>();
                LastPosition = _player.transform.position - _player.InitialPos.transform.position;

                isTutorial = true;
                gameLevel = 1;
                moneyGame = 0;
                travelledDistance = 0;
                AuxDistanceTravelled = 0;
                PontosGanhosnoJogo = 0;

                IsInfiniteFuel = false;
                IsInvencible = false;
                IsSpecialShoot = false;

                StartCoroutine(BeginGame()); //courotine para iniciar o jogo.

                SwapScene = false;
            }
        }

        if (SceneActualName.Equals("00 - Loading"))
        { //P.SELECT
            if (SwapScene)
            {
                IsIntro = false;
                InGame = false;

                SwapScene = false;
            }
        }

    }
    void UpdateDistanceTravelled() //Atualiza distancias do jogo.
    {
        TravelledDistance += Vector2.Distance(_player.transform.position, LastPosition);
        AuxDistanceTravelled += Vector2.Distance(_player.transform.position, LastPosition);

        if (!IsInfiniteFuel)
            _player.FuelActual -= Vector2.Distance(_player.transform.position, LastPosition);

        LastPosition = _player.transform.position;

    }

    void GameLevelUP() //Atualiza level do jogo. 
    {
        //Debug.Log((int)distanceTravelled_AUX % 5);

        if (AuxDistanceTravelled >= DistanceGameLevelUP)
        {
            gameLevel++;
            LevelChanged = true;

            AuxDistanceTravelled = AuxDistanceTravelled - DistanceGameLevelUP;

            DistanceGameLevelUP += ((DistanceGameLevelUP * 2) / 100);
            Debug.Log(DistanceGameLevelUP);
        }
    }

    IEnumerator BeginGame() //IEnumerator que conta o tempo para começar o jogo. 
    {
        yield return new WaitForSeconds(1);
        IsIntro = true;
        InGame = false;
        Debug.LogWarning("BEGIN GAME ");
        StopCoroutine(BeginGame());
    }

    public void RestartGame()
    {
        bannerAdmob.Hide();
        SceneManager.LoadScene("5 - Game", LoadSceneMode.Single);
    }

    public void EndGame()
    {
        bannerAdmob.Show();


        InGame = false;
        axuADSShow++;

        
        int auxRandom = (int)Random.Range(3, 6);
        Debug.Log("AUX ADS: " + axuADSShow + "  Random:" + auxRandom);

        if (axuADSShow >= auxRandom)
        {
            AdsManager.Instance.ShowAdNormal();
            axuADSShow = 0;
            Debug.Log("SHOW ADS NORMAL");
        }

        /*PONTOS ADICIONAS*/
        int addFinalPontos = (int)((TravelledDistance * gameLevel) / 2);
        print("Pontos adicionais: " + addFinalPontos);
        PontosGanhosnoJogo += addFinalPontos;
        /*PONTOS ADICIONAS*/

        TotalMoneyGame += MoneyGame;
        TotalPontosGeral += PontosGanhosnoJogo;

        

        if (TravelledDistance >= BestDistance)
            BestDistance = travelledDistance;

        if (GameLevel >= BestGameLevel)
            BestGameLevel = GameLevel;

        if (pointsOnGame >= BestPontuation)
            BestPontuation = pointsOnGame;



        AnalitycsManager.Instance.TotalMortes++;
        AnalitycsManager.Instance.TotalDistanciaViajada += TravelledDistance;


        Analytics.CustomEvent("ValoresFinais.GameOver", new Dictionary<string, object>
         {
            { "DobroMoeda", IsDoubleMoney },
            { "MoedasColetadas", TotalMoneyGame },
            { "DistanciaTotal", travelledDistance },
            { "PontosTotais", pointsOnGame }
         });

        Analytics.CustomEvent("EstatisticasPos.GameOver", new Dictionary<string, object>
        {
                {"TotalInimigosDestruidos", AnalitycsManager.Instance.TotalInimigosDestruidos },
                {"TotalAereosDestruidos", AnalitycsManager.Instance.TotalAereosDestruidos },
                {"TotalAquaticosDestruidos", AnalitycsManager.Instance.TotalAquaticosDestruidos },
                {"TotalTanquesDestruidos", AnalitycsManager.Instance.TotalTanquesDestruidos },

                {"TotalDinheiro", AnalitycsManager.Instance.TotalDinheiroGanho },
                {"TotalDinheiroPerdido", AnalitycsManager.Instance.TotalDinheiroPerdido },
                {"TotalMedalhas", AnalitycsManager.Instance.TotalMedalhaGanho },

                {"TotalDistanciaViajada", AnalitycsManager.Instance.TotalDistanciaViajada },
                {"TotalMortes", AnalitycsManager.Instance.TotalMortes },

                {"TotalGasolinasColetadas", AnalitycsManager.Instance.TotalGasolinasColetadas },
                {"TotalGasolinasDestruidas", AnalitycsManager.Instance.TotalGasolinasDestruidas },

                {"TiroEspecialUsados", AnalitycsManager.Instance.TotalBoosterTiroEspecialUsados },
                {"InvencibilidadeUsados", AnalitycsManager.Instance.TotalBoosterInvencibilidadeUsados },
                {"GasolinaInfinitaUsados", AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaUsados },

        });

        GooglePlayServices.Instance.PostScore(pointsOnGame, SkykingsGP.leaderboard_world_championship);

        /*if(temConexao)
            WebService.instance.SetScore(pointsOnGame, WebserviceID);*/

        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();

        return;

    }

    public string SaveGame()
    {

        string saveData = "";


        saveData += TotalPlayTime.ToString() + '|';
        LastSave = Time.time;

        //Naves Status
        for (int i = 0; i < naveComprada.Length; i++)
        {
            saveData +=
                naveComprada[i].ToString() + '%' +

                upgradeVelocidade[i].ToString() + '%' +
                upgradeMovimento[i].ToString() + '%' +
                upgradeTanque[i].ToString() + '%' +
                upgradeMagnetude[i].ToString() + '%' +
                upgradePoder[i].ToString() + '%' +

                valorVelocidade[i].ToString() + '%' +
                valorMovimento[i].ToString() + '%' +
                valorTanque[i].ToString() + '%' +
                valorMagnetude[i].ToString() + '%' +
                valorPoder[i].ToString() + '#';
        }

        saveData += '|';


        /* Info Game */

        saveData +=

            TotalMoneyGame.ToString() + '%' +
            MedalMoney.ToString() + '%' +
            BestDistance.ToString() + '%' +
            TotalPontosGeral.ToString() + '%' +
            BestGameLevel.ToString() + '%' +

            QuantityInvencible.ToString() + '%' +
            QuantityInfiniteFuel.ToString() + '%' +
            QuantityEspecialShoot.ToString() + '%' +


            TimeInvencibility.ToString() + '%' +
            TimeFuelInfinite.ToString() + '%' +
            TimeSpecialShoot.ToString() + '%' +

            LevelInvencibility.ToString() + '%' +
            LevelFuelInifinite.ToString() + '%' +
            LevelSpecialShoot.ToString() + '%' +

            ValueBuyUpgradeInvisibility.ToString() + '%' +
            ValueBuyUpgradeFuelInfinite.ToString() + '%' +
            ValueBuyUpgradeSpecialShoot.ToString() + '%' +

            Idioma + '%' +

            Primeira_vez.ToString() + '%' +
            BestPontuation.ToString() + '|';

        saveData +=
            AnalitycsManager.Instance.TotalInimigosDestruidos.ToString() + '%' +
            AnalitycsManager.Instance.TotalAereosDestruidos.ToString() + '%' +
            AnalitycsManager.Instance.TotalTanquesDestruidos.ToString() + '%' +
            AnalitycsManager.Instance.TotalAquaticosDestruidos.ToString() + '%' +

            AnalitycsManager.Instance.TotalMortes.ToString() + '%' +
            AnalitycsManager.Instance.TotalJogosIniciado.ToString() + '%' +

            AnalitycsManager.Instance.TotalDinheiroGanho.ToString() + '%' +
            AnalitycsManager.Instance.TotalDinheiroGasto.ToString() + '%' +
            AnalitycsManager.Instance.TotalDinheiroPerdido.ToString() + '%' +

            AnalitycsManager.Instance.TotalMedalhaGanho.ToString() + '%' +
            AnalitycsManager.Instance.TotalMedalhaGasto.ToString() + '%' +

            AnalitycsManager.Instance.TotalDistanciaViajada.ToString() + '%' +

            AnalitycsManager.Instance.TotalGasolinasColetadas.ToString() + '%' +
            AnalitycsManager.Instance.TotalGasolinasDestruidas.ToString() + '%' +

            AnalitycsManager.Instance.TotalBoosterInvencibilidadeComprados.ToString() + '%' +
            AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaComprados.ToString() + '%' +
            AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados.ToString() + '%' +

            AnalitycsManager.Instance.TotalBoosterInvencibilidadeUsados.ToString() + '%' +
            AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaUsados.ToString() + '%' +
            AnalitycsManager.Instance.TotalBoosterTiroEspecialUsados.ToString() + '%' +

            AnalitycsManager.Instance.NomeUsuario + '%' +
            AnalitycsManager.Instance.Idade.ToString() + '%' +
            AnalitycsManager.Instance.Sexo + '%' +

            AnalitycsManager.Instance.TotalAppIniciado.ToString() + '|';

        saveData +=
            levelInimigos.ToString() + '%' +
            levelAquaticos.ToString() + '%' +
            levelTanques.ToString() + '%' +
            levelAereos.ToString() + '%' +
            levelCombustivelsColet.ToString() + '%' +
            levelCombustivelsDest.ToString() + '%' +
            levelMoedasColet.ToString() + '%' +
            levelMedalhasColet.ToString() + '%' +
            levelDistancia.ToString() + '%' +
            levelDinheiroGasto.ToString() + '%' +
            levelMortes.ToString() + '|';

        saveData +=
            inimigos_TotalRecebido.ToString() + '%' +
            aquaticos_TotalRecebido.ToString() + '%' +
            tanques_TotalRecebido.ToString() + '%' +
            aereos_TotalRecebidos.ToString() + '%' +
            combustivelColet_TotalRecebido.ToString() + '%' +
            EstagioLevelCombustivelsDest.ToString() + '%' +
            moedas_TotalRecebido.ToString() + '%' +
            medalhas_TotalRecebido.ToString() + '%' +
            distancia_TotalRecebido.ToString() + '%' +
            dinheiroGasto_TotalRecebido.ToString() + '%' +
            morter_TotalRecebido.ToString() + '|';

        saveData +=
            totalRecebido_inimigos.ToString() + '|';


        //print ("Salvado no string: " + saveData);

        return saveData;


    }

    public void LoadGame(string saveData)
    {

        if (saveData == "")
            return;

        string[] data = saveData.Split('|');

        //Time
        string[] misc = data[0].Split('%');
        PlayTimeSinceSave = float.Parse(misc[0]);

        //Ships
        string[] ship = data[1].Split('#');


        for (int i = 0; i < naveComprada.Length; i++)
        {
            string[] shipStatus = ship[i].Split('%');

            naveComprada[i] = int.Parse(shipStatus[0]);

            upgradeVelocidade[i] = int.Parse(shipStatus[1]);
            upgradeMovimento[i] = int.Parse(shipStatus[2]);
            upgradeTanque[i] = int.Parse(shipStatus[3]);
            upgradeMagnetude[i] = int.Parse(shipStatus[4]);
            upgradePoder[i] = int.Parse(shipStatus[5]);

            valorVelocidade[i] = int.Parse(shipStatus[6]);
            valorMovimento[i] = int.Parse(shipStatus[7]);
            valorTanque[i] = int.Parse(shipStatus[8]);
            valorMagnetude[i] = int.Parse(shipStatus[9]);
            valorPoder[i] = int.Parse(shipStatus[10]);

        }

        //Status Game
        string[] status = data[2].Split('%');

        TotalMoneyGame = int.Parse(status[0]);
        MedalMoney = int.Parse(status[1]);
        BestDistance = float.Parse(status[2]);
        TotalPontosGeral = int.Parse(status[3]);
        BestGameLevel = int.Parse(status[4]);

        QuantityInvencible = int.Parse(status[5]);
        QuantityInfiniteFuel = int.Parse(status[6]);
        QuantityEspecialShoot = int.Parse(status[7]);


        TimeInvencibility = float.Parse(status[8]);
        TimeFuelInfinite = float.Parse(status[9]);
        TimeSpecialShoot = float.Parse(status[10]);

        LevelInvencibility = int.Parse(status[11]);
        LevelFuelInifinite = int.Parse(status[12]);
        LevelSpecialShoot = int.Parse(status[13]);

        ValueBuyUpgradeInvisibility = int.Parse(status[14]);
        ValueBuyUpgradeFuelInfinite = int.Parse(status[15]);
        ValueBuyUpgradeSpecialShoot = int.Parse(status[16]);

        Idioma = status[17];

        Primeira_vez = bool.Parse(status[18]);

        BestPontuation = int.Parse(status[19]);


        string[] anl = data[3].Split('%');

        AnalitycsManager.Instance.TotalInimigosDestruidos = int.Parse(anl[0]);
        AnalitycsManager.Instance.TotalAereosDestruidos = int.Parse(anl[1]);
        AnalitycsManager.Instance.TotalTanquesDestruidos = int.Parse(anl[2]);
        AnalitycsManager.Instance.TotalAquaticosDestruidos = int.Parse(anl[3]);

        AnalitycsManager.Instance.TotalMortes = int.Parse(anl[4]);
        AnalitycsManager.Instance.TotalJogosIniciado = int.Parse(anl[5]);

        AnalitycsManager.Instance.TotalDinheiroGanho = int.Parse(anl[6]);
        AnalitycsManager.Instance.TotalDinheiroGasto = int.Parse(anl[7]);
        AnalitycsManager.Instance.TotalDinheiroPerdido = int.Parse(anl[8]);

        AnalitycsManager.Instance.TotalMedalhaGanho = int.Parse(anl[9]);
        AnalitycsManager.Instance.TotalMedalhaGasto = int.Parse(anl[10]);

        AnalitycsManager.Instance.TotalDistanciaViajada = float.Parse(anl[11]);

        AnalitycsManager.Instance.TotalGasolinasColetadas = int.Parse(anl[12]);
        AnalitycsManager.Instance.TotalGasolinasDestruidas = int.Parse(anl[13]);

        AnalitycsManager.Instance.TotalBoosterInvencibilidadeComprados = int.Parse(anl[14]);
        AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaComprados = int.Parse(anl[15]);
        AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados = int.Parse(anl[16]);

        AnalitycsManager.Instance.TotalBoosterInvencibilidadeUsados = int.Parse(anl[17]);
        AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaUsados = int.Parse(anl[18]);
        AnalitycsManager.Instance.TotalBoosterTiroEspecialUsados = int.Parse(anl[19]);

        AnalitycsManager.Instance.NomeUsuario = anl[20];
        AnalitycsManager.Instance.Idade = int.Parse(anl[21]);
        AnalitycsManager.Instance.Sexo = anl[22];

        AnalitycsManager.Instance.TotalAppIniciado = int.Parse(anl[23]);

        string[] conquist = data[4].Split('%');

        levelInimigos = int.Parse(conquist[0]);
        levelAquaticos = int.Parse(conquist[1]);
        levelTanques = int.Parse(conquist[2]);
        levelAereos = int.Parse(conquist[3]);
        levelCombustivelsColet = int.Parse(conquist[4]);
        levelCombustivelsDest = int.Parse(conquist[5]);
        levelMoedasColet = int.Parse(conquist[6]);
        levelMedalhasColet = int.Parse(conquist[7]);
        levelDistancia = int.Parse(conquist[8]);
        levelDinheiroGasto = int.Parse(conquist[9]);
        levelMortes = int.Parse(conquist[10]);


        string[] estagiobot = data[5].Split('%');

        inimigos_TotalRecebido = int.Parse(estagiobot[0]);
        aquaticos_TotalRecebido = int.Parse(estagiobot[1]);
        tanques_TotalRecebido = int.Parse(estagiobot[2]);
        aereos_TotalRecebidos = int.Parse(estagiobot[3]);
        combustivelColet_TotalRecebido = int.Parse(estagiobot[4]);
        combustivelDest_TotalRecebido = int.Parse(estagiobot[5]);
        moedas_TotalRecebido = int.Parse(estagiobot[6]);
        medalhas_TotalRecebido = int.Parse(estagiobot[7]);
        distancia_TotalRecebido = int.Parse(estagiobot[8]);
        dinheiroGasto_TotalRecebido = int.Parse(estagiobot[9]);
        morter_TotalRecebido = int.Parse(estagiobot[10]);

        string[] recebido = data[6].Split('%');

        totalRecebido_inimigos = int.Parse(recebido[0]);


    }

    public void SaveLocal()
    {

        ZPlayerPrefs.SetString("SkyKingsGame", SaveGame());
        // print(ZPlayerPrefs.GetRowString("SkyKingsGame"));
        ZPlayerPrefs.Save();
        // PlayerPrefs.SetString("SkyKingsGame", SaveGame());
        // PlayerPrefs.Save();

    }
    public void LoadLocal()
    {

        /*if (PlayerPrefs.HasKey ("SkyKingsGame"))
            LoadGame (PlayerPrefs.GetString ("SkyKingsGame"));*/

        if (ZPlayerPrefs.HasKey("SkyKingsGame"))
        {
            LoadGame(ZPlayerPrefs.GetString("SkyKingsGame"));
            Debug.Log("Loading LOCAL! :)   - FEITO \n" + ZPlayerPrefs.GetString("SkyKingsGame"));
        }

        else
        {
            Inicilizar();
            Debug.Log("Load não feito, pq não tinha arquivo! Inicializou :) ");
        }

    }

    public void Inicilizar()
    {

        if (!ZPlayerPrefs.HasKey("SkyKingsGame"))
        {

            Debug.LogWarning("INICIALIZAR: NÂO TEM SAVE LOCAL - Primeira vez!");

            Primeira_vez = true;
            //Idioma = "en-us";

            TotalMoneyGame = 150;
            MedalMoney = 5;
            BestDistance = 0;
            TotalPontosGeral = 0;
            BestGameLevel = 0;
            BestPontuation = 0;

            QuantityInvencible = 3;
            QuantityInfiniteFuel = 3;
            QuantityEspecialShoot = 3;


            TimeInvencibility = 3.5f;
            TimeFuelInfinite = 3.5f;
            TimeSpecialShoot = 3.5f;

            LevelInvencibility = 1;
            LevelFuelInifinite = 1;
            LevelSpecialShoot = 1;

            ValueBuyUpgradeInvisibility = 10;
            ValueBuyUpgradeFuelInfinite = 10;
            ValueBuyUpgradeSpecialShoot = 10;

            for (int i = 0; i < 16; i++)
            {
                upgradeVelocidade[i] = 0;
                upgradeMovimento[i] = 0;
                upgradeTanque[i] = 0;
                upgradeMagnetude[i] = 0;
                upgradePoder[i] = 0;

                valorVelocidade[i] = 20;
                valorMovimento[i] = 20;
                valorTanque[i] = 20;
                valorMagnetude[i] = 20;
                valorPoder[i] = 20;
            }

            AnalitycsManager.Instance.TotalInimigosDestruidos = 0;
            AnalitycsManager.Instance.TotalAereosDestruidos = 0;
            AnalitycsManager.Instance.TotalTanquesDestruidos = 0;
            AnalitycsManager.Instance.TotalAquaticosDestruidos = 0;

            AnalitycsManager.Instance.TotalMortes = 0;
            AnalitycsManager.Instance.TotalJogosIniciado = 0;

            AnalitycsManager.Instance.TotalDinheiroGanho = 0;
            AnalitycsManager.Instance.TotalDinheiroGasto = 0;
            AnalitycsManager.Instance.TotalDinheiroPerdido = 0;

            AnalitycsManager.Instance.TotalMedalhaGanho = 0;
            AnalitycsManager.Instance.TotalMedalhaGasto = 0;

            AnalitycsManager.Instance.TotalDistanciaViajada = 0;

            AnalitycsManager.Instance.TotalGasolinasColetadas = 0;
            AnalitycsManager.Instance.TotalGasolinasDestruidas = 0;

            AnalitycsManager.Instance.TotalBoosterInvencibilidadeComprados = 0;
            AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaComprados = 0;
            AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados = 0;

            AnalitycsManager.Instance.TotalBoosterInvencibilidadeUsados = 0;
            AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaUsados = 0;
            AnalitycsManager.Instance.TotalBoosterTiroEspecialUsados = 0;

            AnalitycsManager.Instance.TotalAppIniciado = 0;

            AnalitycsManager.Instance.NomeUsuario = "Anonimo";
            AnalitycsManager.Instance.Idade = 0;
            AnalitycsManager.Instance.Sexo = "ND";


            levelInimigos = 0;
            levelAquaticos = 0;
            levelTanques = 0;
            levelAereos = 0;
            levelCombustivelsColet = 0;
            levelCombustivelsDest = 0;
            levelMoedasColet = 0;
            levelMedalhasColet = 0;
            levelDistancia = 0;
            levelDinheiroGasto = 0;
            levelMortes = 0;


            inimigos_TotalRecebido = 0;
            aquaticos_TotalRecebido = 0;
            tanques_TotalRecebido = 0;
            aereos_TotalRecebidos = 0;
            combustivelColet_TotalRecebido = 0;
            combustivelDest_TotalRecebido = 0;
            moedas_TotalRecebido = 0;
            medalhas_TotalRecebido = 0;
            distancia_TotalRecebido = 0;
            dinheiroGasto_TotalRecebido = 0;
            morter_TotalRecebido = 0;

            totalRecebido_inimigos = 0;

            //SaveLocal ();

            GooglePlayServices.Instance.Login("Login");

        }
        else
        {
            Primeira_vez = false;
            Debug.LogWarning("INICIALIZAR: AQUI TEM SAVE LOCAL");
            GooglePlayServices.Instance.Login("Login");

        }

    }

    public void LOADSCENE(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

}
