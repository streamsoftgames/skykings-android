﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using LitJson;

public class NavesManager : MonoBehaviour
{
    public static NavesManager Instance { get; set; }

    public List<Naves> databaseShips = new List<Naves>();


    public Sprite[] navesIcons;

    private string jsonString;
    //public string pathLink = "/StreamingAssets/NavesStatus";

    private JsonData jdNaves;
    private JsonData jdNavesLoadInfo;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        navesIcons = Resources.LoadAll<Sprite>("ships");
    }

    void Start()
    {

        string jNaves = Resources.Load<TextAsset>("NavesStatus").text;
        jdNaves = JsonMapper.ToObject(jNaves);

        DataBaseConstruction();

    }

    public Sprite GetIconSpiteNave(string name)
    {
        Sprite aux;

        for (int i = 0; i < navesIcons.Length; i++)
        {
            if (navesIcons[i].name.Equals(name))
            {
                aux = navesIcons[i];
                return aux;
            }
        }

        return null;
    }
    public Naves GetItem(int id)
    {
        for (int i = 0; i < databaseShips.Count; i++)
        {

            if (databaseShips[i].ID == id)
            {
                return databaseShips[i];

            }
        }

        return null;
    }



    void DataBaseConstruction()
    {

        for (int i = 0; i < jdNaves.Count; i++)
        {
            databaseShips.Add(new Naves(
                (int)jdNaves[i]["id"],
                (int)jdNaves[i]["comprada"],

                jdNaves[i]["nome"].ToString(),
                jdNaves[i]["descricao"].ToString(),
                (int)jdNaves[i]["valor"],

                (int)jdNaves[i]["status"]["velocidadeInicial"],
                (int)jdNaves[i]["status"]["movimentoInicial"],
                (int)jdNaves[i]["status"]["tanqueTotalInicial"],
                (int)jdNaves[i]["status"]["magnetudeInicial"],
                (int)jdNaves[i]["status"]["poderInicial"],

                float.Parse(jdNaves[i]["aprimoramentos"]["velocidade"].ToString()),
                float.Parse(jdNaves[i]["aprimoramentos"]["movimento"].ToString()),
                float.Parse(jdNaves[i]["aprimoramentos"]["tanque"].ToString()),
                float.Parse(jdNaves[i]["aprimoramentos"]["magnetude"].ToString()),
                float.Parse(jdNaves[i]["aprimoramentos"]["poder"].ToString()),

                GetIconSpiteNave(jdNaves[i]["slug"].ToString()),
                jdNaves[i]["slug"].ToString()
            ));
        }


    }
}


[System.Serializable]
public class Naves
{
    //Infos
    public int ID { get; set; }
    public int BUY { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Value { get; set; }
    //Status
    public int VelocidadeInicial { get; set; }
    public int MovimentoInicial { get; set; }
    public int TanqueTotalInicial { get; set; }
    public int MagnetudeInicial { get; set; }
    public int PoderInicial { get; set; }
    //Aprimoramentos
    public float VelocidadeAprimoramento { get; set; }
    public float MovimentoAprimoramento { get; set; }
    public float TanqueAprimoramento { get; set; }
    public float MagnetudeAprimoramento { get; set; }
    public float PoderAprimoramento { get; set; }
    //Outros
    public Sprite SpriteIcon { get; set; }
    public string Slug { get; set; }

    public Naves(
        int id, int buy, string name, string description, int value, //Infos
        int velocidadeinicial, int movimentoinicial, int tanquetotalinicial, int magnetudeInicial, int poderInicial, //Status
        float velocidade, float movimento, float tanque, float magnetude, float poder,
        Sprite spriteicon, string slug //Outros
        )
    {
        //Infos
        this.ID = id;
        this.BUY = buy;
        this.Name = name;
        this.Description = description;
        this.Value = value;
        //Status
        this.VelocidadeInicial = velocidadeinicial;
        this.MovimentoInicial = movimentoinicial;
        this.TanqueTotalInicial = tanquetotalinicial;
        this.MagnetudeInicial = magnetudeInicial;
        this.PoderInicial = poderInicial;
        //Aprimoramentos
        this.VelocidadeAprimoramento = velocidade;
        this.MovimentoAprimoramento = movimento;
        this.TanqueAprimoramento = tanque;
        this.MagnetudeAprimoramento = magnetude;
        this.PoderAprimoramento = poder;
        //Outros
        this.SpriteIcon = spriteicon;
        this.Slug = slug;
    }

    public Naves()
    {
        this.ID = -1;
    }

}

