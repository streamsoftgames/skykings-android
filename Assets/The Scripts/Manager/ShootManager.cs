﻿using UnityEngine;
using System.Collections;

public class ShootManager : MonoBehaviour
{

    public float velocity = 100f;
    Rigidbody2D _r;
    public string Type;
    public bool faceLeft = true;



    // Use this for initialization
    void Start()
    {
        _r = GetComponent<Rigidbody2D>();
        StartCoroutine(DestroyTime());
    }

    
    void Update()
    {

        if (Type.Equals("Player"))
        {
            _r.velocity = new Vector2(_r.velocity.x, 1 * (Time.deltaTime * velocity));
        }

        else if (Type.Equals("Tank"))
        {
            if (!faceLeft)
            {
                _r.velocity = new Vector2(1, -1) * (velocity * Time.deltaTime);
            }

            else
            {
                _r.velocity = new Vector2(-1, -1) * (velocity * Time.deltaTime);
            }
        }
        
    }

    IEnumerator DestroyTime()
    {
        if(Type.Equals("Player"))
            yield return new WaitForSeconds(1f);
        else
            yield return new WaitForSeconds(3f);

        Destroy(gameObject);
    }


}
