﻿using UnityEngine;
using System.Collections;

public class AnalitycsManager : MonoBehaviour
{

    public static AnalitycsManager Instance { get; private set; }



    #region Encapsuladores
    public int TotalInimigosDestruidos
    {
        get
        {
            return totalInimigosDestruidos;
        }

        set
        {
            totalInimigosDestruidos = value;
        }
    }

    public int TotalAereosDestruidos
    {
        get
        {
            return totalAereosDestruidos;
        }

        set
        {
            totalAereosDestruidos = value;
        }
    }

    public int TotalTanquesDestruidos
    {
        get
        {
            return totalTanquesDestruidos;
        }

        set
        {
            totalTanquesDestruidos = value;
        }
    }

    public int TotalAquaticosDestruidos
    {
        get
        {
            return totalAquaticosDestruidos;
        }

        set
        {
            totalAquaticosDestruidos = value;
        }
    }

    public int TotalMortes
    {
        get
        {
            return totalMortes;
        }

        set
        {
            totalMortes = value;
        }
    }

    public int TotalDinheiroGanho
    {
        get
        {
            return totalDinheiroGanho;
        }

        set
        {
            totalDinheiroGanho = value;
        }
    }

    public int TotalDinheiroGasto
    {
        get
        {
            return totalDinheiroGasto;
        }

        set
        {
            totalDinheiroGasto = value;
        }
    }

    public int TotalMedalhaGanho
    {
        get
        {
            return totalMedalhaGanho;
        }

        set
        {
            totalMedalhaGanho = value;
        }
    }

    public int TotalMedalhaGasto
    {
        get
        {
            return totalMedalhaGasto;
        }

        set
        {
            totalMedalhaGasto = value;
        }
    }

    public float TotalDistanciaViajada
    {
        get
        {
            return totalDistanciaViajada;
        }

        set
        {
            totalDistanciaViajada = value;
        }
    }

    public int TotalGasolinasColetadas
    {
        get
        {
            return totalGasolinasColetadas;
        }

        set
        {
            totalGasolinasColetadas = value;
        }
    }

    public int TotalGasolinasDestruidas
    {
        get
        {
            return totalGasolinasDestruidas;
        }

        set
        {
            totalGasolinasDestruidas = value;
        }
    }

    public string NomeUsuario
    {
        get
        {
            return nomeUsuario;
        }

        set
        {
            nomeUsuario = value;
        }
    }

    public int Idade
    {
        get
        {
            return idade;
        }

        set
        {
            idade = value;
        }
    }

    public string Sexo
    {
        get
        {
            return sexo;
        }

        set
        {
            sexo = value;
        }
    }

    public int TotalJogosIniciado
    {
        get
        {
            return totalJogosIniciado;
        }

        set
        {
            totalJogosIniciado = value;
        }
    }

    public int TotalBoosterInvencibilidadeComprados
    {
        get
        {
            return totalBoosterInvencibilidadeComprados;
        }

        set
        {
            totalBoosterInvencibilidadeComprados = value;
        }
    }

    public int TotalBoosterGasolinaInfinitaComprados
    {
        get
        {
            return totalBoosterGasolinaInfinitaComprados;
        }

        set
        {
            totalBoosterGasolinaInfinitaComprados = value;
        }
    }

    public int TotalBoosterTiroEspecialComprados
    {
        get
        {
            return totalBoosterTiroEspecialComprados;
        }

        set
        {
            totalBoosterTiroEspecialComprados = value;
        }
    }

    public int TotalBoosterInvencibilidadeUsados
    {
        get
        {
            return totalBoosterInvencibilidadeUsados;
        }

        set
        {
            totalBoosterInvencibilidadeUsados = value;
        }
    }

    public int TotalBoosterGasolinaInfinitaUsados
    {
        get
        {
            return totalBoosterGasolinaInfinitaUsados;
        }

        set
        {
            totalBoosterGasolinaInfinitaUsados = value;
        }
    }

    public int TotalBoosterTiroEspecialUsados
    {
        get
        {
            return totalBoosterTiroEspecialUsados;
        }

        set
        {
            totalBoosterTiroEspecialUsados = value;
        }
    }

    public int TotalDinheiroPerdido
    {
        get
        {
            return totalDinheiroPerdido;
        }

        set
        {
            totalDinheiroPerdido = value;
        }
    }

    public int TotalAppIniciado
    {
        get
        {
            return totalAppIniciado;
        }

        set
        {
            totalAppIniciado = value;
        }
    }


    #endregion

    //Legenda: ** Implementado

    /* ANALITYCS */
    [SerializeField]    private int totalInimigosDestruidos;//**
    [SerializeField]    private int totalAereosDestruidos; //**
    [SerializeField]    private int totalTanquesDestruidos;//**
    [SerializeField]    private int totalAquaticosDestruidos;//**

    [SerializeField]    private int totalMortes; //**
    [SerializeField]    private int totalJogosIniciado; //**
    [SerializeField]    private int totalAppIniciado;  //**

    [SerializeField]    private int totalDinheiroGanho;  //**
    [SerializeField]    private int totalDinheiroGasto;
    [SerializeField]    private int totalDinheiroPerdido; //**

    [SerializeField]    private int totalMedalhaGanho; //**
    [SerializeField]    private int totalMedalhaGasto;//**

    [SerializeField]    private float totalDistanciaViajada; //**

    [SerializeField]    private int totalGasolinasColetadas; //**
    [SerializeField]    private int totalGasolinasDestruidas; //**

    /*LOJA*/
    [SerializeField]    private int totalBoosterInvencibilidadeComprados;
    [SerializeField]    private int totalBoosterGasolinaInfinitaComprados;
    [SerializeField]    private int totalBoosterTiroEspecialComprados;

    [SerializeField]    private int totalBoosterInvencibilidadeUsados; //**
    [SerializeField]    private int totalBoosterGasolinaInfinitaUsados; //**
    [SerializeField]    private int totalBoosterTiroEspecialUsados; //**



    [SerializeField]    private string nomeUsuario; 
    [SerializeField]    private int idade;
    [SerializeField]    private string sexo;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Debug.Log("ANALITICS INSTANCIADO");
        }
       


    }
}

