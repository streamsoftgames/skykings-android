﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionManager : MonoBehaviour {

    public Button soundButton;
    public Button loginButton1;
    public Button loginButton2;
    public Color[] colors;

    public Sprite[] spriteSoundButton;


	void Update () {
        if (GooglePlayServices.Instance.IsAuthenticated)
        {
            loginButton1.image.color = colors[0]; //Color.green;
            loginButton2.image.color = colors[0]; //Color.green;
        }
        else
        {
            loginButton1.image.color = colors[1]; //Color.red;
            loginButton2.image.color = colors[1]; //Color.red;
        }
    }

    public void SoundControl()
    {
		if (AudioListener.pause == false) //PAUSAR
		{
			soundButton.transform.GetChild (0).GetComponent<Image>().sprite = spriteSoundButton [0];
			AudioListener.pause = true;
		} 
		else //DESPAUSAR 
		{
			soundButton.transform.GetChild (0).GetComponent<Image>().sprite = spriteSoundButton [1];
			AudioListener.pause = false;
		}
    }

    public void Login()
    {
        
        GooglePlayServices.Instance.Login("Login");
    }
    public void Save()
    {
        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
    }
    public void Load()
    {
        GooglePlayServices.Instance.LoadFromCloud();
    }

	public void SHOWUI ()
	{
		GooglePlayServices.Instance.showUI ();
	}
}
