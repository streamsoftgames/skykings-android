﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopManager : MonoBehaviour
{

    //public GameObject[] abas;
    public GameObject painelBooster;
    public GameObject painelIAP;
    public GameObject painelConquistas;

    public Image btnBooster;
    public Image btnIAP;
    public Image btnConquista;

    public Text dinheiro;
    public Text medalsCash;

    public Text quantidadeInvencibilidade;
    public Text quantidadeTanqueCheio;
    public Text quantidadeTiroEspecial;

    public Text priceInvencibilidade;
    public Text priceTanqueCheio;
    public Text priceTiroEspecial;


    public Text nivelUpgradeInvencibilidade;
    public Text nivelUpgradeTiroEspecial;
    public Text nivelUpgradeTanqueCheio;

    public Text timeInvencibility;
    public Text timeFuel;
    public Text timeShoot;

    public Text priceUpgradeInvencibilidade;
    public Text priceUpgradeTanqueCheio;
    public Text priceUpgradeTiroEspecial;


    public int valueBuyInvisibility = 30;
    public int valueBuyFuelInfinite = 30;
    public int valueBuyEspecialShoot = 30;

    

    public Image Logo;
    public Sprite[] logos;

    void Start()
    {
        //btnBooster.Select();

		PainelControle ("IAP");
    }

    void Update()
    {

        dinheiro.text = GameManager._Instance.TotalMoneyGame.ToString();
        medalsCash.text = GameManager._Instance.MedalMoney.ToString();

        quantidadeInvencibilidade.text = GameManager._Instance.QuantityInvencible.ToString() + "x";
        quantidadeTanqueCheio.text = GameManager._Instance.QuantityInfiniteFuel.ToString() + "x";
        quantidadeTiroEspecial.text = GameManager._Instance.QuantityEspecialShoot.ToString() + "x";

        nivelUpgradeInvencibilidade.text = "LVL "+ GameManager._Instance.LevelInvencibility.ToString();
        nivelUpgradeTanqueCheio.text = "LVL " + GameManager._Instance.LevelFuelInifinite.ToString();
        nivelUpgradeTiroEspecial.text = "LVL " + GameManager._Instance.LevelSpecialShoot.ToString();

        timeInvencibility.text = GameManager._Instance.TimeInvencibility.ToString() + "'s";
        timeFuel.text = GameManager._Instance.TimeFuelInfinite.ToString() + "'s";
        timeShoot.text = GameManager._Instance.TimeSpecialShoot.ToString() + "'s";

        priceInvencibilidade.text = valueBuyInvisibility.ToString();
        priceTanqueCheio.text = valueBuyFuelInfinite.ToString();
        priceTiroEspecial.text =  valueBuyEspecialShoot.ToString();

        priceUpgradeInvencibilidade.text = GameManager._Instance.ValueBuyUpgradeInvisibility.ToString();
        priceUpgradeTanqueCheio.text = GameManager._Instance.ValueBuyUpgradeFuelInfinite.ToString();
        priceUpgradeTiroEspecial.text =  GameManager._Instance.ValueBuyUpgradeSpecialShoot.ToString();


    }

    public void PainelControle(string painel)
    {
        if (painel.Equals("Boosters"))
        {
            Logo.sprite = logos[0];

            painelBooster.SetActive(true);
            painelConquistas.SetActive(false);
            painelIAP.SetActive(false);

            btnBooster.color = new Color(0, 255, 0, 255);
            btnConquista.color = new Color(165, 165, 165, 255);
            btnIAP.color = new Color(165, 165, 165, 255);

        }
        else if (painel.Equals("IAP"))
        {
            Logo.sprite = logos[1];

            painelBooster.SetActive(false);
            painelConquistas.SetActive(false);
            painelIAP.SetActive(true);

            btnBooster.color = new Color(165, 165, 165, 255);
            btnConquista.color = new Color(165, 165, 165, 255);
            btnIAP.color = new Color(0, 255, 0, 255);

        }
        else if (painel.Equals("Conquistas"))
        {

            Logo.sprite = logos[2];

            painelBooster.SetActive(false);
            painelConquistas.SetActive(true);
            painelIAP.SetActive(false);

            btnBooster.color = new Color(165, 165, 165, 255);
            btnConquista.color = new Color(0, 255, 0, 255);
            btnIAP.color = new Color(165, 165, 165, 255);

        }

    }


    /*********************  MEDALHAS  *********************/
    /* Comprar Quantidade */
    public void BuyInvencibility()
    {
        if (GameManager._Instance.TotalMoneyGame >= valueBuyInvisibility && GameManager._Instance.MedalMoney > 0 && GameManager._Instance.QuantityInvencible < 99)
        {
            GameManager._Instance.QuantityInvencible++;
            GameManager._Instance.TotalMoneyGame -= valueBuyInvisibility;
            GameManager._Instance.MedalMoney--;
            AnalitycsManager.Instance.TotalBoosterInvencibilidadeComprados++;

            AnalitycsManager.Instance.TotalDinheiroGasto += valueBuyInvisibility;
            AnalitycsManager.Instance.TotalMedalhaGasto++;
        }
    }
    public void BuyFuelGas()
    {
        if (GameManager._Instance.TotalMoneyGame >= valueBuyFuelInfinite && GameManager._Instance.MedalMoney > 0 && GameManager._Instance.QuantityInfiniteFuel < 99)
        {
            GameManager._Instance.QuantityInfiniteFuel++;
            GameManager._Instance.TotalMoneyGame -= valueBuyFuelInfinite;
            GameManager._Instance.MedalMoney--;
            AnalitycsManager.Instance.TotalBoosterGasolinaInfinitaComprados++;

            AnalitycsManager.Instance.TotalDinheiroGasto += valueBuyFuelInfinite;
            AnalitycsManager.Instance.TotalMedalhaGasto++;
        }
    }
    public void BuySpecialShot()
    {
        if (GameManager._Instance.TotalMoneyGame >= valueBuyEspecialShoot && GameManager._Instance.MedalMoney > 0 && GameManager._Instance.QuantityEspecialShoot < 99)
        {
            GameManager._Instance.QuantityEspecialShoot++;
            GameManager._Instance.TotalMoneyGame -= valueBuyEspecialShoot;
            GameManager._Instance.MedalMoney--;
            AnalitycsManager.Instance.TotalBoosterTiroEspecialComprados++;

            AnalitycsManager.Instance.TotalDinheiroGasto += valueBuyEspecialShoot;
            AnalitycsManager.Instance.TotalMedalhaGasto++;
        }
    }
    /* Comprar Upgrade */
    public void BuyUpgradeInvencibility()
    {
        if(GameManager._Instance.MedalMoney >= GameManager._Instance.ValueBuyUpgradeInvisibility && GameManager._Instance.LevelInvencibility < 5)
        {
            GameManager._Instance.LevelInvencibility++;
            GameManager._Instance.TimeInvencibility += 1.5f;
            GameManager._Instance.MedalMoney -= GameManager._Instance.ValueBuyUpgradeInvisibility;
            GameManager._Instance.ValueBuyUpgradeInvisibility = (int)(GameManager._Instance.ValueBuyUpgradeInvisibility * 2.2f);

            AnalitycsManager.Instance.TotalMedalhaGasto += GameManager._Instance.ValueBuyUpgradeInvisibility;
        }
        else
        {
            Debug.Log("NAO TEM MOEDA OU LVL MAIOR");
        }
    }
    public void BuyUpgradeGAS()
    {
        if(GameManager._Instance.MedalMoney >= GameManager._Instance.ValueBuyUpgradeFuelInfinite && GameManager._Instance.LevelFuelInifinite < 5)
        {
            GameManager._Instance.LevelFuelInifinite++;
            GameManager._Instance.TimeFuelInfinite += 1.5f;
            GameManager._Instance.MedalMoney -= GameManager._Instance.ValueBuyUpgradeFuelInfinite;
            GameManager._Instance.ValueBuyUpgradeFuelInfinite = (int)(GameManager._Instance.ValueBuyUpgradeFuelInfinite * 2.2f);

            AnalitycsManager.Instance.TotalMedalhaGasto += GameManager._Instance.ValueBuyUpgradeFuelInfinite;
        }
        else
        {
            Debug.Log("NAO TEM MOEDA OU LVL MAIOR");
        }
    }
    public void BuyUpgradeShoot()
    {
        if(GameManager._Instance.MedalMoney >= GameManager._Instance.ValueBuyUpgradeSpecialShoot && GameManager._Instance.LevelSpecialShoot < 5)
        {
            GameManager._Instance.LevelSpecialShoot++;
            GameManager._Instance.TimeSpecialShoot += 1.5f;
            GameManager._Instance.MedalMoney -= GameManager._Instance.ValueBuyUpgradeSpecialShoot;
            GameManager._Instance.ValueBuyUpgradeSpecialShoot = (int)(GameManager._Instance.ValueBuyUpgradeSpecialShoot * 2.2f);

            AnalitycsManager.Instance.TotalMedalhaGasto += GameManager._Instance.ValueBuyUpgradeSpecialShoot;
        }
        else
        {
            Debug.Log("NAO TEM MOEDA OU LVL MAIOR");
        }
    }

    


}
