﻿// This code automatically generated by TableCodeGen
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVTranslate : MonoBehaviour
{
	public class Row
	{
		public string ID;
		public string English;
		public string Portuguese;
		public string Spanish;
		public string Chinese;

	}

    static public CSVTranslate Instance;

    public TextAsset file;

    List<Row> rowList = new List<Row>();
    bool isLoaded = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            print("CSV INSTANCIADO");
        }
        else
            print("CSV já INSTANCIADO");
    }

    void Start()
    {
        Load(file);

        //print(Find_ID("OPTION_TITULO").English);
        
    }

	public bool IsLoaded()
	{
		return isLoaded;
	}

	public List<Row> GetRowList()
	{
		return rowList;
	}

	public void Load(TextAsset csv)
	{
		rowList.Clear();
		string[][] grid = CsvParser2.Parse(csv.text);
		for(int i = 1 ; i < grid.Length ; i++)
		{
			Row row = new Row();
			row.ID = grid[i][0];
			row.English = grid[i][1];
			row.Portuguese = grid[i][2];
			row.Spanish = grid[i][3];
			row.Chinese = grid[i][4];

			rowList.Add(row);
		}
		isLoaded = true;
	}

	public int NumRows()
	{
		return rowList.Count;
	}

	public Row GetAt(int i)
	{
		if(rowList.Count <= i)
			return null;
		return rowList[i];
	}

	public Row Find_ID(string find)
	{
		return rowList.Find(x => x.ID == find);
	}
	public List<Row> FindAll_ID(string find)
	{
		return rowList.FindAll(x => x.ID == find);
	}
	public Row Find_English(string find)
	{
		return rowList.Find(x => x.English == find);
	}
	public List<Row> FindAll_English(string find)
	{
		return rowList.FindAll(x => x.English == find);
	}
	public Row Find_Portuguese(string find)
	{
		return rowList.Find(x => x.Portuguese == find);
	}
	public List<Row> FindAll_Portuguese(string find)
	{
		return rowList.FindAll(x => x.Portuguese == find);
	}
	public Row Find_Spanish(string find)
	{
		return rowList.Find(x => x.Spanish == find);
	}
	public List<Row> FindAll_Spanish(string find)
	{
		return rowList.FindAll(x => x.Spanish == find);
	}
	public Row Find_Chinese(string find)
	{
		return rowList.Find(x => x.Chinese == find);
	}
	public List<Row> FindAll_Chinese(string find)
	{
		return rowList.FindAll(x => x.Chinese == find);
	}

}