﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TranslateText : MonoBehaviour
{
    public string _id;
    private CSVTranslate csv;


    void Start()
    {
        csv = CSVTranslate.Instance;
    }

    void Update()
    {
		if(_id != null)
			Localization();
        
    }

    public void Localization()
    {

        if (GameManager._Instance.Idioma.Equals("English"))
        {
            GetComponent<Text>().text = csv.Find_ID(_id).English;
            //print(c.Find_ID(_id).English);
        }

        else if (GameManager._Instance.Idioma.Equals("Portuguese"))
        {
            GetComponent<Text>().text = csv.Find_ID(_id).Portuguese;
           // print(c.Find_ID(_id).Portuguese);
        }
		else if (GameManager._Instance.Idioma.Equals("Spanish"))
		{
			GetComponent<Text>().text = csv.Find_ID(_id).Spanish;
			// print(c.Find_ID(_id).Portuguese);
		}
		/*else if (GameManager._Instance.Idioma.Equals("Chinese"))
		{
			GetComponent<Text>().text = c.Find_ID(_id).Chinese;
			// print(c.Find_ID(_id).Portuguese);
		}*/
        else
        {
            GetComponent<Text>().text = csv.Find_ID(_id).English;
            //print(c.Find_ID(_id).English);
        }


    }


}
