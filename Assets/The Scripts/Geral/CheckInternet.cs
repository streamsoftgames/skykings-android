﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckInternet : MonoBehaviour {
    
    public bool ChecarConexao()
    {
        bool isConect;

        if (Application.internetReachability == NetworkReachability.NotReachable)
            isConect = false;
        else
            isConect = true;

        return isConect;
    }
}
