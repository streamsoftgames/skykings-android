﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIManager : MonoBehaviour
{
    private PlayerManager _p;

    public Text distanciaInGame;
    public Text pontosInGame;
    public Text gameLevelInGame;
    public Text moneyInGame;

    /* Game Over Informações */
    public GameObject panelGameOver;
    public GameObject panelPrincipal;
    public GameObject barraBooster;
    public GameObject pausePanel;
	public GameObject botaoADS;


    public Image barFuel;
    public Image barBooster;

    public GameObject InvencibilityButton, FuelInfiniteButton, SpecialShootButton;

    public Text pontosFinais;
    public Text distanciaPercorrida;
    public Text maiorDistanciaPercorrida;
    public Text dinheiroGanho;
    public Text dinheiroTotalGanho;
    public Text medalhaTotal;
    public Text dificuldadeAlcancada;


    public Button soundButton;
    public Sprite[] soundImage;


    //public Text Debug;


    public float timeToBoster;
    public float timeToBoster_AUX;

  
    void Start()
    {

        _p = GameObject.Find("Player").GetComponent<PlayerManager>();
        panelPrincipal.SetActive(false);
    }


    void Update()
    {

        if (!GameManager._Instance.InGame)
        {
            panelPrincipal.SetActive(false);
        }
        else
        {
            panelPrincipal.SetActive(true);
        }

        float auxFuel = _p.FuelActual / _p.FuelMax;
        barFuel.fillAmount = auxFuel;

        //Informações gerais do Jogo
        distanciaInGame.text = 				GameManager._Instance.TravelledDistance.ToString("0000") + "mi";
        pontosInGame.text = 				GameManager._Instance.PontosGanhosnoJogo.ToString("0000");
        gameLevelInGame.text = 				GameManager._Instance.GameLevel.ToString("00");
        moneyInGame.text = 					GameManager._Instance.MoneyGame.ToString("0000");

        //Quantidade de Boosters
        InvencibilityButton.transform.GetChild(1).GetComponent<Text>().text = GameManager._Instance.QuantityInvencible.ToString();
        FuelInfiniteButton.transform.GetChild(1).GetComponent<Text>().text = GameManager._Instance.QuantityInfiniteFuel.ToString();
        SpecialShootButton.transform.GetChild(1).GetComponent<Text>().text = GameManager._Instance.QuantityEspecialShoot.ToString();


        if (panelGameOver.activeSelf == true) //Quando painel de Game Over aparece.
        {
            pontosFinais.text = 			GameManager._Instance.PontosGanhosnoJogo.ToString(); //Mostra total de pontos
            distanciaPercorrida.text = 		GameManager._Instance.TravelledDistance.ToString("00");
            maiorDistanciaPercorrida.text = GameManager._Instance.BestDistance.ToString("00");
            dinheiroGanho.text = 			GameManager._Instance.MoneyGame.ToString();
            dinheiroTotalGanho.text = 		GameManager._Instance.TotalMoneyGame.ToString();

            medalhaTotal.text = 			GameManager._Instance.MedalMoney.ToString();
            dificuldadeAlcancada.text = 	GameManager._Instance.GameLevel.ToString();
            //panelPrincipal.SetActive(false);
            botaoADS.SetActive (!GameManager._Instance.usedADS);
        }


        //Barra de gasolina
        if (barFuel.fillAmount >= 0.6f)
        {
            barFuel.color = Color.green;
        }
        else if (barFuel.fillAmount < 0.6f && barFuel.fillAmount > 0.35f)
        {
            barFuel.color = Color.yellow;
        }
        else if (barFuel.fillAmount <= 0.35f)
        {
            barFuel.color = Color.red;
        }



        /* Barra especial */

        if (GameManager._Instance.IsInfiniteFuel || GameManager._Instance.IsInvencible || GameManager._Instance.IsSpecialShoot)
        {
            float auxTime;

            timeToBoster = Time.time - timeToBoster_AUX;
            barraBooster.SetActive(true);

            if (GameManager._Instance.IsInfiniteFuel)
            {
                auxTime = GameManager._Instance.TimeFuelInfinite - timeToBoster;
                barBooster.fillAmount = auxTime / GameManager._Instance.TimeFuelInfinite;
            }
            if (GameManager._Instance.IsInvencible)
            {
                auxTime = GameManager._Instance.TimeInvencibility - timeToBoster;
                barBooster.fillAmount = auxTime / GameManager._Instance.TimeInvencibility;
            }
            if (GameManager._Instance.IsSpecialShoot)
            {
                auxTime = GameManager._Instance.TimeSpecialShoot - timeToBoster;
                barBooster.fillAmount = auxTime / GameManager._Instance.TimeSpecialShoot;
            }


            //print(barBooster.fillAmount);

        }
        else
        {
            barraBooster.SetActive(false);
            barBooster.fillAmount = 1;

            timeToBoster = 0;
            timeToBoster_AUX = Time.time;
        }



    }

    public void Pause()
    {
        if (Time.timeScale == 1 && pausePanel.activeSelf == false)
        {
            pausePanel.SetActive(true);

            GameManager._Instance.IsPaused = true;
            Time.timeScale = 0;

        }
        else
        {
            GameManager._Instance.IsPaused = false;
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
    }
    public void PauseReturnMenu()
    {
        Time.timeScale = 1;
        GameManager._Instance.IsPaused = false;
        pausePanel.SetActive(false);
        SceneManager.LoadScene("2 - Menu", LoadSceneMode.Single);

    }

    public void SoundControl()
    {
        if (AudioListener.pause == true) //PAUSAR
        {
            soundButton.transform.GetChild(0).GetComponent<Image>().sprite = soundImage[0];
            AudioListener.pause = false;
        }
        else //DESPAUSAR 
        {
            soundButton.transform.GetChild(0).GetComponent<Image>().sprite = soundImage[1];
            AudioListener.pause = true;
        }
    }
}
