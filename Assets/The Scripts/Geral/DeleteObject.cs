﻿using UnityEngine;
using System.Collections;

public class DeleteObject : MonoBehaviour {

	public GameObject pointDelet;
	
	void Start()
	{
		pointDelet = GameObject.Find ("Ponto Destruir Objetos");	
	}
	void Update () {

		if (transform.position.y <= pointDelet.transform.position.y) {
		
			Destroy (gameObject);
		}

	}
}
