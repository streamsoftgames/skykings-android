﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour {

    public string aux;

    public void DestroyObject()
    {
        Destroy(gameObject);
       

        if (aux.Equals("Tank"))
        {
            AnalitycsManager.Instance.TotalTanquesDestruidos++;
            AnalitycsManager.Instance.TotalInimigosDestruidos++;
        }
        else if (aux.Equals("Air"))
        {
            AnalitycsManager.Instance.TotalAereosDestruidos++;
            AnalitycsManager.Instance.TotalInimigosDestruidos++;
        }
        else if (aux.Equals("Water"))
        {
            AnalitycsManager.Instance.TotalAquaticosDestruidos++;
            AnalitycsManager.Instance.TotalInimigosDestruidos++;
        }
        
        
    }

	public void DestroyGallon()
	{
		Destroy(gameObject);
	}
}
