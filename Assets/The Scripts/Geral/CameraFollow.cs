﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public PlayerManager player;

	private Vector3 ultimaPosicaoPlayer;
	private float distanciaParaMover;

	// Use this for initialization
	void Start()
	{
		player = FindObjectOfType<PlayerManager>();
		ultimaPosicaoPlayer = player.transform.position;
	}

	// Update is called once per frame
	void Update()
	{
   
        if (!GameManager._Instance.IsIntro && GameManager._Instance.InGame)
        {
            distanciaParaMover = player.transform.position.y - ultimaPosicaoPlayer.y;


            transform.position = new Vector3(transform.position.x, transform.position.y + distanciaParaMover, transform.position.z);
            ultimaPosicaoPlayer = player.transform.position;
        }
        else
        {
            ultimaPosicaoPlayer = player.transform.position;
        }


	}
}
