﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InformationManager : MonoBehaviour {

	public Text totalMorte;
	
	public Text TotalDestruidos;
	public Text totalAereosDestruidos;
	public Text totalAquaticosDestruidos;
	public Text totalTerrestreDestruidos;

    public Text totalDinheiroGanho;
    public Text totalDinheiroGasto;

    public Text totalMedalhasGanhas;
    public Text totalMedalhasGastas;

    public Text totalDistancia;
    public Text maiorDistancia;

    public Text maiorDificuldade;

    public Text maiorPontuação;
    public Text totalPontuação;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		totalMorte.text = AnalitycsManager.Instance.TotalMortes.ToString();
		
		TotalDestruidos.text = AnalitycsManager.Instance.TotalInimigosDestruidos.ToString();
		totalAereosDestruidos.text =  AnalitycsManager.Instance.TotalAereosDestruidos.ToString();
		totalAquaticosDestruidos.text =  AnalitycsManager.Instance.TotalAquaticosDestruidos.ToString();
		totalTerrestreDestruidos.text =  AnalitycsManager.Instance.TotalTanquesDestruidos.ToString();

        totalDinheiroGanho.text = AnalitycsManager.Instance.TotalDinheiroGanho.ToString();
        totalDinheiroGasto.text = AnalitycsManager.Instance.TotalDinheiroGasto.ToString();

        totalMedalhasGanhas.text = AnalitycsManager.Instance.TotalMedalhaGanho.ToString();
        totalMedalhasGastas.text = AnalitycsManager.Instance.TotalMedalhaGasto.ToString();

        maiorDistancia.text = GameManager._Instance.BestDistance.ToString("000");
		totalDistancia.text = AnalitycsManager.Instance.TotalDistanciaViajada.ToString("000");

        maiorDificuldade.text = GameManager._Instance.BestGameLevel.ToString();

		maiorPontuação.text = GameManager._Instance.BestPontuation.ToString();
        totalPontuação.text = GameManager._Instance.TotalPontosGeral.ToString();

	}
}

