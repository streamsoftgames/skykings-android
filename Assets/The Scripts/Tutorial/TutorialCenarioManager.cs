﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class TutorialCenarioManager : MonoBehaviour {

	public float estagioTutorial;

	public GameObject tutorialPanel;
	public Text titulo;
	public Text descricao;

	public bool isOK = false;
	public string botaoAvancar;

	public PlayerManager _p;

	public GameObject[] gatilhos;
	public GameObject inimigoSpawn;
	public GameObject[] setas;
	public GameObject[] itens;


	public GameObject joystick;
	public GameObject firebutton;

	public GameObject boosterInvencibilidade;
	public GameObject boosterGasolina;
	public GameObject boosterTiroEspecial;

	public GameObject hudCombustivel;


	public GameObject[] hudDificuldade;
	public GameObject[] hudpontos;
	public GameObject[] hudDistancia;
	public GameObject[] hudMoedas;

	// Use this for initialization
	void Start () 
	{

		GooglePlayServices.Instance.UnlockAchievements (SkykingsGP.achievement_make_tutorial);

		joystick.SetActive (false);
		firebutton.SetActive (false);
		hudCombustivel.SetActive (false);

		boosterInvencibilidade.SetActive (false);
		boosterGasolina.SetActive (false);
		boosterTiroEspecial.SetActive (false);

		hudDificuldade[0].SetActive (false);
		hudDificuldade[1].SetActive (false);

		hudpontos[0].SetActive (false);
		hudpontos[1].SetActive (false);

		hudDistancia[0].SetActive (false);
		hudDistancia[1].SetActive (false);

		hudMoedas[0].SetActive (false);
		hudMoedas[1].SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
	
		if (isOK) 
		{
			switch (estagioTutorial.ToString ())
			{
			case "1":
				joystick.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_movimento_1_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_movimento_1_descricao";
				tutorialPanel.SetActive (true);
				setas [0].SetActive (true);
				setas [1].SetActive (false);
				botaoAvancar = "normal";
				break;

			case "1.1":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_movimento_1_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_movimento_2_descricao";
				tutorialPanel.SetActive (true);
				setas [0].SetActive (false);
				setas [1].SetActive (true);
				botaoAvancar = "normal";
				break;

			case "1.2":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_movimento_1_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_movimento_3_descricao";
				tutorialPanel.SetActive (true);
				gatilhos [0].SetActive (true); //Ativa gatilho para continuar
				setas [0].SetActive (false);
				setas [1].SetActive (false);
				botaoAvancar = "normal";
				break;

			case "1.3":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_movimento_1_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_movimento_4_descricao";
				tutorialPanel.SetActive (true);
				Time.timeScale = 0;
				botaoAvancar = "normal";
				break;

			case "2":
				firebutton.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_tiro_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_tiro_1_descricao";
				tutorialPanel.SetActive (true);
				inimigoSpawn.SetActive (true);
				botaoAvancar = "normal";
				break;

			case "3":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_hud_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_hud_1_descricao";
				tutorialPanel.SetActive (true);
				Time.timeScale = 0;
				botaoAvancar = "diferente";

				break;

			case "3.1":
				hudDificuldade [0].SetActive (true);
				hudDificuldade [1].SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_hud_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_hud_dificuldade_descricao";
				botaoAvancar = "diferente";
				break;

			case "3.2":
				hudDistancia [0].SetActive (true);
				hudDistancia [1].SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_hud_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_hud_distancia_descricao";
				botaoAvancar = "diferente";
				break;

			case "3.3":
				hudpontos [0].SetActive (true);
				hudpontos [1].SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_hud_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_hud_pontos_descricao";
				botaoAvancar = "diferente";
				break;

			case "3.4":
				hudMoedas [0].SetActive (true);
				hudMoedas [1].SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_hud_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_hud_moedas_descricao";
				botaoAvancar = "normal";

				break;

			case "4":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_item_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_item_1_descricao";
				Time.timeScale = 0;
				tutorialPanel.SetActive (true);
				itens [0].SetActive (true);
				itens [1].SetActive (true);
				itens [2].SetActive (true);
				botaoAvancar = "diferente";
				break;

			case "4.1":
				hudCombustivel.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_item_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_item_galao";
				botaoAvancar = "diferente";
				break;

			case "4.2":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_item_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_item_moeda";
				botaoAvancar = "diferente";
				break;

			case "4.3":
				titulo.GetComponent<TranslateText> ()._id = "tutorial_item_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_item_medalha";
				//estagioTutorial = 5;
				botaoAvancar = "normal";
				break;
			
			case "5":
				Time.timeScale = 0;
				tutorialPanel.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_booster_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_booster_descricao";
				//estagioTutorial = 5;
				botaoAvancar = "diferente";
				break;

			case "5.1":
				boosterInvencibilidade.SetActive (true);

				titulo.GetComponent<TranslateText> ()._id = "tutorial_booster_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_booster_invencibilidade";
				//estagioTutorial = 5;
				botaoAvancar = "diferente";
				break;

			case "5.2":

				boosterGasolina.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_booster_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_booster_gasolina";
				//estagioTutorial = 5;
				botaoAvancar = "diferente";
				break;

			case "5.3":
				
				boosterTiroEspecial.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_booster_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_booster_tiro";
				//estagioTutorial = 5;
				botaoAvancar = "normal";
				break;

			case "6":
				Time.timeScale = 0;
				tutorialPanel.SetActive (true);
				boosterTiroEspecial.SetActive (true);
				titulo.GetComponent<TranslateText> ()._id = "tutorial_fim_titulo";
				descricao.GetComponent<TranslateText> ()._id = "tutorial_fim_descricao";
				//estagioTutorial = 5;
				botaoAvancar = "fim";
				break;

			}
		}
	}


	public void TutorialOK()
	{
		
		switch (botaoAvancar) 
		{
		case "normal":
			tutorialPanel.SetActive (false);
			isOK = false;
			descricao.text = "";
			titulo.text = "";

			Time.timeScale = 1;
			break;

		case "diferente":
			descricao.text = "";
			titulo.text = "";

			estagioTutorial += .1f;
			break;

		case "fim":
			GameManager._Instance.IsTutorial = false;
			SceneManager.LoadScene ("00 - Loading", LoadSceneMode.Single);
			GameManager._Instance._player = null;
			break;


		}


	}


}
