﻿using UnityEngine;
using System.Collections;

public class IdiomaSelect : MonoBehaviour {


    public GameObject idiomaPainel;
    public GameObject optionPainel;


    public void Idioma(string idioma)
    {
        GameManager._Instance.Idioma = idioma;
        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
        AbrirPainel();
    }

    public void AbrirPainel()
    {
        bool aux = !idiomaPainel.activeSelf;
        optionPainel.SetActive(!aux);
        idiomaPainel.SetActive(aux);
    }

}
