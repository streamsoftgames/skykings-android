﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using Facebook.MiniJSON;


public class FacebookInicializar : MonoBehaviour
{
    public static FacebookInicializar instance { get; private set; }

    public string get_data;

    public string fb_name;
    public string fb_email;
    public int fb_id;

    [SerializeField]
    private bool fb_isLogged;



    // Awake function from Unity's MonoBehavior
    void Awake()
    {
        if (instance == null)
            //   instance = this;

            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }



        if (!ZPlayerPrefs.HasKey("fb_login"))
        {
            fb_isLogged = false;
            ZPlayerPrefs.SetString("fb_login", fb_isLogged.ToString());
        }
        else
        {
            fb_isLogged = bool.Parse(ZPlayerPrefs.GetString("fb_login"));
        }
    }

    void Start()
    {

    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...

            if (fb_isLogged && FB.IsInitialized && !FindObjectOfType<MenuGame>().PrimeiraVez_FB)
                Login();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void Login()
    {
        var perms = new List<string>() { "public_profile", "user_friends", "email" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }

            if (!fb_isLogged)
            {
                GameManager._Instance.MoneyGame += 500;
                fb_isLogged = true;
                ZPlayerPrefs.SetString("fb_login", fb_isLogged.ToString());
            }
            ChecarLogin(FB.IsLoggedIn);
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    void ChecarLogin(bool isLogin)
    {
        if (isLogin)
        {
            Debug.Log("FACEBOOK LOGADO");
            FB.API("me?fields=name,id,email,locale", HttpMethod.GET, UserCallBack);
        }
        else
        {
            Debug.Log("FACEBOOK NÂO LOGADO");
        }
    }

    void UserCallBack(IResult result)
    {
        if (result.Error != null)
            print("Error Response:\n" + result.Error);

        else
        {
            //IDictionary dict = Json.Deserialize(result.ResultDictionary["name"].ToString()) as IDictionary;
            GameManager._Instance.Fb_nome = result.ResultDictionary["name"].ToString();
            GameManager._Instance.Fb_email = result.ResultDictionary["email"].ToString();
            GameManager._Instance.Fb_id = result.ResultDictionary["id"].ToString();

            print(result.RawResult);

            if (GameManager._Instance.temConexao)
                WebService.instance.RegisterUser();
            //WebService.instance.SetScore(1350, GameManager._Instance.WebserviceID);
            //WebService.instance.GetRecord();

        }
    }

    
}


