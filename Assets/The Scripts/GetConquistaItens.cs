﻿using UnityEngine;
using System.Collections;

public class GetConquistaItens : MonoBehaviour {

    public GameObject[] itens;

	void Start () {

        itens = new GameObject[GameObject.Find("Grid Conquista").transform.childCount];

        for (int i = 0; i < itens.Length; i++)
        {
            itens[i] = GameObject.Find("Grid Conquista").transform.GetChild(i).gameObject;
        }

	}
	
}
