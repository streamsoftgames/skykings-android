﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuGame : MonoBehaviour
{

    public GameObject optionPanel;
    public GameObject menuPanel;
    public GameObject loadCloudPanel;
    public GameObject infoPanel;
    public GameObject clearPanel;
    public GameObject buttonSim;
    public GameObject noticiasPanel;

    [SerializeField]
    private bool primeiraVez_FB = true;

    public UnityEngine.UI.Text menuFBSaudacao;

    public bool PrimeiraVez_FB
    {
        get
        {
            return primeiraVez_FB;
        }

        set
        {
            primeiraVez_FB = value;
        }
    }

    void Awake()
    {
        if (!ZPlayerPrefs.HasKey("fbPrimeiraVez"))
        {
            ZPlayerPrefs.SetString("fbPrimeiraVez", PrimeiraVez_FB.ToString());
            print("nao tem FBPRIMEIRAVEZ");
        }
        else
        {
            PrimeiraVez_FB = bool.Parse(ZPlayerPrefs.GetString("fbPrimeiraVez"));
            print(" tem FBPRIMEIRAVEZ");
        }
    }

    void Start()
    {

        if (SceneManager.GetActiveScene().name.Equals("2 - Menu"))
        {
            if (!GameManager._Instance.LoadInicial_ok)
            {
                if (GameManager._Instance.Primeira_vez)
                {
                    loadCloudPanel.SetActive(true);
                    menuPanel.SetActive(false);

                    GameManager._Instance.tutorialOK = true;

                    Debug.LogWarning("PRIMEIRA VEZ");
                }
                else
                {
                    GameManager._Instance.LoadLocal();
                    Debug.LogWarning("NAO É a PRIMEIRA VEZ");
                }

                GameManager._Instance.LoadInicial_ok = true;
            }

            if (Application.systemLanguage.Equals("Portuguese")) 
                noticiasPanel.SetActive(true);
            else
                noticiasPanel.SetActive(false);

        }



    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            PrimeiraVez_FB = true;
            ZPlayerPrefs.SetString("fbPrimeiraVez", PrimeiraVez_FB.ToString());
            print(PrimeiraVez_FB);
        }

        if (SceneManager.GetActiveScene().name.Equals("2 - Menu"))
        {
            if (loadCloudPanel.activeSelf)
            {
                if (GooglePlayServices.Instance.IsAuthenticated)
                {
                    buttonSim.SetActive(true);
                }
                else
                    buttonSim.SetActive(false);
            }
            menuFBSaudacao.text = "Hi " + GameManager._Instance.Fb_nome + "!";
        }
    }

    public void SimLoad()
    {
        GooglePlayServices.Instance.LoadFromCloud();
        //GameManager._Instance.LOADSCENE("2 - Menu");
        GameManager._Instance.Primeira_vez = false;
        loadCloudPanel.SetActive(false);
        menuPanel.SetActive(true);
    }
    public void NaoLoad()
    {
        //GameManager._Instance.LOADSCENE("2 - Menu");
        GameManager._Instance.Primeira_vez = false;
        loadCloudPanel.SetActive(false);
        menuPanel.SetActive(true);
    }


    public void ClearData()
    {
        if (SceneManager.GetActiveScene().name.Equals("2 - Menu"))
        {
            clearPanel.SetActive(true);
        }
    }
    public void ClearData_SIM()
    {
        ZPlayerPrefs.DeleteAll();
        GameManager._Instance.LoadInicial_ok = false;

        clearPanel.SetActive(false);

        GameManager._Instance.LOADSCENE("0 - Inicializar");


    }
    public void ClearData_NAO()
    {
        clearPanel.SetActive(false);
    }


    public void InfoPainel()
    {
        bool aux = !infoPanel.activeSelf;
        infoPanel.SetActive(aux);
    }

    public void NoticiaPanel()
    {
        bool aux = !noticiasPanel.activeSelf;
        noticiasPanel.SetActive(aux);

        PrimeiraVez_FB = false;
        ZPlayerPrefs.SetString("fbPrimeiraVez", PrimeiraVez_FB.ToString());
        print(PrimeiraVez_FB);
    }

    public void Option()
    {
        if (optionPanel.activeSelf)
        {
            optionPanel.SetActive(false);
            clearPanel.SetActive(false);
            menuPanel.SetActive(true);
        }
        else
        {
            optionPanel.SetActive(true);
            menuPanel.SetActive(false);
        }
    }

    public void GoScene(string scene)
    {
        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
        SceneManager.LoadScene(scene, LoadSceneMode.Single);

    }
    /**/

    public void BuyDoubleMedal() { IAPManager.Instance.BuyDoubleMedal(); }
    public void BuyDoubleCoin() { IAPManager.Instance.BuyDoubleCoin(); }

    public void BuyPacoteMoeda1() { IAPManager.Instance.BuyCoin1(); }
    public void BuyPacoteMoeda2() { IAPManager.Instance.BuyCoin2(); }
    public void BuyPacoteMoeda3() { IAPManager.Instance.BuyCoin3(); }
    public void BuyPacoteMedalha1() { IAPManager.Instance.BuyMedal1(); }
    public void BuyPacoteMedalha2() { IAPManager.Instance.BuyMedal2(); }
    public void BuyPacoteMaster() { IAPManager.Instance.BuyMaster(); }
    public void BuyPS4() { IAPManager.Instance.BuyPS4(); }

    /* CLOUD TESTE */

    public void SaveCloud() { GooglePlayServices.Instance.SaveToCloud(); }
    public void LoadCloud() { GooglePlayServices.Instance.LoadFromCloud(); }
    public void LoginGP() { GooglePlayServices.Instance.Login("Login"); }
    public void ShowSaves() { GooglePlayServices.Instance.showUI(); }
    public void ShowConquistas() { GooglePlayServices.Instance.ShowConquistas(); }
    public void ShowPlacar() { GooglePlayServices.Instance.ShowNormalScore(); }


    public void PropagandaNormal()
    {
        AdsManager.Instance.ShowAdNormal();
    }
    public void PropagandaComTesouro()
    {
        AdsManager.Instance.ShowAdsRecompensa();
    }
}
