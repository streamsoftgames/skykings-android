﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Radio : MonoBehaviour
{
    public Canvas canvas;

    public static Radio _instance { get; private set; }
    public AudioSource _musicas;

    public AudioClip[] musics;
    public List<int> sequencia = new List<int>();

    private Animator _anim;

    public GameObject radioPainel;

    public Text displayInfo;
    public Text openInformation;

    public Button previous;
    public Button next;

    public Button pause;
    public Button play;
    public Button stop;

    private bool b = false;

    private int contPlayer = 0;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            print("Instancia Radio criada");
        }
        else
        {
            Destroy(gameObject);
            print("Radio Instancia destruida");
        }
        DontDestroyOnLoad(gameObject);

        _musicas = GetComponent<AudioSource>();
        _anim = transform.GetChild(0).transform.GetChild(0).GetComponent<Animator>();

        CriarListadeMusica(0);
    }

    // Use this for initialization
    void Start()
    {
        b = false;
        Anima();
        _musicas.clip = musics[sequencia[contPlayer]];
        _musicas.Play();

    }

    // Update is called once per frame
    void Update()
    {
        string timeTotal = string.Format("{0:0}:{1:00}", Mathf.Floor(_musicas.clip.length / 60), _musicas.clip.length % 60);
        string timeCurrent = string.Format("{0:0}:{1:00}", Mathf.Floor(_musicas.time / 60), _musicas.time % 60);

        displayInfo.text = _musicas.clip.name + " - " + timeCurrent + "/" + timeTotal;

       // canvas.worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		//Debug.Log (_musicas.clip.loadState);


		if (_musicas.time >= _musicas.clip.length - 0.5f) 
		{
			NextMusic ();
			print ("PROXIMA MUSICA");
		}

      
    }

    public void CriarListadeMusica(int a)
    {
        for (int i = a; i < musics.Length; i++)
        {
            //Debug.Log(i);
            int aux = Random.Range(0, musics.Length);

            //Debug.Log(aux);
            if (sequencia.Contains(aux))
            {
                CriarListadeMusica(i);
                break;
            }
            else
            {
                sequencia.Add(aux);
            }
        }

        
    }

    public void NextMusic()
    {
        if (contPlayer < musics.Length-1)
        {
            contPlayer++;
            _musicas.clip = musics[sequencia[contPlayer]];
            _musicas.Play();
        }
		else if(contPlayer >= musics.Length-1)
		{
			contPlayer = 0;
			_musicas.clip = musics[sequencia[contPlayer]];
			_musicas.Play();

		}
    }
    public void PreviousMusic()
    {
        if (contPlayer > 0)
        {
            contPlayer--;
            _musicas.clip = musics[sequencia[contPlayer]];
            _musicas.Play();
        }
    }
    public void PlayOrPause()
    {
        if (_musicas.isPlaying)
        {
            _musicas.Pause();
        }
        else
        {
            _musicas.UnPause();
        }

    }

    public void Anima()
    {
        
        print("1 " + b);

        _anim.SetTrigger("Apertado");
        _anim.SetBool("Pos", b);

        b = !b;
    }

}
