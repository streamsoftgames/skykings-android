﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingInicial : MonoBehaviour
{

    private bool loadScene = false;
    AsyncOperation async;

    [SerializeField]
    private Text loadingText;


    void Start()
    {
        GameManager._Instance.Inicilizar();

        if (ZPlayerPrefs.HasKey("DobroOuTudo"))
        {
            GameManager._Instance.IsDoubleMedal = bool.Parse(ZPlayerPrefs.GetString("DobroOuTudo"));
            Debug.Log("Medalha: " + GameManager._Instance.IsDoubleMedal);
        }
        else
        {
            ZPlayerPrefs.SetString("DobroOuTudo", GameManager._Instance.IsDoubleMedal.ToString());
            Debug.Log("Medalha 2: " + GameManager._Instance.IsDoubleMedal);
        }


        if (ZPlayerPrefs.HasKey("DobroOuNada"))
        {
            GameManager._Instance.IsDoubleMoney = bool.Parse(ZPlayerPrefs.GetString("DobroOuNada"));
            Debug.Log("Moeda: " + GameManager._Instance.IsDoubleMoney);
        }
        else
        {
            ZPlayerPrefs.SetString("DobroOuNada", GameManager._Instance.IsDoubleMoney.ToString());
            Debug.Log("Moeda 2: " + GameManager._Instance.IsDoubleMoney);
        }

        if (ZPlayerPrefs.HasKey("ps4Campeonato"))
        {
            GameManager._Instance.Ps4Participante = bool.Parse(ZPlayerPrefs.GetString("ps4Campeonato"));
            Debug.Log("ps4  1: " + GameManager._Instance.Ps4Participante);
        }
        else
        {
            ZPlayerPrefs.SetString("ps4Campeonato", GameManager._Instance.Ps4Participante.ToString());
            Debug.Log("ps4  2: " + GameManager._Instance.Ps4Participante);
        }

        GameManager._Instance.Idioma = Application.systemLanguage.ToString();

    }


    void Update()
    {
        if (loadScene == false)
        {
            loadScene = true;
            StartCoroutine(LoadNewScene());
        }

        if (loadScene == true)
        {
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
        }

    }

    IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(2);

        async = SceneManager.LoadSceneAsync("2 - Menu", LoadSceneMode.Single);

        while (!async.isDone)
        {
            yield return null;
        }

    }

}
