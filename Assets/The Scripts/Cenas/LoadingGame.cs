﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingGame : MonoBehaviour {

    private bool loadScene = false;

	public string[] idTips;

    [SerializeField]    private string scene;
    [SerializeField]    private Text loadingText;
    [SerializeField]    private Text tipsText;
    public Text pressText;

    


    public TranslateText textoTranslate;

	void Awake	()
	{
		textoTranslate = tipsText.GetComponent<TranslateText> ();	
	}

	void Start()
    {

		textoTranslate._id = idTips[Random.Range(0, idTips.Length)];

        
    }

    void Update()
    {
        if (loadScene == false)
        { 
            loadScene = true;
            StartCoroutine(LoadNewScene());
        }
       
        if (loadScene == true)
        {

            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));

        }

    }



    IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(3);
       // SceneManager.LoadScene(scene, LoadSceneMode.Single);

        AsyncOperation async = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);

        while (!async.isDone)
        {
            yield return null;
        }

    }

}
