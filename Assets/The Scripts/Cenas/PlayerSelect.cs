﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using LitJson;


public class PlayerSelect : MonoBehaviour
{

    public GameObject UpgradePanel;
    public GameObject BuyPanel;
	public GameObject TutorialPanel;

    private int IDSelect = 0;

    public Sprite sprite;
    public NavesManager naveData;

    public Text VERSION;

    public Image naveSlug;
    public Text naveName;
    public Text naveValor;
    public Text dinheiroJogo;
    public Text medalhaJogo;
    public Text dinheiroUpgrade;
    public Text medalhaUpgrade;


    /* Infos Comuns */
    public Text debug;
    
    
    public Text naveStatusMagnetude;
    public Text naveStatusPoder;

    public Image barVelocidade;
    public Image barMovimento;
    public Image barTanque;


    /* Infos Loja */
    public Text upgradeValorVelocidade;
    public Text upgradeValorMovimento;
    public Text upgradeValorTanque;
    public Text upgradeValorMagnetude;
    public Text upgradeValorPoder;

    public Text infoVelocidade;
    public Text infoMovimento;
    public Text infoTanque;
    public Text infoMagnetude;
    public Text infoPoder;

    public Text naveStatusValor;
    public Image iconNave;

    private JsonData JW;
    private Naves aux_n;

    
    


    void Awake()
    {
        naveData = NavesManager.Instance; //GameObject.Find("Game Manager").GetComponent<NavesManager>();
    }
    void Start()
    {
        VERSION.text = "Versão: " + Application.version;
        SelectPlayer("Begin");
    }

    void Update()
    {
        dinheiroJogo.text =  GameManager._Instance.TotalMoneyGame.ToString();
        medalhaJogo.text =  GameManager._Instance.MedalMoney.ToString();

        if (Input.GetKeyDown(KeyCode.R))
            SelectPlayer("Reset");

        if(UpgradePanel.activeSelf == true)
        {
            upgradeValorVelocidade.text = 	GameManager._Instance.valorVelocidade[IDSelect].ToString();
            upgradeValorMovimento.text = 	GameManager._Instance.valorMovimento[IDSelect].ToString();
            upgradeValorTanque.text = 		GameManager._Instance.valorTanque[IDSelect].ToString();
            upgradeValorMagnetude.text = 	GameManager._Instance.valorMagnetude[IDSelect].ToString();
            upgradeValorPoder.text = 		GameManager._Instance.valorPoder[IDSelect].ToString();

            infoVelocidade.text  = 		(aux_n.VelocidadeInicial 	+ 	GameManager._Instance.upgradeVelocidade[IDSelect]).	ToString();
            infoMovimento.text  = 		(aux_n.MovimentoInicial 	+	GameManager._Instance.upgradeMovimento[IDSelect]).	ToString();
            infoTanque.text = 			(aux_n.TanqueTotalInicial 	+ 	GameManager._Instance.upgradeTanque[IDSelect]).		ToString();
            infoMagnetude.text  = 		(aux_n.MagnetudeInicial 	+ 	GameManager._Instance.upgradeMagnetude[IDSelect]).	ToString();
            infoPoder.text  = 			(aux_n.PoderInicial 		+ 	GameManager._Instance.upgradePoder[IDSelect]).		ToString();

            dinheiroUpgrade.text = GameManager._Instance.TotalMoneyGame.ToString();
            medalhaUpgrade.text = GameManager._Instance.MedalMoney.ToString();

        }
    }

    public void SelectPlayer(string move)
    {
        if (move.Equals("Next"))
        {
            if (IDSelect < naveData.databaseShips.Count - 1)
                IDSelect++;
            else
                IDSelect = 0;
        }
        else if (move.Equals("Previous"))
        {
            if (IDSelect > 0)
                IDSelect--;
            else
                IDSelect = naveData.databaseShips.Count - 1;
        }
        else if (move.Equals("Begin"))
        {
            IDSelect = 0;
        }
        else if (move.Equals("Refresh"))
        {
            //GameManager._Instance.LoadLocal();
        }

        else if (move.Equals("Reset"))
        {
            ZPlayerPrefs.DeleteAll();
            Debug.LogError("RESETADO");
            return;
        }



        Naves _n = naveData.GetItem(IDSelect);

        //Load informações
        aux_n = _n;

        GameManager._Instance.SelectedShip_ID = IDSelect;

        if (_n.BUY == 1)
            GameManager._Instance.naveComprada[IDSelect] = _n.BUY;

        
        naveName.text = 			_n.Name;
        naveStatusValor.text = 		_n.Value.ToString();

        /*
        infoVelocidade.text = naveStatusVelocidade.text = "Velocidade: ";// + (_n.VelocidadeInicial + GameManager._Instance.upgradeVelocidade[IDSelect]);
        infoMovimento.text = naveStatusMovimento.text = "Movimento: "; // + (_n.MovimentoInicial + GameManager._Instance.upgradeMovimento[IDSelect]);
        infoTanque.text = naveStatusTanque.text = "Tanque: "; //+ (_n.TanqueTotalInicial + GameManager._Instance.upgradeTanque[IDSelect]);*/
        infoMagnetude.text = 	naveStatusMagnetude.text = 		(_n.MagnetudeInicial + GameManager._Instance.upgradeMagnetude[IDSelect]).ToString();
        infoPoder.text = 		naveStatusPoder.text = 			(_n.PoderInicial + GameManager._Instance.upgradePoder[IDSelect]).ToString();
        iconNave.sprite = 		_n.SpriteIcon;
        iconNave.preserveAspect = true;

        float auxVelocidade =	_n.VelocidadeInicial    +    GameManager._Instance.upgradeVelocidade[IDSelect];
        float auxMovimento =	_n.MovimentoInicial     +    GameManager._Instance.upgradeMovimento[IDSelect];
        float auxTanque = 		_n.TanqueTotalInicial   +    GameManager._Instance.upgradeTanque[IDSelect];
        float auxMagnetude = 	_n.MagnetudeInicial     +    GameManager._Instance.upgradeMagnetude[IDSelect];
        float auxPoder = 		_n.PoderInicial         +    GameManager._Instance.upgradePoder[IDSelect];

        barVelocidade.fillAmount = 	(float)(auxVelocidade / 240);
        barMovimento.fillAmount = 	(float)(auxMovimento / 200);
        barTanque.fillAmount = 		(float)(auxTanque / 240);

        if (GameManager._Instance.naveComprada[IDSelect] == 0)
        {
            BuyPanel.SetActive(true);
            UpgradePanel.SetActive(false);
        }
        else
            BuyPanel.SetActive(false);


    }

    public void GoGame()
    {
        //GameManager._Instance.SaveLocal(); //SALVANDO JOGO
        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
        SceneManager.LoadScene("00 - Loading" , LoadSceneMode.Single);
    }

    public void PanelUpgrade()
    {
        if (UpgradePanel.activeSelf == false)
        {
            UpgradePanel.SetActive(true);
        }

        else
        {
            //GameManager._Instance.SaveLocal();  //GAME SALVO
            UpgradePanel.SetActive(false);
            GooglePlayServices.Instance.SaveToCloud();
            GameManager._Instance.SaveLocal();
            SelectPlayer("Refresh");
        }
    }

    public void UpgradeSystem(string type)
    {
        if (type.Equals("velocidade"))
        {
         
            if (GameManager._Instance.TotalMoneyGame >= GameManager._Instance.valorVelocidade[IDSelect] && GameManager._Instance.upgradeVelocidade[IDSelect] < aux_n.VelocidadeInicial)
            {
                Debug.Log("Comprado " + type);
                GameManager._Instance.TotalMoneyGame -= 				GameManager._Instance.valorVelocidade[IDSelect];
                GameManager._Instance.valorVelocidade[IDSelect] = 		(int)(GameManager._Instance.valorVelocidade[IDSelect] * 1.8f);
                GameManager._Instance.upgradeVelocidade[IDSelect] += 	(int)(aux_n.VelocidadeInicial * 0.1f);

                AnalitycsManager.Instance.TotalDinheiroGasto += 		GameManager._Instance.valorVelocidade[IDSelect];
            }
        }
        else if (type.Equals("movimento"))
        {
            if (GameManager._Instance.TotalMoneyGame >= GameManager._Instance.valorMovimento[IDSelect] && GameManager._Instance.upgradeMovimento[IDSelect] < (aux_n.MovimentoInicial))
            {
                Debug.Log("Comprado " + type);
                GameManager._Instance.TotalMoneyGame -= 				GameManager._Instance.valorMovimento[IDSelect];
                GameManager._Instance.valorMovimento[IDSelect] = 		(int)(GameManager._Instance.valorMovimento[IDSelect] * 1.8f);
                GameManager._Instance.upgradeMovimento[IDSelect] += 	(int)(aux_n.MovimentoInicial * 0.1f);

                AnalitycsManager.Instance.TotalDinheiroGasto += 		GameManager._Instance.valorMovimento[IDSelect];
            }
        }
        else if (type.Equals("tanque"))
        {
            if (GameManager._Instance.TotalMoneyGame >= GameManager._Instance.valorTanque[IDSelect] && (GameManager._Instance.upgradeTanque[IDSelect]) < (aux_n.TanqueTotalInicial))
            {
                Debug.Log("Comprado " + type);
                GameManager._Instance.TotalMoneyGame -= 			GameManager._Instance.valorTanque[IDSelect];
                GameManager._Instance.valorTanque[IDSelect] = 		(int)(GameManager._Instance.valorTanque[IDSelect] * 1.8f);
                GameManager._Instance.upgradeTanque[IDSelect] += 	(int)(aux_n.TanqueTotalInicial * 0.1f);

                AnalitycsManager.Instance.TotalDinheiroGasto += 	GameManager._Instance.valorTanque[IDSelect];
            }
        }
        else if (type.Equals("magnetude"))
        {
            if (GameManager._Instance.TotalMoneyGame >= GameManager._Instance.valorMagnetude[IDSelect] /*&& (GameManager._Instance.upgradeMagnetude[IDSelect]) < (aux_n.MagnetudeInicial)*/)
            {
                Debug.Log("Comprado " + type);

				GameManager._Instance.upgradeMagnetude[IDSelect] += 	(int)(aux_n.MagnetudeInicial * 0.1f);
				GameManager._Instance.TotalMoneyGame -= 				GameManager._Instance.valorMagnetude[IDSelect];
				AnalitycsManager.Instance.TotalDinheiroGasto += 		GameManager._Instance.valorMagnetude[IDSelect];

				if(GameManager._Instance.upgradePoder[IDSelect] < 9)
				{
					GameManager._Instance.valorMagnetude[IDSelect] =	(int)(GameManager._Instance.valorMagnetude[IDSelect] * 1.8f);
				}
				else
				{
					GameManager._Instance.valorMagnetude[IDSelect] = 	(int)(GameManager._Instance.valorMagnetude[IDSelect] * 1.5f);
				}
            }
        }
        else if (type.Equals("poder"))
        {
            if (GameManager._Instance.TotalMoneyGame >= GameManager._Instance.valorPoder[IDSelect])
            {
                Debug.Log("Comprado " + type);

                GameManager._Instance.TotalMoneyGame -= 			GameManager._Instance.valorPoder[IDSelect];
				AnalitycsManager.Instance.TotalDinheiroGasto += 	GameManager._Instance.valorPoder[IDSelect];

                if (GameManager._Instance.upgradePoder[IDSelect] < 9)
                {
                    GameManager._Instance.valorPoder[IDSelect] = 	(int)(GameManager._Instance.valorPoder[IDSelect] * 1.8f);
                    GameManager._Instance.upgradePoder[IDSelect]++;
                }
                else
                {
                    GameManager._Instance.valorPoder[IDSelect] = 	(int)(GameManager._Instance.valorPoder[IDSelect] * 1.5f);
                    GameManager._Instance.upgradePoder[IDSelect] += 5;
                }
            }
        }


    }
    public void ComprarNave()
    {
       // print("Tentando comprar nave" +  IDSelect);
        if (GameManager._Instance.naveComprada[IDSelect] == 0)
        {
           // print("O ID É 0");
            if (GameManager._Instance.TotalMoneyGame >= aux_n.Value)
            {
                GameManager._Instance.naveComprada[IDSelect] = 1;
                //PlayerPrefs.SetInt("Ship" + aux_n.ID + "_isBuy", GameManager._Instance.naveComprada[IDSelect]);
                GameManager._Instance.TotalMoneyGame -= aux_n.Value;
                //GameManager._Instance.SaveLocal();
                GooglePlayServices.Instance.SaveToCloud();
                GameManager._Instance.SaveLocal();

                GooglePlayServices.Instance.UnlockAchievements(SkykingsGP.achievement_buy_yout_first_ship);

                AnalitycsManager.Instance.TotalDinheiroGasto += aux_n.Value;

                SelectPlayer("Refresh");
                print("Tentando comprar nave e CONSEGUI");
            }
        }
        else
            print("nao pode comprar nave");
    }

    public void MENU()
    {
		GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
        SceneManager.LoadScene("2 - Menu", LoadSceneMode.Single);
    }

    public void MudarTela(string cena)
    {
        GooglePlayServices.Instance.SaveToCloud();
        GameManager._Instance.SaveLocal();
        SceneManager.LoadScene(cena, LoadSceneMode.Single);
    }

	public void TutorialSIM()
	{
		MudarTela ("7 - Tutorial");
		GameManager._Instance.tutorialOK = false;
	}
	public void TutorialNAO()
	{
		TutorialPanel.SetActive (false);
		GameManager._Instance.tutorialOK = false;
	}
}
