﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

    [SerializeField] private int velocity = 10;
    public int timeToDestroy = 4;
	public int valor;
    public int pontosDados = 15;

    void Start()
    {

        StartCoroutine(TimeLife(timeToDestroy));

        velocity = (int)(GameManager._Instance.upgradeMagnetude[GameManager._Instance.SelectedShip_ID] + GameManager._Instance._player.MagnetudePadrao);

		valor = (int)Random.Range (1,4);
    }

    void Update()
    {
        if(Vector2.Distance(transform.position, GameManager._Instance._player.transform.position) <= 4)
            GetComponent<Rigidbody2D>().velocity = (GameManager._Instance._player.transform.position - transform.position).normalized * (Time.deltaTime * (velocity + 1));
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        { 
			if (GameManager._Instance.IsDoubleMoney) 
			{
				GameManager._Instance.MoneyGame += 2;
				AnalitycsManager.Instance.TotalDinheiroGanho += 2;
			} 
			else 
			{
				GameManager._Instance.MoneyGame++;
				AnalitycsManager.Instance.TotalDinheiroGanho++;
			}

            GameManager._Instance.PontosGanhosnoJogo += pontosDados;
            Destroy(gameObject);
        }
    }

    IEnumerator TimeLife(int time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
        AnalitycsManager.Instance.TotalDinheiroPerdido++;
    }
}
