﻿using UnityEngine;
using System.Collections;

public class Medal : MonoBehaviour {


    [SerializeField] private int pontosDados = 250;

    public int PontosDados
    {
        get
        {
            return pontosDados;
        }

        set
        {
            pontosDados = value;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (GameManager._Instance.IsDoubleMedal)
                GameManager._Instance.MedalMoney += 2;
            else
                GameManager._Instance.MedalMoney++;

            AnalitycsManager.Instance.TotalMedalhaGanho++;

            Destroy(gameObject);

            GameManager._Instance.PontosGanhosnoJogo += PontosDados;

        }
    }

    IEnumerator TimeLife(int time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }

}
