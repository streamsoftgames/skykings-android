﻿using UnityEngine;
using System.Collections;

public class Fuel : MonoBehaviour {

    public PlayerManager _player;
   [SerializeField] private int points = 50, pointsGet = 5;

    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }
    public int PointsGet
    {
        get
        {
            return pointsGet;
        }

        set
        {
            pointsGet = value;
        }
    }

    void Start () {
        _player = GameObject.Find("Player").GetComponent<PlayerManager>();
	}
	
	
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            
            RechargeFuel();
            Destroy(gameObject);
            AnalitycsManager.Instance.TotalGasolinasColetadas++;
            GameManager._Instance.PontosGanhosnoJogo += PointsGet;
            //GetComponent<Animator>().SetBool("Destroy", true);
        }

        if (col.CompareTag("Shoot"))
        {
            
            //Destroy(gameObject);
            _player._audio.PlaySFX("Explosion");
            
            Destroy(col.gameObject);
            GameManager._Instance.PontosGanhosnoJogo += Points;
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Animator>().SetTrigger("Destroy");
            AnalitycsManager.Instance.TotalGasolinasDestruidas++;
        }
    }

    void RechargeFuel()
    {
        _player.FuelActual = _player.FuelMax;
    }


}
