﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WebService : MonoBehaviour
{

    public static WebService instance { get; set; }

    #region Encapsulamento
    public int QuantidadeRecord
    {
        get
        {
            return quantidadeRecord;
        }

        set
        {
            quantidadeRecord = value;
        }
    }

    public string[] Nomes
    {
        get
        {
            return nomes;
        }

        set
        {
            nomes = value;
        }
    }

    public int[] Pontos
    {
        get
        {
            return pontos;
        }

        set
        {
            pontos = value;
        }
    }

    public string MinhaPosicao
    {
        get
        {
            return minhaPosicao;
        }

        set
        {
            minhaPosicao = value;
        }
    }

    #endregion

    private tempService wservice;
    [SerializeField]
    private int quantidadeRecord;
    [SerializeField]
    private string[] nomes;
    [SerializeField]
    private int[] pontos;

    private string keySky = "0b6c259c5b0b11782f95e99b6b0a9aea";

    private string minhaPosicao;

    private void Awake()
    {
        if (instance == null)
            instance = this;

       // wservice = new tempService();
        
    }


    public void SetScore(int score, int id)
    {
        stRegisterRecord record = new stRegisterRecord();
        record.iRecord = score;
        record.iCod_User = id;
        record.sUser = GameManager._Instance.Fb_id;
        record.sKeySkyKings = keySky;

        //wservice.Set_RecordSkyKings(record);
    }

    public void SendADS()
    {
        stRegisterADS rAds = new stRegisterADS();

        rAds.sKeySkyKings = keySky;
        rAds.iCod_User = GameManager._Instance.WebserviceID;
        rAds.sUser = GameManager._Instance.Fb_id;

        print("ads enviado");
    }

    public void RegisterCampeonato()
    {
        stRegisterChampion campeonato = new stRegisterChampion();
        campeonato.sKeySkyKings = keySky;
        campeonato.iCod_User = GameManager._Instance.WebserviceID;
        campeonato.sUser = GameManager._Instance.Fb_id;

        //wservice.Set_RegisterChampions(campeonato);

        GameManager._Instance.Ps4Participante = true;

        if (ZPlayerPrefs.HasKey("ps4Campeonato"))
            ZPlayerPrefs.SetString("ps4Campeonato", GameManager._Instance.Ps4Participante.ToString());

        print("registrado no campeonato");


    }

    public void RegisterUser()
    {
        //print(wservice.);

        stRegisterUser user = new stRegisterUser();
        stReturnUser retornoID = new stReturnUser();

        user.sEmail = GameManager._Instance.Fb_email;
        user.sLinkFacebook = "http://www.facebook.com/" + GameManager._Instance.Fb_id;
        user.sNm_User = GameManager._Instance.Fb_nome;
        user.sSmartPhone = Application.platform.ToString();
        user.sUser = GameManager._Instance.Fb_id;
        user.sLanguage = GameManager._Instance.Idioma;
        
        //wservice.Set_UserSkyKings(user);

      //  GameManager._Instance.WebserviceID = retornoID.iCod_User = wservice.Set_UserSkyKings(user).iCod_User;

        print("Registrado, id: " + GameManager._Instance.WebserviceID);

        

       // GetRecord(System.DateTime.Now.Month, System.DateTime.Now.Year);
        print(System.DateTime.Now.Month.ToString() + " m:a " + System.DateTime.Now.Year);

        SetScore((int)GameManager._Instance.BestDistance, GameManager._Instance.WebserviceID);
        
    }

    public void GetRecord(int mes, int ano)
    {
        stGetListRecords getLista = new stGetListRecords();
        stListRecords lista = new stListRecords();

        getLista.iMes = mes;
        getLista.iAno = ano;
        getLista.iQtLista = 30;
        getLista.iCod_User = GameManager._Instance.WebserviceID;
        getLista.sUser = GameManager._Instance.Fb_id;
     

       // lista = wservice.Get_List_Record(getLista);
        minhaPosicao = lista.sPosicao;

       // quantidadeRecord = wservice.Get_List_Record(getLista).Records.Length;

        Nomes = new string[quantidadeRecord];
        Pontos = new int[quantidadeRecord];

        print("tamanho: "+ quantidadeRecord);
        for (int i = 0; i < lista.Records.Length; i++)
        {
            Debug.Log("Nome: " + lista.Records[i].sNm_User + " Record: " + lista.Records[i].iRecord);
            Nomes[i] = lista.Records[i].sNm_User;
            Pontos[i] = lista.Records[i].iRecord;
        }

    }
}
