﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditosMenu : MonoBehaviour {

    public Text creditosInformacao;


	// Use this for initialization
	void Start () {
        if (GameManager._Instance.Idioma.Equals("Portuguese"))
        {
            creditosInformacao.text = "Produtor \nTARCISIO MICHELS \n\nProgramação e Game Design \nFREDERICO A. RAISS \n\nIlustração e Animação \nMAYKE SANTOS";
        }
        else
        {
            creditosInformacao.text = "Producer \nTARCISIO MICHELS \n\nPrograming and Game Design \nFREDERICO A. RAISS \n\nArt and Animation \nMAYKE SANTOS";
        }
       

    }
	
	public void ControlePainel()
    {
        bool aux = !gameObject.activeSelf;
        gameObject.SetActive(aux);
    }
}
