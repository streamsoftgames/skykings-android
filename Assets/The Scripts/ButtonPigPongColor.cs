﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonPigPongColor : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
		Color c = Color.Lerp(Color.white, Color.green, Mathf.PingPong(Time.time, 1));
		GetComponent<Image> ().color = c;
	}
}
