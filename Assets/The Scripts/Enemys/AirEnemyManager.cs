﻿using UnityEngine;
using System.Collections;

public class AirEnemyManager : MonoBehaviour
{

    [SerializeField]  private float lastPosition, nextPosition;

    public int velocity = 100;
    public int distanceTravel = 4;
    public string faceActual = "Left";

    private bool changeDirection = false;
    

    private Rigidbody2D _rigibody2D;
   
    
    void Start()
    {

        GetComponent<EnemysManager>().type = "Air";

        _rigibody2D = GetComponent<Rigidbody2D>();

        lastPosition = transform.position.x;

        if (transform.position.x < 0)
        {
           faceActual = "Right";
        }

        if (faceActual.Equals("Left"))
        {
            nextPosition = lastPosition - distanceTravel;
            GetComponent<SpriteRenderer>().flipY = false;
        }
        else
        {
            nextPosition = lastPosition + distanceTravel;
            GetComponent<SpriteRenderer>().flipY = true;
        }
    }


    void Update()
    {
            Move();
    }

    void Move()
    {
        if (this.GetComponent<EnemysManager>().life > 0) {
            if (faceActual.Equals("Left"))
            {

                if (changeDirection)
                {
                    GetComponent<SpriteRenderer>().flipY = false;
                    lastPosition = transform.position.x;
                    nextPosition = lastPosition - distanceTravel;
                    changeDirection = false;
                }

                if (transform.position.x < nextPosition)
                {
                    faceActual = "Right";
                    changeDirection = true;
                }

                _rigibody2D.velocity = new Vector2(-1 * (Time.deltaTime * velocity), _rigibody2D.velocity.y);
            }
            else
            {
                _rigibody2D.velocity = new Vector2(1 * (Time.deltaTime * velocity), _rigibody2D.velocity.y);

                if (changeDirection)
                {
                    GetComponent<SpriteRenderer>().flipY = true;
                    lastPosition = transform.position.x;
                    nextPosition = lastPosition + distanceTravel;
                    changeDirection = false;
                }

                if (transform.position.x > nextPosition)
                {
                    faceActual = "Left";
                    changeDirection = true;
                }
            }
        }
    }
   
}
