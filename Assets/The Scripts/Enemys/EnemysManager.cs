﻿using UnityEngine;
using System.Collections;

public class EnemysManager : MonoBehaviour
{
    public GameObject coin;
	public GameObject hitVisual;

    public AudioSource _audio;
    public AudioClip explosion;
	private Animator _anim;

    public string type;

    public float life = 1;
    public int level;
    public int pointsToPlayer = 10;


    // Use this for initialization
    void Start()
    {
        _audio = GetComponent<AudioSource>();
        _anim = GetComponent<Animator>();
        life = level = GameManager._Instance.GameLevel;

        GetComponent<Destroy>().aux = type;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._Instance.LevelChanged)
        {
            life = level = GameManager._Instance.GameLevel;

            GameManager._Instance.LevelChanged = false;
        }

        if (life <= 0 && !_anim.GetCurrentAnimatorStateInfo(0).IsName("ExplosionEnemies"))
        {
            _audio.clip = explosion;
            _audio.Play();
            GetComponent<Collider2D>().enabled = false;
            _anim.SetBool("Morte", true);
            life = -1;

			if (GameManager._Instance.IsTutorial) 
			{
				//GameObject.Find ("Tutorial Manager").GetComponent<TutorialCenarioManager> ().estagioTutorial;
				GameManager._Instance._player.GoSpeed = 1;
			}
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Shoot"))
        {
            life -= GameManager._Instance._player.Poder;
			//GameObject hitAux =  Instantiate(hitVisual, transform.position, Quaternion.identity) as GameObject;
			//hitAux.transform.SetParent (transform);
            Destroy(col.gameObject);
        }
        if (col.CompareTag("EspecialShoot"))
        {
            life = 0;
            Destroy(col.gameObject);
        }

    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            life = 0;
        }
    }
   
	public void EnemiesDestroy()
	{
		Instantiate(coin, transform.position, Quaternion.identity);
		print ("GEREI MOEDA!");
		GameManager._Instance.PontosGanhosnoJogo += pointsToPlayer;
	}
}
