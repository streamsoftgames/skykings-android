﻿using UnityEngine;
using System.Collections;

public class WaterEnemyManager : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GetComponent<EnemysManager>().type = "Water";

    }
	
	// Update is called once per frame
	void Update () {

        if (GameManager._Instance.InGame)
        {
            if (Vector2.Distance(transform.position, GameManager._Instance._player.transform.position) < 4)
            {
                GetComponent<Animator>().SetTrigger("SubmarinoOK");
            }
        }
	}
}
