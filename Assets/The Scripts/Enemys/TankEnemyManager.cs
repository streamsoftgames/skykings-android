﻿using UnityEngine;
using System.Collections;

public class TankEnemyManager : MonoBehaviour
{

    public float distancePlayerTotal = 0, distanceToAction = 0;

    public GameObject arm;
    public GameObject pointShoot;
    public GameObject Shoot;
    Vector3 targetPos;
    public float timer;
    public float fireRate = 1.2f;
    public float shootVelocity = 100f;

    bool aux;

    void Start()
    {

        GetComponent<EnemysManager>().type = "Tank";

        if (transform.position.x < -0.1)
            arm.transform.rotation = Quaternion.Euler(0, 0, 45);

        else if (transform.position.x >= -0.1 && transform.position.x <= 0.1)
        {
            int[] aux = new int[2] { 45, -45 };
            arm.transform.rotation = Quaternion.Euler(0, 0, aux[Random.Range(0, aux.Length)]);
        }

        else if (transform.position.x > 0.1)
            arm.transform.rotation = Quaternion.Euler(0, 0, -45);


        //Debug.Log(gameObject.name + " : " + arm.transform.rotation.z);
    }

    void Update()
    {
        distancePlayerTotal = Vector2.Distance(transform.position, GameObject.Find("Player").transform.position);

        timer += Time.deltaTime;


        if (distancePlayerTotal < distanceToAction && GetComponent<EnemysManager>().life > 0)
        {

            //Quaternion.LookRotation(Vector3.forward, (GameObject.Find("Player").transform.position - arm.transform.position)* -1);


            if (timer > fireRate)
            {
                GameObject shootAux = Instantiate(Shoot, pointShoot.transform.position, Quaternion.identity) as GameObject;

                if (arm.transform.rotation.z > 0)
                {
                    shootAux.transform.rotation = Quaternion.Euler(0, 0, -135);
                    shootAux.GetComponent<ShootManager>().faceLeft = false;
                }

                else if (arm.transform.rotation.z < 0)
                {
                    shootAux.transform.rotation = Quaternion.Euler(0, 0, 135);
                    shootAux.GetComponent<ShootManager>().faceLeft = true;
                }
                    

                timer = timer - fireRate;

            }

        }
        else
        {
            timer = 0;
        }

        if(GetComponent<EnemysManager>().life <= 0 && transform.childCount > 0)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }


}
