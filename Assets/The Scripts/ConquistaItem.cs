﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConquistaItem : MonoBehaviour
{

    public string ID;

    private int estagioBotao = 0;

    public Text proximoGanhoMedalha_txt; //quanto vai ganhar de medalha na proxima conquista
    public Text proximaComquista_txt; //quanto precisa para ganhar a proxima conquista
    public Text quantidadeAtual_txt; //numero atual para a conquista
    public Text totalRecompensa_txt;

    [SerializeField]
    private int LevelAUX;
    [SerializeField]
    private string quantidadeProximaConquista; //mostra o quando precisa par ao proxima conquista
    [SerializeField]
    private int quantidadeAtualInformacao; //pega informação do analitics e mostra a quantidade atual de itens feitos.

    [SerializeField]    private int totalRecebido = 0;


    //  public Button compraBotao;
    public Sprite[] medalsImages;

    public Image medalBronze;
    public Image medalSilver;
    public Image medalGold;

    [SerializeField]
    private bool carregarInicial = false;

    GameManager g;

    // Use this for initialization
    void Start()
    {
        g = GameManager._Instance;
        //   compraBotao = transform.FindChild("Acept Reward").GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        AtualizarInformacoes();

        proximaComquista_txt.text = quantidadeProximaConquista;
        quantidadeAtual_txt.text = " " + quantidadeAtualInformacao;
        totalRecompensa_txt.text = totalRecebido.ToString();
    }


    void AtualizarInformacoes()
    {

        #region Inimigos Destruidos

        if (ID.Equals("inimigos destruidos"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelInimigos;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalInimigosDestruidos;
            totalRecebido = g.EstagioLevelInimigos;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[LevelAUX].ToString();
                
            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }

        }
        #endregion
        
        #region Aereos Destruidos
        else if (ID.Equals("aereos destruidos"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelAereos;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalAereosDestruidos;
            totalRecebido = g.EstagioLevelAereos;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_aereos[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.aereosDestruidos[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Aquaticos Destruidos
        else if (ID.Equals("aquaticos destruidos"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelAquaticos;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalAquaticosDestruidos;
            totalRecebido = g.EstagioLevelAquaticos;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_aquaticos[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.aquaticosDestruidos[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Tanques Destruidos
        else if (ID.Equals("tanques destruidos"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelTanques;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalTanquesDestruidos;
            totalRecebido = g.EstagioLevelTanques;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_tanques[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.tanquesDestruidos[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Combustiveis Destruidos
        else if (ID.Equals("combustiveis destruidos"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelCombustivelsDest;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalGasolinasDestruidas;
            totalRecebido = g.EstagioLevelCombustivelsDest;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_combustiveisDestruidos[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.combustivelDestruidos[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Combustiveis Coletados
        else if (ID.Equals("combustiveis coletados"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelCombustivelsColet;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalGasolinasColetadas;
            totalRecebido = g.EstagioLevelCombustivelsColet;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_combustiveisColetados[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.combustiveColetados[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Medalhas Coletadas
        else if (ID.Equals("medalhas coletadas"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelMedalhasColet;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalMedalhaGanho;
            totalRecebido = g.EstagioLevelInimigos;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_medalhasColetadas[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.medalhasColetadas[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }

        }
        #endregion

        #region Moedas Coletadas
        else if (ID.Equals("moedas coletadas"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelMoedasColet;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalDinheiroGanho;
            totalRecebido = g.EstagioLevelMoedasColet;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_moedasColetadas[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.moedasColetadas[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }

        }
        #endregion

        #region Distancia Percorrida
        else if (ID.Equals("distancia percorrida"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelDistancia;

            quantidadeAtualInformacao = (int)AnalitycsManager.Instance.TotalDistanciaViajada;
            totalRecebido = g.EstagioLevelDistancia;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_distancia[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.distanciaPercorrida[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Quantidade de Mortes
        else if (ID.Equals("quantidade mortes"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelMortes;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalMortes;
            totalRecebido = g.EstagioLevelMortes;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_quantidadeMorte[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.quantidadeMortes[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

        #region Dinheiro Gasto
        else if (ID.Equals("dinheiro gasto"))
        {
            LevelAUX = ConquistasManager.Instancia.LevelDinheiroGasto;

            quantidadeAtualInformacao = AnalitycsManager.Instance.TotalDinheiroGasto;
            totalRecebido = g.EstagioLevelInimigos;

            if (LevelAUX < 3)
            {
                proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_dinheiroGasto[LevelAUX].ToString();
                quantidadeProximaConquista = ConquistasManager.Instancia.dinheiroGasto[LevelAUX].ToString();

            }
            else
            {
                proximoGanhoMedalha_txt.text = "MAX";
                quantidadeProximaConquista = "MAX";
            }

            if (!carregarInicial)
            {
                //estagioBotao = GameManager._Instance.EstagioLevelInimigos;
                //totalRecebido = GameManager._Instance.TotalRecebido_inimigos;
                carregarInicial = true;
            }

            //auxGoal = ConquistasManager.Instancia.inimigosDestruidos[ConquistasManager.Instancia.levelInimigos];

            if (LevelAUX == 0)
            {
                medalBronze.sprite = medalsImages[0];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[0].ToString();
                //  quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[0];

            }

            else if (LevelAUX == 1)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[0];
                medalGold.sprite = medalsImages[0];

                //  proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[1].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[1];

            }
            else if (LevelAUX == 2)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[0];

                //   proximoGanhoMedalha_txt.text = ConquistasManager.Instancia.recompenca_inimigos[2].ToString();
                //   quantidadeProximaConquista = ConquistasManager.Instancia.inimigosDestruidos[2];

            }
            else if (LevelAUX == 3)
            {
                medalBronze.sprite = medalsImages[1];
                medalSilver.sprite = medalsImages[2];
                medalGold.sprite = medalsImages[3];
            }
        }
        #endregion

    }

}
