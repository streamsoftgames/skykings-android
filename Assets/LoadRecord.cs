﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadRecord : MonoBehaviour {

    public GameObject recordObj;
    public Text posicaoPlayer;
    public Dropdown mesSelecao;
    public Dropdown anoSelecao;

    private void Awake()
    {
        if (GameManager._Instance.temConexao)
        {
           // WebService.instance.GetRecord(System.DateTime.Now.Month, System.DateTime.Now.Year);
            posicaoPlayer.text = " " + WebService.instance.MinhaPosicao;
        }
        else
            posicaoPlayer.text = "NO CONECTION";


      /*  for (int i = System.DateTime.Now.Month; i > 0; i--)
        {
            Dropdown.OptionData aux = new Dropdown.OptionData(i.ToString());
            mesSelecao.options.Add(aux);
        }*/
    }


	// Use this for initialization
	void Start ()
    {

        

        for (int i = 0; i < WebService.instance.QuantidadeRecord; i++)
        {
            GameObject auxObj = Instantiate(recordObj);
            auxObj.transform.SetParent(this.transform,false);

            auxObj.name = "Record " + i + " " + WebService.instance.Nomes[i]; 
            auxObj.transform.GetChild(1).GetComponent<Text>().text = (i+1).ToString() + "";
            auxObj.transform.GetChild(2).GetComponent<Text>().text = WebService.instance.Nomes[i];
            auxObj.transform.GetChild(3).GetComponent<Text>().text = WebService.instance.Pontos[i].ToString();
        }
	}

    public void MudarData()
    {
        int mes = int.Parse(mesSelecao.transform.GetChild(0).GetComponent<Text>().text);

        int ano = int.Parse(anoSelecao.transform.GetChild(0).GetComponent<Text>().text);

        print(mes + " " + ano);
        //WebService.instance.GetRecord(mes, ano);
    }

    public void GoMenu() { SceneManager.LoadScene("2 - Menu"); print("CLIQUEI AQUI POARA VOLTAR AO MENU"); }
}


