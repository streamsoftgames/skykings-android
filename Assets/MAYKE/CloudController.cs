﻿using UnityEngine;
using System.Collections;

public class CloudController : MonoBehaviour {

	public Renderer[] rends;
	public float speed = 0.1f;

	// Use this for initialization
	void Start () {
		rends = new Renderer[2];
		rends [0] = transform.GetChild (0).GetComponent<MeshRenderer> ();
		rends [1] = transform.GetChild (1).GetComponent<MeshRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		float offset = Time.time * speed;
		rends [0].material.SetTextureOffset ("_MainTex", new Vector2 (0, offset));
		rends [1].material.SetTextureOffset ("_MainTex", new Vector2 (0, offset));
	}
}
