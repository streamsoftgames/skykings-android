#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("2LZQAbG7if6oxunw9aPr1fLuxuBHxq4arPLEep5Feesok4UJkaiTSkHtS2W00uTcMfnrQLtqqJU+vXbhjMZ094DG+PD1o+v59/cJ8vL19PeBgdiXhoaak9iVmZvZl4aGmpOVl4m3Xm4PJzyQatKd5yZVTRLt3DXp8Mb58PWj6+X39wny88b19/cJxuva1pWThIKfkJ+Vl4KT1oaZmp+Vj5Sak9aFgpeYkpeEktaCk4SbhdaXXiqI1MM80yMv+SCdIlTS1ecBV1rxGovPdX2l1iXOMkdJbLn8nQndCvvw/9xwvnAB+/f38/P29XT39/aqhpqT1qSZmYLWtbfG6OH7xsDGwsTZxnc18P7d8Pfz8/H09MZ3QOx3RfD1o+v48uDy4t0mn7FigP8IAp178vDl9KOlx+XG5/D1o/L85fy3hoavUfP/iuG2oOfogiVBfdXNsVUjmdaZkNaCnpPWgp6TmNaXhoaan5WXXVWHZLGlozdZ2bdFDg0VhjsQVbp09/bw/9xwvnABlZLz98Z3BMbc8IKemYSfgo/H4Mbi8PWj8vXl+7eG/qjGdPfn8PWj69bydPf+xnT38sbpZy3osaYd8xuoj3LbHcBUobqjGpiS1pWZmJKfgp+ZmIXWmZDWg4WTpJOan5eYlZPWmZjWgp6fhdaVk4Q2lcWBAczx2qAdLPnX+CxMhe+5Q5+Qn5WXgp+ZmNa3g4KemYSfgo/Hj9aXhYWDm5OF1peVlZOGgpeYlZP5a8sF3b/e7D4IOENP+C+o6iA9y3mFd5Yw7a3/2WREDrK+BpbOaOMDxcCsxpTH/cb/8PWj8vDl9KOlx+WSw9XjveOv60ViAQBqaDmmTDeupsbn8PWj8vzl/LeGhpqT1r+YldjHgp+Qn5WXgpPWlI/Wl5iP1oaXhIJ24t0mn7FigP8IAp172LZQAbG7ifP29XT3+fbGdPf89HT39/YSZ1//xnTyTcZ09VVW9fT39PT39Mb78P8vwIk3caMvUW9PxLQNLiOHaIhXpGNojPpSsX2tIuDBxT0y+bs44p8ny9CR1nzFnAH7dDkoHVXZD6WcrZJDzFsC+fj2ZP1H1+DYgiPK+y2U4LOI6bqdpmC3fzKClP3mdbdxxXx3wG+6245BG3ptKgWBbQSAJIHGuTfQxtLw9aPy/eXrt4aGmpPWtZOEgr8ugGnF4pNXgWI/2/T19/b3VXT3fe9/KA+9mgPxXdTG9B7uyA6m/yXccL5wAfv39/Pz9saUx/3G//D1o5qT1r+YldjH0MbS8PWj8v3l67eGSAKFbRgkkvk9j7nCLlTID44JnT6El5WCn5WT1oWCl4KTm5OYgoXYxsPEx8LGxcCs4fvFw8bExs/Ex8LGkXn+QtYBPVra1pmGQMn3xnpBtTk/74QDq/gjialtBNP1TKN5u6v7B+lzdXPtb8uxwQRfbbZ42iJHZuQu/t3w9/Pz8fT34OiegoKGhczZ2YHWl5iS1pWThIKfkJ+Vl4KfmZjWhoaak9a1k4SCn5CflZeCn5mY1reD1rW3xnT31Mb78P/ccL5wAfv39/fgxuLw9aPy9eX7t4aGmpPWpJmZgtIUHSdBhin5sxfRPAebjhsRQ+Hhplx8IywSCib/8cFGg4PX");
        private static int[] order = new int[] { 31,26,59,28,36,17,29,44,42,54,51,47,29,37,30,59,52,41,24,34,41,33,59,43,38,57,47,40,59,55,42,41,54,39,54,42,41,58,38,56,43,57,48,51,51,52,47,59,50,59,51,56,54,59,56,56,57,58,58,59,60 };
        private static int key = 246;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
