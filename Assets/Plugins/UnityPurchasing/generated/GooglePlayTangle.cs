#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("4WcUZMGlMHUZLk9snurkn73DGnq6wLVsaRAPzLG1M2y/cNTFiLl44B/z4U3b2uQDs2XLFeFrcqf/cJm5X3hAqNmWCQqgQj3JY7sn63MO50Zpyh4bMgjZg/BrgecQ6rfTn+7G8wQ5drlgDQYFVKPUYW3kYGhcygxpHaNk2ffm68wFqJnO4erB9dkQKg00xYOtlzGD2bTrdo0ROoZ5fh42sRqoKwgaJywjAKxirN0nKysrLyoprsOysPpue9Wjb/JWQaOK5NbGqvH7TGwWv3EFBUrJy95UjfOj7PY46sFQGNUBr9tIdFncQ4bqj+q2QNlQqCslKhqoKyAoqCsrKrXENMR7sRyvun1GaUGtbEE6oNLkWQOlEaA1qf2oza1rMAPoLSgpKyor");
        private static int[] order = new int[] { 8,2,9,7,10,6,13,10,8,12,10,11,12,13,14 };
        private static int key = 42;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
